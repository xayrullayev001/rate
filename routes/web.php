<?php

use App\Http\Controllers\AutoController;
use App\Http\Controllers\BonusController;
use App\Http\Controllers\CertificateController;
use App\Http\Controllers\ConcernController;
use App\Http\Controllers\ConclusionController;
use App\Http\Controllers\DebitController;
use App\Http\Controllers\DillerController;
use App\Http\Controllers\DistrictController;
use App\Http\Controllers\EstateController;
use App\Http\Controllers\FileStoreController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\NotificationTemplateController;
use App\Http\Controllers\NotificationTypeController;
use App\Http\Controllers\OrderMembersController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\PurposeController;
use App\Http\Controllers\QrController;
use App\Http\Controllers\RegionController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\ToolsController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserInfoController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//Language Translation
Route::get('index/{locale}', [HomeController::class, 'lang']);

Route::get('/', [HomeController::class, 'index'])->name('root');
Route::get('index', [HomeController::class, 'index'])->name('index');

//Update User Details
Route::post('/update-profile/{id}', [ProfileController::class, 'updateProfile'])->name('updateProfile');
Route::post('/update-password/{id}', [ProfileController::class, 'updatePassword'])->name('updatePassword');
Route::get('qr/show/{content}', [QrController::class, 'show'])->name('qr.show');
Route::get('qr/re-generate/{id}/{type}', [QrController::class, 'reGenerate'])->name('qr.generate');
Route::get('conclusion/test-water-mark', [ConclusionController::class, 'testWaterMark'])->name('wm.show');

Route::post('/member/remove', [OrderMembersController::class, 'remove'])->name('member.remove');
Route::post('/members/invite', [OrderMembersController::class, 'invite'])->name('members.invite');

Route::get('/estate', [EstateController::class, 'index'])->name('estate.index');
Route::get('/estate/create', [EstateController::class, 'create'])->name('estate.create');
Route::post('/estate/store', [EstateController::class, 'store'])->name('estate.store');
Route::get('/estate/edit/{id}', [EstateController::class, 'edit'])->name('estate.edit');
Route::put('/estate/update/{id}', [EstateController::class, 'update'])->name('estate.update');
Route::get('/estate/show/{order}', [EstateController::class, 'show'])->name('estate.show');
Route::get('/estate/show-activities/{order}', [EstateController::class, 'showActivities'])->name('estate.show-activities');
Route::get('/estate/show-team/{order}', [EstateController::class, 'showTeam'])->name('estate.show-team');
Route::get('/estate/show-documents/{order}', [EstateController::class, 'showDocuments'])->name('estate.show-documents');
Route::put('/estate/order-clone/{id}', [EstateController::class, 'orderClone'])->name('estate.clone');

Route::get('/auto', [AutoController::class, 'index'])->name('auto.index');
Route::get('/auto/create', [AutoController::class, 'create'])->name('auto.create');
Route::post('/auto/store', [AutoController::class, 'store'])->name('auto.store');
Route::get('/auto/edit/{id}', [AutoController::class, 'edit'])->name('auto.edit');
Route::put('/auto/update/{id}', [AutoController::class, 'update'])->name('auto.update');
Route::get('/auto/show/{order}', [AutoController::class, 'show'])->name('auto.show');
Route::get('/auto/show-activities/{order}', [AutoController::class, 'showActivities'])->name('auto.show-activities');
Route::get('/auto/show-team/{order}', [AutoController::class, 'showTeam'])->name('auto.show-team');
Route::get('/auto/show-documents/{order}', [AutoController::class, 'showDocuments'])->name('auto.show-documents');
Route::put('/auto/order-clone/{id}', [AutoController::class, 'orderClone'])->name('auto.clone');

Route::get('/conclusion/add/{id}/{type}', [ConclusionController::class, 'create'])->name('conclusion.add');
Route::post('/conclusion/store', [ConclusionController::class, 'store'])->name('conclusion.store');
Route::put('/conclusion/reject', [ConclusionController::class, 'reject'])->name('conclusion.reject');


Route::get('/tools', [ToolsController::class, 'index'])->name('tools.index');
Route::get('/user', [UserController::class, 'index'])->name('user.index');
Route::post('/user/store', [UserController::class, 'create'])->name('user.store');
Route::get('/user/show/{id}', [UserController::class, 'show'])->name('user.show');
Route::put('/user/update', [UserController::class, 'update'])->name('user.update');
Route::post('/user/favourite/{id}', [UserController::class, 'favourite'])->name('user.favourite');
Route::put('/user/password/change', [UserController::class, 'updatePassword'])->name('password.change');

Route::get('/user/info/edit', [UserInfoController::class, 'edit'])->name('profile.info.edit');
Route::put('/user/info/store', [UserInfoController::class, 'update'])->name('profile.info.update');

Route::get('/profile/edit', [UserController::class, 'edit'])->name('profile.edit');
Route::put('/profile/update', [UserController::class, 'update'])->name('profile.update');
Route::get('/profile/show-activities', [ProfileController::class, 'showActivities'])->name('profile.show-activities');
Route::get('/profile/show-projects', [ProfileController::class, 'showProjects'])->name('profile.show-projects');
Route::get('/profile/show-documents', [ProfileController::class, 'showDocuments'])->name('profile.show-documents');

Route::get('/user/profile', [ProfileController::class, 'profile'], 'profile')->name('profile');
Route::put('/user/activate/{id}', [UserController::class, 'activate'])->name('user.activate');
Route::put('/user/block/{id}', [UserController::class, 'block'])->name('user.block');
Route::get('/user/appraisers', [UserController::class, 'appraisers'])->name('user.appraisers');
Route::get('/appraisers/bonus', [UserController::class, 'appraisersBonus'])->name('appraisers.bonus');
Route::delete('/diller/{id}', [UserController::class, 'destroy'])->name('diller.destroy');

Route::get('/diller', DillerController::class)->name('diller.index');
Route::get('/role', [RoleController::class, 'index'])->name('role.index');
Route::put('/role/permit', [RoleController::class, 'permit'])->name('role.permit');
Route::get('/notification/type', NotificationTypeController::class)->name('notification.type.index');
Route::get('/notification/template', [NotificationTemplateController::class, 'index'])->name('notification.template.index');
//Route::post('/store/my/file', [FileStoreController::class, 'storeMyFile'])->name('store.my.file');
Route::post('/attach/file', [FileStoreController::class, 'attachFiles'])->name('attach.files');
Route::post('/attach/profile-file', [FileStoreController::class, 'attachProfileFiles'])->name('profile.files');
Route::get('/download/{file}', [FileStoreController::class, 'downloadFile'])->name('download');
Route::delete('file/delete/{file}', [FileStoreController::class, 'delete'])->name('file.delete');
Route::put('debit/credit', [BonusController::class, 'storeCredit'])->name('credit.store');
Route::resource('/debit', DebitController::class);
Route::resource('bonus', BonusController::class);
Route::resource('certificates', CertificateController::class);
Route::resource('purposes', PurposeController::class);
Route::resource('regions', RegionController::class);
Route::get('/regions/districts/{id}', [RegionController::class, 'districts'])->name('regions.districts');
Route::resource('districts', DistrictController::class);
Route::resource('concerns', ConcernController::class);

Route::get('{any}', [HomeController::class, 'pages'])->name('pages');
