@extends('layouts.master')
@section('title') @lang('translation.certificates')  @endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.pages')  @endslot
        @slot('title') @lang('translation.certificates')  @endslot
    @endcomponent

    <div class="col-4 mb-4">
        <button class="btn btn-success createFile-modal" data-bs-toggle="modal"
                data-bs-target="#createFileModal">
            <i class="ri-add-line align-bottom me-1"></i>
            @lang('translation.create-file')
        </button>
    </div>
    <div class="row">
        @foreach ($certificates as $certificate)
            <div class="col-xxl-4 col-lg-6">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title mb-0">
                            <p class="card-text mb-0">
                                @lang(\App\Enums\MediaTypeEnum::getLabel($certificate->type))
                            </p>
                        </h4>
                    </div>
                    <div class="card-img rounded-0 img-fluid">
                        <object
                            data="{{ \Illuminate\Support\Facades\Storage::url($certificate->path) }}"
                            alt="Card image cap" height="500" width="100%"></object>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <!-- ghost Buttons -->
                            @component('components.delete-btn',['url'=> route('certificates.destroy',[$certificate->id]) ])
                            @endcomponent
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>

    <!-- START CREATE FILE MODAL -->
    <div class="modal fade zoomIn" id="createFileModal" tabindex="-1" aria-labelledby="createFileModalLabel"
         aria-hidden="true" validate>
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0">
                <div class="modal-header p-3 bg-soft-success">
                    <h5 class="modal-title" id="createFileModalLabel">
                        @lang('translation.create-file')
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" id="addFileBtn-close"
                            aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form action="{{ route('certificates.store') }}" method="post" enctype="multipart/form-data"
                          class="needs-validation"
                          validate>
                    @csrf
                    <!-- end dropzon-preview -->
                        <div class="mb-4 form-group">
                            <label for="type">
                                <p class="card-title mb-0">Sertifakat turini tanlang</p>
                            </label>
                            <select class="form-control @error('type') is-invalid @enderror" name="type"
                                    id="type" required>
                                <option value="{{ \App\Enums\MediaTypeEnum::APPRAISER_CERTIFICATE->name }}">
                                    @lang(\App\Enums\MediaTypeEnum::APPRAISER_CERTIFICATE->value)
                                </option>
                                <option value="{{ \App\Enums\MediaTypeEnum::INSURANCE_POLICY->name }}">
                                    @lang(\App\Enums\MediaTypeEnum::INSURANCE_POLICY->value)
                                </option>
                                <option value="{{ \App\Enums\MediaTypeEnum::PARTICIPATE_CERTIFICATE->name }}">
                                    @lang(\App\Enums\MediaTypeEnum::PARTICIPATE_CERTIFICATE->value)
                                </option>
                                <option value="{{ \App\Enums\MediaTypeEnum::CERTIFICATE->name }}">
                                    @lang(\App\Enums\MediaTypeEnum::CERTIFICATE->value)
                                </option>
                            </select>
                            @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="mb-4 form-group">
                            <label for="type">
                                <p class="card-title mb-0">Sertifakat faylini yuklang</p>
                            </label>
                            <p class="text-muted mb-2"> Sertifakat fayli <code>.pdf</code> bo'lishi kerak! </p>
                            <input type="file" class="form-control @error('file') is-invalid @enderror"
                                   name="file" required>
                            @error('file')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                        @enderror
                        <!-- end card body -->
                        </div>
                        <div class="mb-4">
                            <!-- Gradient Buttons -->
                            <button type="submit" class="btn btn-secondary bg-gradient waves-effect waves-light">
                                @lang('translation.save')
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END CREATE FILE MODAL -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
