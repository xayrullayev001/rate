{{ __($title) }} :<span class="badge badge-soft-info text-uppercase">
    {{ $area }} m<sup>2</sup>
</span>
