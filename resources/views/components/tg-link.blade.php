<label for="cleave-phone" class="form-label">
    @lang('translation.telegram-phone')
</label>
<div class="badge fw-medium badge-soft-secondary">
    <a target="_blank" href="https://t.me/<?= str_replace('-', '', $phone)?>">t.me/{{ $phone }}</a>
</div>
