<select data-choices data-choices-search-false onchange="this.form.submit()"
        data-choices-multiple-groups="false" class="form-control"
        name="size">
    <option value="10" @if($size==10) selected @endif> 10</option>
    <option value="30" @if($size==30) selected @endif> 30</option>
    <option value="50" @if($size==50) selected @endif> 50</option>
    <option value="100" @if($size==100) selected @endif> 100</option>
</select>
