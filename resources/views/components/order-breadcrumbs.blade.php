<div class="row">
    <div class="col-lg-12">
        <div class="card mt-n4 mx-n4">
            <div class="bg-soft-warning">
                <div class="card-body pb-0 px-4">
                    <div class="row mb-3">
                        <div class="col-md">
                            <div class="row align-items-center g-3">
                                <div class="col-md-auto">
                                    <div class="avatar-md">
                                        <div class="avatar-title bg-white rounded-circle">
                                            <!-- 'assets/images/brands/slack.png' -->
                                            <img
                                                src="{{ URL::asset('storage/attachments/auto_'.$order->id.'/qr.png') }}"
                                                alt=""
                                                class="avatar-md">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md">
                                    <div>
                                        @props(['order'])
                                        <h4 class="fw-bold">{{ $order->car_mark }}</h4>
                                        <div class="hstack gap-3 flex-wrap">
                                            <div>
                                                <i class="ri-car-line align-bottom me-1"></i>
                                                {{ $order->number }}
                                            </div>
                                            <div class="vr"></div>
                                            <div>@lang('translation.created-date') : <span
                                                    class="fw-medium">{{ $order->created_at }}</span>
                                            </div>
                                            <div class="vr"></div>
                                            <div>@lang('translation.updated-date') : <span
                                                    class="fw-medium">{{ $order->updated_at }}</span>
                                            </div>
                                            <div class="vr"></div>
                                            <div class="badge rounded-pill bg-info fs-12">
                                                @lang(\App\Enums\OrderStatusEnum::getLabel($order->status))
                                            </div>
                                            <div class="badge rounded-pill bg-warning fs-12">
                                                <a class="fs-12 text-white"
                                                   href="{{ route('auto.edit', ['id' => $order->id]) }}">
                                                    @lang('translation.edit')
                                                </a>
                                            </div>
                                            <form action="{{ route('auto.clone', [$order->id]) }}" method="post"
                                                  novalidate>
                                                @csrf
                                                @method('put')
                                                <input type="hidden" value="{{ $order->id }}" name="id">
                                                <button type="submit"
                                                        class="fs-12 badge rounded-pill bg-primary text-white">
                                                    @lang('translation.clone')
                                                </button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-auto">
                            <div class="hstack gap-1 flex-wrap">
                                <button type="button" class="btn py-0 fs-16 favourite-btn active">
                                    <i class="ri-star-fill"></i>
                                </button>
                                <a target="_blank" data-bs-toggle="tooltip" data-bs-placement="top"
                                   title="@lang('translation.share-tg')"
                                   href="https://t.me/share/url?url={{ route('auto.show', [$order->id]) }}&text={{ $order->car_mark }}"
                                   type="button" class="btn py-0 fs-16 text-body">
                                    <i class="ri-share-line"></i>
                                </a>
                                <a href="{{ route('qr.generate',[$order->id,\App\Enums\OrderTypeEnum::AUTO->name]) }}"
                                   data-bs-toggle="tooltip" data-bs-placement="top"
                                   title="@lang('translation.re-generate-qr-code')"
                                   class="btn py-0 fs-16 text-body">
                                    <i class="ri-restart-line"></i>
                                </a>
                                <a href="{{ route('qr.show', base64_encode("{$order->id}:AUTO")) }}"
                                   data-bs-toggle="tooltip" data-bs-placement="top"
                                   title="@lang('translation.show-qr-code')"
                                   class="btn py-0 fs-16 text-body">
                                    <i class="ri-qr-code-line"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                    <ul class="nav nav-tabs-custom border-bottom-0" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link @if ($active == 'index') active @endif fw-semibold"
                               href="{{ route('auto.show', [$order->id]) }}" role="tab">
                                @lang('translation.overview')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-semibold @if ($active == 'documents') active @endif"
                               href="{{ route('auto.show-documents', [$order->id]) }}" role="tab">
                                @lang('translation.documents')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-semibold @if ($active == 'activities') active @endif"
                               href="{{ route('auto.show-activities', [$order->id]) }}" role="tab">
                                @lang('translation.activities')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link fw-semibold @if ($active == 'team') active @endif"
                               href="{{ route('auto.show-team', [$order->id]) }}" role="tab">
                                @lang('translation.team')
                            </a>
                        </li>
                    </ul>
                </div>
                <!-- end card body -->
            </div>
        </div>
        <!-- end card -->
    </div>
    <!-- end col -->
</div>

