@if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
    <form method="POST" action="{{ $url }}">
        @csrf
        @method('delete')
        <button type="submit" onclick="return confirm('Are you sure?')" class="dropdown-item" data-bs-toggle="tooltip"
                data-bs-placement="top"
                title="@lang("translation.delete-information")">
            <i class="ri-delete-bin-fill align-bottom me-2 text-muted"></i>
        </button>
    </form>
@endif
