<span class="badge badge-soft-info text-uppercase">
    @if ($balance < 0) <?=number_format($balance, 2)?> @lang('translation.sum')
    @else <?=number_format($balance, 2)?> @lang('translation.sum')
    @endif
</span>
