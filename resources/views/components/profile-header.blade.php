<div class="d-flex">
    <!-- Nav tabs -->
    <ul class="nav nav-pills animation-nav profile-nav gap-2 gap-lg-3 flex-grow-1" role="tablist">
        <li class="nav-item">
            <a class="nav-link fs-14 @if($active==="active") active @endif"
               href="{{ route('profile') }}">
                <i class="ri-airplay-fill d-inline-block d-md-none"></i> <span
                    class="d-none d-md-inline-block">@lang('translation.overview')</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link fs-14 @if($active==="active") active @endif"
               href="{{ route('profile.show-activities') }}">
                <i class="ri-list-unordered d-inline-block d-md-none"></i> <span
                    class="d-none d-md-inline-block">@lang('translation.actions')</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link fs-14 @if($active==="active") active @endif"
               href="{{ route('profile.show-projects') }}">
                <i class="ri-price-tag-line d-inline-block d-md-none"></i> <span
                    class="d-none d-md-inline-block">@lang('translation.orders')</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link fs-14 @if($active==="active") active @endif"
               href="{{ route('profile.show-documents') }}">
                <i class="ri-folder-4-line d-inline-block d-md-none"></i> <span
                    class="d-none d-md-inline-block">@lang('translation.documents')</span>
            </a>
        </li>
    </ul>
    <div class="flex-shrink-0">
        <a href="{{ route('profile.edit') }}" class="btn btn-success"><i
                class="ri-edit-box-line align-bottom"></i> Edit Profile</a>
    </div>
</div>
