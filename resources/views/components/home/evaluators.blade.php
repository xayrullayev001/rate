<div class="row">
    <div class="col-xl-4">
        <div class="card card-height-100">
            <div class="card-header align-items-center d-flex">
                <h4 class="card-title mb-0 flex-grow-1">@lang('translation.conclusions-are-in-the-section-of-evaluators')</h4>
                <div class="flex-shrink-0">
                    <div class="dropdown card-header-dropdown">
                        <a class="text-reset dropdown-btn" href="#"
                           data-bs-toggle="dropdown" aria-haspopup="true"
                           aria-expanded="false">
                                        <span class="text-muted">Report<i
                                                class="mdi mdi-chevron-down ms-1"></i></span>
                        </a>
                        <div class="dropdown-menu dropdown-menu-end">
                            <a class="dropdown-item" href="#">Download Report</a>
                            <a class="dropdown-item" href="#">Export</a>
                            <a class="dropdown-item" href="#">Import</a>
                        </div>
                    </div>
                </div>
            </div><!-- end card header -->

            <div class="card-body">
                <div id="store-visits-source2"
                     data-colors='["#405189", "#0ab39c", "#f7b84b", "#f06548", "#299cdb", "#299cdb"]'
                     class="apex-charts" dir="ltr"
                     data-labels='{{ $labels }}'
                     data-values="{{ $values }}"></div>
            </div>
        </div> <!-- .card-->
    </div> <!-- .col-->
