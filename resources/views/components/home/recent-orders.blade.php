<div class="col-xl-8">
    <div class="card">
        <div class="card-header align-items-center d-flex">
            <h4 class="card-title mb-0 flex-grow-1">@lang('translation.recently-created-orders')</h4>
            <div class="flex-shrink-0">
                <button type="button" class="btn btn-soft-info btn-sm">
                    <i class="ri-file-list-3-line align-middle"></i> Generate Report
                </button>
            </div>
        </div><!-- end card header -->
        <div class="card-body">
            <div class="table-responsive table-card">
                <table
                    class="table table-borderless table-centered align-middle table-nowrap mb-0">
                    <thead class="text-muted table-light">
                    <tr>
                        <th scope="col">@lang('translation.order-id')</th>
                        <th scope="col">@lang('translation.customer')</th>
                        <th scope="col">@lang('translation.car-mark')</th>
                        <th scope="col">@lang('translation.conclusion-price')</th>
                        <th scope="col">@lang('translation.status')</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($orders as $order)
                        <tr>
                            <td>
                                <a href="{{ route('auto.show',[$order->id]) }}"
                                   class="fw-medium link-primary">
                                    {{ $order->number }}
                                </a>
                            </td>
                            <td>{{ $order->customer }}</td>
                            <td>
                                {{ $order->car_mark }}
                            </td>
                            <td>
                                <span class="text-success">
                                    @component('components.price',['balance' => $order->cost]) @endcomponent
                                </span>
                            </td>
                            <td>
                                <span class="badge badge-soft-success">
                                    @lang(\App\Enums\OrderStatusEnum::getLabel($order->status))
                                </span>
                            </td>
                        </tr>
                        <!-- end tr -->
                    @endforeach
                    </tbody><!-- end tbody -->
                </table><!-- end table -->
            </div>
        </div>
    </div> <!-- .card-->
</div> <!-- .col-->
