<div class="col-xl-3 col-md-6">
    <!-- card -->
    <div class="card card-animate">
        <div class="card-body">
            <div class="d-flex align-items-center">
                <div class="flex-grow-1 overflow-hidden">
                    <p
                        class="text-uppercase fw-medium text-muted text-truncate mb-0">
                        @lang("translation.my-account")</p>
                </div>
                <div class="flex-shrink-0">
                    <h5 class="text-muted fs-14 mb-0">
                        +0.00 %
                    </h5>
                </div>
            </div>
            <div class="d-flex align-items-end justify-content-between mt-4">
                <div>
                    <h4 class="fs-22 fw-semibold ff-secondary mb-4">$<span
                            class="counter-value" data-target="{{ Auth()->user()->bonus }}">0</span>k
                    </h4>
                    <a href=""
                       class="text-decoration-underline">@lang('translation.withdraw-money')</a>
                </div>
                <div class="avatar-sm flex-shrink-0">
                                    <span class="avatar-title bg-soft-primary rounded fs-3">
                                        <i class="bx bx-wallet text-primary"></i>
                                    </span>
                </div>
            </div>
        </div><!-- end card body -->
    </div><!-- end card -->
</div><!-- end col -->
</div> <!-- end row-->
