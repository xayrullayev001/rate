<!-- ORDER_CREATED start item -->
@if ($action->type == App\Enums\TrackingActionTypeEnum::ORDER_CREATED->name)
    <div class="acitivity-item d-flex">
        @if ($params = json_decode("{$action->params}", true))
            <div class="flex-shrink-0">
                <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                     alt="" class="avatar-xs rounded-circle acitivity-avatar"/>
            </div>
            <div class="flex-grow-1 ms-3">
                <h6 class="mb-1">
                    {{ $params['user']['name'] }}
                    <span class="badge bg-soft-primary text-primary align-middle">
                         @lang('translation.'.$action->type)
                    </span>
                </h6>
                <p class="text-muted mb-2">
                    {{ __($action->msg, $params['attachments']) }}
                </p>
                <small class="mb-0 text-muted">
                    {{ $action->created_at }}
                </small>
            </div>
        @endif
    </div>
@endif
<!-- ORDER_CREATED end item -->
