<!-- COMPLETED start item -->
@if ($action->type == App\Enums\TrackingActionTypeEnum::COMPLETED->name)
    <div class="acitivity-item py-3 d-flex">
        @if ($params = json_decode("{$action->params}", true))
            <div class="flex-shrink-0">
                <img src="{{ asset("{$params['user']['avatar']}") }}"
                     alt="" class="avatar-xs rounded-circle acitivity-avatar"/>
            </div>
            <div class="flex-grow-1 ms-3">
                <h6 class="mb-1">
                    {{ $params['user']['name'] }}
                    <span class="badge bg-soft-success text-success align-middle">
                       @lang('translation.'.$action->type)
                    </span>
                </h6>
                <p class="text-muted mb-2">
                    {{ __(\App\Enums\TrackingActionTypeEnum::COMPLETED->value, [$params['attachments'][0], $params['attachments'][1]]) }}
                </p>
                <div class="col-xxl-4 col-lg-6">
                    <div class="border rounded border-dashed p-2">
                        <div class="d-flex align-items-center">
                            <div class="flex-shrink-0 me-3">
                                <div class="avatar-sm">
                                    <div
                                        class="avatar-title bg-light text-secondary rounded fs-24">
                                        <i class="ri-folder-zip-line"></i>
                                    </div>
                                </div>
                            </div>
                            <div class="flex-grow-1 overflow-hidden">
                                <h5 class="fs-13 mb-1">
                                    <span class="text-body text-truncate d-block">
                                        {{ $params['attachments'][3] }}
                                    </span>
                                </h5>
                                <div>
                                    <span class="text-body text-truncate d-block">
                                        {{ $params['attachments'][5] }}
                                    </span>
                                </div>
                            </div>
                            <div class="flex-shrink-0 ms-2">
                                <div class="d-flex gap-1">
                                    <a href="{{ route('download', [$params['attachments'][2]]) }}"
                                       class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                        <i class="ri-download-2-line"></i>
                                    </a>
                                    <a href="{{ Storage::url($params['attachments'][4]) }}"
                                       class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                        <i class="ri-fullscreen-line"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <small class="mb-0 text-muted">
                    {{ $action->created_at }}
                </small>
            </div>
        @endif
    </div>
@endif
<!-- COMPLETED end item -->
