<!-- MEMBERS_ADDED start item -->
@if ($action->type == App\Enums\TrackingActionTypeEnum::MEMBERS_ADDED->name)
    <div class="acitivity-item py-3 d-flex">
        @if ($params = json_decode("{$action->params}", true))
            <div class="flex-shrink-0 avatar-xs acitivity-avatar">
            <div class="avatar-title bg-soft-success text-success rounded-circle">
                <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                     alt="" class="avatar-xs rounded-circle acitivity-avatar"/>
            </div>
        </div>
        <div class="flex-grow-1 ms-3">
            <h6 class="mb-1">
                {{ $params['user']['name'] }}
                <span class="badge bg-soft-secondary text-secondary align-middle">
                  @lang('translation.'.$action->type)
                </span>
            </h6>
            <p class="text-muted mb-2">
                <i class="ri-file-text-line align-middle ms-2"></i>
                {{ __("{$action->msg}") }}
            </p>
            <div class="avatar-group mb-2">
                @if ($params = json_decode("{$action->params}", true))
                    @foreach ($params['attachments'] as $key => $value)
                        <a href="{{ route('user.show', [$value[0]]) }}"
                           class="avatar-group-item" data-bs-toggle="tooltip"
                           data-bs-placement="top" title="{{ $value[1] }}"
                           data-bs-original-title="{{ $value[1] }}">
                            <img src="{{ URL::asset("{$value[2]}") }}" alt=""
                                 class="rounded-circle avatar-xs"/>
                        </a>
                    @endforeach
                @endif
            </div>
            <small class="mb-0 text-muted">{{ $action->created_at }}</small>
        </div>
        @endif
    </div>
@endif
<!-- MEMBERS_ADDED end item -->
