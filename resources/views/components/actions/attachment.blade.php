<!-- ATTACHMENTS start item -->
@if ($action->type == App\Enums\TrackingActionTypeEnum::ATTACHMENTS->name)
    <div class="acitivity-item py-3 d-flex">
        @if ($params = json_decode("{$action->params}", true))
            <div class="flex-shrink-0">
                <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                     alt="" class="avatar-xs rounded-circle acitivity-avatar"/>
            </div>
            <div class="flex-grow-1 ms-3">
                <h6 class="mb-1">
                    {{ $params['user']['name'] }}
                    <span class="badge bg-soft-success text-success align-middle">
                        {{ \App\Enums\TrackingActionTypeEnum::ATTACHMENTS->name }}
                    </span>
                </h6>
                <p class="text-muted mb-2">
                    @lang('translation.'.$action->type)
                </p>
                <div class="row">
                    <div class="col-xxl-4">
                        <div class="row border border-dashed gx-2 p-2 mb-2">
                            @foreach ($params['attachments'] as $attachedItem)
                                <div class="col-4">
                                    <img src="{{ URL::asset("{$attachedItem}") }}"
                                         alt="" class="img-fluid rounded"/>
                                </div>
                        @endforeach
                        <!--end col-->
                        </div>
                        <!--end row-->
                    </div>
                </div>
                <small class="mb-0 text-muted">
                    {{ $action->created_at }}
                </small>
            </div>
        @endif
    </div>
@endif
<!-- ATTACHMENTS end item -->
