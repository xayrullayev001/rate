<!-- START: ORDER_UPDATED -->
@if ($action->type == App\Enums\TrackingActionTypeEnum::ORDER_UPDATED->name)
    @if ($params = json_decode("{$action->params}", true))
        <div class="acitivity-item py-3 d-flex">
            <div class="flex-shrink-0 avatar-xs acitivity-avatar">
                <div
                    class="avatar-title bg-soft-success text-success rounded-circle">
                    <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                         alt=""
                         class="avatar-xs rounded-circle acitivity-avatar"/>
                </div>
            </div>
            <div class="flex-grow-1 ms-3">
                <h6 class="mb-1">
                    {{ $params['user']['name'] }}
                    <span
                        class="badge bg-soft-secondary text-secondary align-middle">
                                                            @lang('translation.'.$action->type)
                                                        </span>
                </h6>
                <p class="text-muted mb-2">
                    <i class="ri-file-text-line align-middle ms-2"></i>
                    {{ __("{$action->msg}", [$params['user']['name']]) }}
                </p>
                <div class="avatar-group mb-2">
                    <ul>
                        @foreach ($params['attachments'] as $key => $value)
                            <li> {{ $value }}</li>
                        @endforeach
                    </ul>
                </div>
                <small class="mb-0 text-muted">{{ $action->created_at }}</small>
            </div>
        </div>
    @endif
@endif
<!-- END: ORDER_UPDATED -->
