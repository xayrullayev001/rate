@extends('layouts.master')
@section('title')
    @lang('translation.settings')
@endsection
@section('content')
    <div class="position-relative mx-n4 mt-n4">
        <div class="profile-wid-bg profile-setting-img">
            <img src="{{ URL::asset('assets/images/profile-bg.jpg') }}" class="profile-wid-img" alt="">
            <div class="overlay-content">
                <div class="text-end p-3">
                    <div class="p-0 ms-auto rounded-circle profile-photo-edit">
                        <input id="profile-foreground-img-file-input" type="file"
                               class="profile-foreground-img-file-input"
                               data-url="{{ route('profile.files') }}" data-id="{{ auth()->id() }}" data-type="COVER"
                               data-csrf="{{ csrf_token() }}">
                        <label for="profile-foreground-img-file-input" class="profile-photo-edit btn btn-light">
                            <i class="ri-image-edit-line align-bottom me-1"></i> Change Cover
                        </label>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xxl-3">
            <div class="card mt-n5">
                <div class="card-body p-4">
                    <div class="text-center">
                        <div class="profile-user position-relative d-inline-block mx-auto  mb-4">
                            <img
                                src="@if (auth()->user()->avatar != '') {{ URL::asset( auth()->user()->avatar) }}@else{{ URL::asset('assets/images/users/avatar-1.jpg') }} @endif"
                                class="  rounded-circle avatar-xl img-thumbnail user-profile-image"
                                alt="user-profile-image">
                            <div class="avatar-xs p-0 rounded-circle profile-photo-edit">
                                <input id="profile-img-file-input" type="file" class="profile-img-file-input"
                                       data-url="{{ route('profile.files') }}" data-id="{{ auth()->id() }}"
                                       data-type="AVATAR">
                                <label for="profile-img-file-input" class="profile-photo-edit avatar-xs">
                                    <span class="avatar-title rounded-circle bg-light text-body">
                                        <i class="ri-camera-fill"></i>
                                    </span>
                                </label>
                            </div>
                        </div>
                        <h5 class="fs-16 mb-1">
                            {{ auth()->user()->name }}
                        </h5>
                        <p class="text-muted mb-0">
                            {{ auth()->user()->role }}
                        </p>
                    </div>
                </div>
            </div>
            <!--end card-->
            <div class="card">
                <div class="card-body">
                    <div class="d-flex align-items-center mb-5">
                        <div class="flex-grow-1">
                            <h5 class="card-title mb-0">Complete Your Profile</h5>
                        </div>
                        <div class="flex-shrink-0">
                            <a href="javascript:void(0);" class="badge bg-light text-primary fs-12"><i
                                    class="ri-edit-box-line align-bottom me-1"></i> Edit</a>
                        </div>
                    </div>
                    <div class="progress animated-progress custom-progress progress-label">
                        <div class="progress-bar bg-danger" role="progressbar" style="width: {{ $info->completed  }}%"
                             aria-valuenow="{{ $info->completed  }}"
                             aria-valuemin="0" aria-valuemax="100">
                            <div class="label">{{ $info->completed  }}%</div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end card-->
        </div>
        <!--end col-->
        <div class="col-xxl-9">
            <div class="card mt-xxl-n5">
                <div class="card-header">
                    <ul class="nav nav-tabs-custom rounded card-header-tabs border-bottom-0" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active " href="{{ route('profile.info.edit') }}">
                                <i class="fas fa-home"></i>
                                Personal Details
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('profile.edit') }}">
                                <i class="far fa-user"></i>
                                Change Password
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="card-body p-4">
                    <div class="tab-content">
                        <div class="tab-pane active" id="personalDetails" role="tabpanel">
                            <form action="{{ route('profile.info.update') }}" method="post">
                                @csrf
                                @method('put')
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="firstnameInput" class="form-label">
                                                First Name
                                            </label>
                                            <input type="text" class="form-control" id="firstnameInput"
                                                   placeholder="Enter your firstname" name="first_name"
                                                   value="{{ $info->first_name }}">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="lastnameInput" class="form-label">Last
                                                Name</label>
                                            <input type="text" class="form-control" id="lastnameInput"
                                                   placeholder="Enter your lastname" name="last_name"
                                                   value="{{ $info->last_name }}">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="cleave-phone" class="form-label">
                                                Phone Number</label>
                                            <input type="text" disabled class="form-control" id="cleave-phone"
                                                   value="{{ auth()->user()->phone }}">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="emailInput" class="form-label">
                                                Email Address
                                            </label>
                                            <input type="email" class="form-control" id="emailInput"
                                                   placeholder="Enter your email" name="email" value="{{ $info->email }}">
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="addressInput" class="form-label">
                                                Email Address
                                            </label>
                                            <input type="address" class="form-control" id="addressInput"
                                                   placeholder="Enter your address" name="address" value="{{ $info->address }}">
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-6">
                                        <div class="mb-3">
                                            <label for="skillsInput" class="form-label">
                                                Email skills
                                            </label>
                                            <input type="skills" class="form-control" id="skillsInput"
                                                   placeholder="Enter your skills" name="skills" value="{{ $info->skills }}">
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-12">
                                        <div class="mb-3">
                                            <label for="companyInput" class="form-label">
                                                Company
                                            </label>
                                            <input type="company" class="form-control" id="companyInput"
                                                   placeholder="Enter your company" name="company" value="{{ $info->company }}">
                                        </div>
                                    </div>
                                    <!--end col-->

                                    <div class="col-lg-12">
                                        <div class="mb-3 pb-2">
                                            <label for="description"
                                                   class="form-label">About yourself</label>
                                            <textarea class="form-control" id="description"
                                                      placeholder="Enter your description" name="description"
                                                      rows="3">{{ $info->description }}</textarea>
                                        </div>
                                    </div>
                                    <!--end col-->
                                    <div class="col-lg-12">
                                        <div class="hstack gap-2 justify-content-end">
                                            <button type="submit" class="btn btn-primary">Updates</button>
                                            <button type="button" class="btn btn-soft-success">Cancel</button>
                                        </div>
                                    </div>
                                    <!--end col-->
                                </div>
                                <!--end row-->
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--end col-->
    </div>
    <!--end row-->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script>
        // if (document.querySelector("#profile-foreground-img-file-input")) {
        //     var foreground = document.querySelector("#profile-foreground-img-file-input");
        //     foreground.addEventListener("change", function(data, value) {
        //         var preview = document.querySelector(".profile-wid-img");
        //         var file = document.querySelector(".profile-foreground-img-file-input").files[0];
        //         var reader = new FileReader();
        //         reader.addEventListener("load", function() {
        //             preview.src = reader.result;
        // var formData = new FormData();
        // formData.append("file_type", foreground.dataset.type);
        // formData.append("user_id", foreground.dataset.id);
        // formData.append("file", reader.result);
        // console.table(formData);

        // var xhttp = new XMLHttpRequest();
        // xhttp.onreadystatechange = function(response) {
        //     if (this.readyState == 4 && this.status == 200) {
        //         // Typical action to be performed when the document is ready:
        //         preview.src = reader.result;
        //         console.log(response);
        //     }
        // };
        //     console.table(foreground.dataset);
        //     xhttp.open("POST", foreground.dataset.url, true);
        //     xhttp.setRequestHeader('X-CSRF-TOKEN', foreground.dataset.csrf);
        //     xhttp.send(formData);
        // }, function(e) {
        //     console.log(e);
        // });


        // if (file) {
        //     reader.readAsDataURL(file);
        // }
        // });
        //} // Profile Foreground Img
    </script>
@endsection
