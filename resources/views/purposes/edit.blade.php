@extends('layouts.master')
@section('title')
    @lang('translation.edit-purpose')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.purpose')
        @endslot
        @slot('title')
            @lang('translation.edit-purpose')

        @endslot
    @endcomponent
    <form action="{{ route('purposes.update',['purpose'=>$purpose->id]) }}" method="post" autocomplete="off" class="needs-validation" validate>
        @csrf
        @method('put')
        <div class="row">
            <div class="col-lg-12">
                <div class="card">
                    <div class="card-header">
                        @lang('translation.purpose')
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-number-input">
                                    @lang('translation.uz')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('uz') is-invalid @enderror"
                                       name="uz" placeholder="@lang('translation.uz')"
                                       id="order-number-input" value="{{ $purpose->uz }}"
                                       tabindex="1">
                                @error('uz')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.uz')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-number-input">
                                    @lang('translation.ru')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('ru') is-invalid @enderror"
                                       name="ru" placeholder="@lang('translation.ru')"
                                       id="order-number-input" value="{{ $purpose->ru }}"
                                       tabindex="2">
                                @error('ru')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.ru')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-number-input">
                                    @lang('translation.cr')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('cr') is-invalid @enderror"
                                       name="cr" placeholder="@lang('translation.cr')"
                                       id="order-number-input" value="{{ $purpose->cr }}"
                                       tabindex="3">
                                @error('cr')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.cr')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-lg-4 col-sm-4 col-md-4">
                            <div class="text-start mb-3 btn-block">
                                <button type="submit" class="btn btn-success w-sm" name="save"
                                        tabindex="4"> @lang('translation.save') </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
    </form>
@endsection
