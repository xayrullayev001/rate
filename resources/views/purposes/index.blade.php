@extends('layouts.master')
@section('title')
    @lang('translation.purposes')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.purposes')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-1">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.purposes')</h5>
                        <div class="flex-shrink-0 ">
                            <a class="btn btn-ghost-primary bg-primary text-white"
                               href="{{ route('purposes.create') }}">  @lang('translation.create-purpose') </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table class="table table-borderless table-hover align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">@lang('translation.uz')</th>
                                <th scope="col">@lang('translation.ru')</th>
                                <th scope="col">@lang('translation.cr')</th>
                                <th scope="col">@lang('translation.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($purposes as $purpose)
                                <tr>
                                    <td class="fw-medium"> {{$loop->iteration}}</td>
                                    <td>
                                        {{  $purpose->uz }}
                                    </td>
                                    <td>
                                        {{  $purpose->ru }}
                                    </td>
                                    <td>
                                        {{  $purpose->cr }}
                                    </td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            {{--                                            <a href="{{ route('purposes.show', [$purpose->id]) }}"--}}
                                            {{--                                               title="@lang("translation.view-information")" data-bs-toggle="tooltip"--}}
                                            {{--                                               data-bs-placement="top"--}}
                                            {{--                                               class="link-success">--}}
                                            {{--                                                <i class="ri-eye-line align-middle"></i>--}}
                                            {{--                                            </a>--}}
                                            <a href="{{ route('purposes.edit', ['purpose'=>$purpose->id]) }}"
                                               title="@lang("translation.edit-information")" data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               class="link-warning">
                                                <i class="ri-pencil-line align-middle"></i>
                                            </a>
                                            @component('components.delete-btn')
                                                @slot('url')
                                                    {{ route('purposes.destroy', ['purpose' => $purpose->id]) }}
                                                @endslot
                                            @endcomponent
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
