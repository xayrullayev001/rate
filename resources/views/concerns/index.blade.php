@extends('layouts.master')
@section('title')
    @lang('translation.concerns')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.concerns')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-1">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.concerns')</h5>
                        <div class="flex-shrink-0 ">
                            <a class="btn btn-ghost-primary bg-primary text-white"
                               href="{{ route('concerns.create') }}">  @lang('translation.create-concern') </a>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive ">
                        <table class="table table-borderless table-hover align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">@lang('translation.uz')</th>
                                <th scope="col">@lang('translation.ru')</th>
                                <th scope="col">@lang('translation.cr')</th>
                                <th scope="col">@lang('translation.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($concerns as $concern)
                                <tr>
                                    <td class="fw-medium"> {{$loop->iteration}}</td>
                                    <td>
                                        {{  $concern->uz }}
                                    </td>
                                    <td>
                                        {{  $concern->ru }}
                                    </td>
                                    <td>
                                        {{  $concern->cr }}
                                    </td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <a href="{{ route('concerns.edit', ['concern'=>$concern->id]) }}"
                                               title="@lang("translation.edit-information")" data-bs-toggle="tooltip"
                                               data-bs-placement="top"
                                               class="link-warning">
                                                <i class="ri-pencil-line align-middle"></i>
                                            </a>
                                            @component('components.delete-btn')
                                                @slot('url')
                                                    {{ route('concerns.destroy', ['concern' => $concern->id]) }}
                                                @endslot
                                            @endcomponent
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
