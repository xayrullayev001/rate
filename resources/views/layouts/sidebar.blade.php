<!-- ========== App Menu ========== -->
<div class="app-menu navbar-menu">
    <!-- LOGO -->
    <div class="navbar-brand-box p-3">
        <!-- Dark Logo-->
    {{--        <a href="index" class="logo logo-dark">--}}
    {{--            <span class="logo-sm">--}}
    {{--                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="" height="22">--}}
    {{--            </span>--}}
    {{--            <span class="logo-lg">--}}
    {{--                <img src="{{ URL::asset('assets/images/logo-dark.png') }}" alt="" height="17">--}}
    {{--            </span>--}}
    {{--        </a>--}}
    <!-- Light Logo-->
        {{--        <a href="index" class="logo logo-light">--}}
        {{--            <span class="logo-sm">--}}
        {{--                <img src="{{ URL::asset('assets/images/logo-sm.png') }}" alt="" height="22">--}}
        {{--            </span>--}}
        {{--            <span class="logo-lg">--}}
        {{--                <img src="{{ URL::asset('assets/images/logo-light.png') }}" alt="" height="17">--}}
        {{--            </span>--}}
        {{--        </a>--}}
        <button type="button" class="btn btn-sm p-0 fs-20 header-item float-end btn-vertical-sm-hover"
                id="vertical-hover">
            <i class="ri-record-circle-line"></i>
        </button>
    </div>

    <div id="scrollbar">
        <div class="container-fluid">

            <div id="two-column-menu">
            </div>
            <ul class="navbar-nav" id="navbar-nav">
                <li class="menu-title"><span>@lang('translation.menu')</span></li>
                <li class="nav-item">
                    <a href="{{ route('index') }}"
                       class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "index") active @endif">
                        <i class="ri-dashboard-2-line"></i> <span>@lang('translation.dashboards')</span>
                    </a>
                </li>
                <li class="menu-title">
                    <span>@lang('translation.conclusions')</span>
                </li>
                <li class="nav-item">
                    <a href="{{ route('auto.index') }}"
                       class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "auto.index") active @endif">
                        <i class="ri-car-line"></i> <span>@lang('translation.auto') </span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('estate.index') }}"
                       class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "estate.index") active @endif">
                        <i class=" ri-community-line"></i> <span>@lang('translation.estate')</span>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('tools.index') }}"
                       class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "tools.index") active @endif">
                        <i class="ri-tools-line"></i> <span>@lang('translation.tools')</span>
                    </a>
                </li>
                <li class="menu-title"><span>@lang('translation.cabinet')</span></li>
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="nav-item">
                        <a href="{{ route('bonus.index') }}"
                           class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "bonus.index") active @endif">
                            <i class=" ri-coins-line"></i> <span>@lang('translation.bonuses')</span>
                        </a>
                    </li>
                @endif
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="nav-item">
                        <a href="{{ route('diller.index') }}"
                           class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "diller.index") active @endif">
                            <i class="ri-map-pin-user-line"></i> <span>@lang('translation.dillers')</span>
                        </a>
                    </li>
                @endif
                @if(Auth::user()->role!=\App\Enums\RoleEnum::USER->name)
                    <li class="nav-item">
                        <a href="{{ route('debit.index') }}"
                           class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "debit.index") active @endif">
                            <i class="ri-hand-coin-fill"></i> <span>@lang('translation.debit')</span>
                        </a>
                    </li>
                @endif
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="menu-title"><span>@lang('translation.adminstration')</span></li>
                    <li class="nav-item">
                        <a href="{{ route('user.appraisers') }}"
                           class="nav-link link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "user.appraisers") active @endif">
                            <i class="ri-auction-line"></i> <span>@lang('translation.appraisers')</span>
                        </a>
                    </li>
                @endif
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="nav-item">
                        <a href="{{ route('user.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "user.index") active @endif">
                            <i class="ri-file-user-line"></i> <span>@lang('translation.users')</span>
                        </a>
                    </li>
                @endif
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="nav-item">
                        <a href="{{ route('role.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "role.index") active @endif">
                            <i class="ri-shield-user-line"></i> <span>@lang('translation.roles')</span>
                        </a>
                    </li>

                @endif


                {{--                    <li class="nav-item">--}}
                {{--                        <a class="nav-link menu-link" href="#notification" data-bs-toggle="collapse" role="button"--}}
                {{--                           aria-expanded="false" aria-controls="notification">--}}
                {{--                            <i class="ri-chat-settings-line"></i> <span>@lang('translation.notification')</span>--}}
                {{--                        </a>--}}
                {{--                        <div class="collapse menu-dropdown" id="notification">--}}
                {{--                            <ul class="nav nav-sm flex-column">--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('notification.template.index') }}"--}}
                {{--                                       class="nav-link">@lang('translation.notification-template')</a>--}}
                {{--                                </li>--}}
                {{--                                <li class="nav-item">--}}
                {{--                                    <a href="{{ route('notification.type.index') }}"--}}
                {{--                                       class="nav-link">@lang('translation.notification-type')</a>--}}
                {{--                                </li>--}}
                {{--                            </ul>--}}
                {{--                        </div>--}}
                {{--                    </li>--}}
                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                    <li class="menu-title"><span>@lang('translation.settings')</span></li>
                    <li class="nav-item">
                        <a href="{{ route('certificates.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "certificates.index") active @endif">
                            <i class=" ri-profile-line"></i>
                            <span>@lang('translation.certificates')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('purposes.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "purposes.index") active @endif">
                            <i class=" ri-profile-line"></i>
                            <span>@lang('translation.purposes')</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('regions.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "regions.index") active @endif">
                            <i class=" ri-profile-line"></i>
                            <span>@lang('translation.regions')</span>
                        </a>
                    </li>
                    {{--                                <li class="nav-item">--}}
                    {{--                                    <a href="{{ route('districts.index') }}"--}}
                    {{--                                       class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "districts.index") active @endif">--}}
                    {{--                                        <i class=" ri-profile-line"></i>--}}
                    {{--                                        <span>@lang('translation.districts')</span>--}}
                    {{--                                    </a>--}}
                    {{--                                </li>--}}
                    <li class="nav-item">
                        <a href="{{ route('concerns.index') }}"
                           class="nav-link @if(\Illuminate\Support\Facades\Route::currentRouteName() == "concerns.index") active @endif">
                            <i class=" ri-profile-line"></i>
                            <span>@lang('translation.concerns')</span>
                        </a>

                @endif
                <li class="menu-title"><span></span></li>
            </ul>
        </div>
        <!-- Sidebar -->
    </div>
    <div class="sidebar-background"></div>
</div>
<!-- Left Sidebar End -->
<!-- Vertical Overlay-->
<div class="vertical-overlay"></div>
