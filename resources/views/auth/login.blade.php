@extends('layouts.master-without-nav')
@section('title')
    @lang('translation.sign-in')
@endsection
@section('content')
    <div class="auth-page-wrapper pt-5">
        <!-- auth page bg -->
        <div class="auth-one-bg-position auth-one-bg" id="auth-particles">
            <div class="bg-overlay"></div>

            <div class="shape">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                     viewBox="0 0 1440 120">
                    <path d="M 0,36 C 144,53.6 432,123.2 720,124 C 1008,124.8 1296,56.8 1440,40L1440 140L0 140z"></path>
                </svg>
            </div>
        </div>

        <!-- auth page content -->
        <div class="auth-page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mt-sm-5 mb-4 text-white-50">
{{--                            <div>--}}
{{--                                <a href="index" class="d-inline-block auth-logo">--}}
{{--                                    <img src="{{ URL::asset('assets/images/logo-light.png')}}" alt="" height="20">--}}
{{--                                </a>--}}
{{--                            </div>--}}
                            <p class="mt-3 fs-15 fw-medium">
                                @lang('translation.login')
                            </p>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row justify-content-center">
                    <div class="col-md-8 col-lg-6 col-xl-5">
                        <div class="card mt-4">

                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <h5 class="text-primary">Welcome Back !</h5>
                                    <p class="text-muted">@lang('translation.login-to-start')</p>
                                </div>
                                <div class="p-2 mt-4">
                                    <form action="{{ route('login') }}" method="POST" class="auth-pass-inputgroup">
                                        @csrf
                                        <div class="mb-3">
                                            @if ($errors->has('phone'))
                                                <div class="alert alert-danger d-block">
                                                    <ul>
                                                        @foreach ($errors->all() as $error)
                                                            <li>{{ __($error) }}</li>
                                                        @endforeach
                                                    </ul>
                                                </div>
                                            @endif

                                            <label for="phone" class="form-label">
                                                @lang('translation.phone')
                                            </label>
                                            <input type="text"
                                                   class="form-control cleave-phone "
                                                   id="cleave-phone" name="phone"
                                                   placeholder="@lang('translation.enter-phone')">
                                        </div>

                                        <div class="mb-3">
                                            <label class="form-label"
                                                   for="password-input">@lang('translation.password')</label>
                                            <div class="position-relative auth-pass-inputgroup mb-3">
                                                <input type="password"
                                                       class="form-control pe-5"
                                                       name="password" placeholder="@lang('translation.enter-password')"
                                                       id="password-input">
                                                <button
                                                    class="btn btn-link position-absolute end-0 top-0 text-decoration-none text-muted"
                                                    type="button" id="password-addon">
                                                    <i class="ri-eye-fill align-middle"></i>
                                                </button>
                                            </div>
                                        </div>

                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" checked value="true"
                                                   id="auth-remember-check">
                                            <label class="form-check-label" for="auth-remember-check">
                                                @lang('translation.remember-me')
                                            </label>
                                        </div>

                                        <div class="mt-4">
                                            <button class="btn btn-success w-100" type="submit">
                                                @lang('translation.sign-in')
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/js/pages/password-addon.init.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/cleave-phone.js/cleave-phone.js.min.js') }}"></script>

    <script>
        if (document.querySelector("#cleave-phone")) {
        var cleaveBlocks2 = new Cleave('#cleave-phone', {
            prefix: '+998',
            delimiters: ['-', '-', '-', '-'],
            blocks: [4, 2, 3, 2, 2]
        });
        // var cleave = new Cleave('.cleave-phone', {
        //     phone: true,
        //     phoneRegionCode: 'uz',
        //     nationalPrefix: +998
        // });
        }
    </script>
@endsection
