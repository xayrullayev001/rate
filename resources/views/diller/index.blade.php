@extends('layouts.master')
@section('title')
    @lang('translation.dillers')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.dillers')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-1">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.dillers')</h5>
                        <div class="flex-shrink-0">
                            <form action="{{ route('diller.index') }}" method="get" autocomplete="off" novalidate>
                                @component('components.page-size',['size'=>$size])@endcomponent
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-borderless align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">@lang('translation.avatar')</th>
                                <th scope="col">@lang('translation.name')</th>
                                <th scope="col">@lang('translation.phone-number')</th>
                                <th scope="col">@lang('translation.registered-date')</th>
                                <th scope="col">@lang('translation.balance')</th>
                                <th scope="col">@lang('translation.status')</th>
                                <th scope="col">@lang('translation.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($dillers as $user)
                                <tr>
                                    <td class="fw-medium">{{($dillers->currentPage() - 1) * $dillers->perPage() + $loop->iteration}}</td>
                                    <td>
                                        <img src="{{ URL::asset($user->avatar) }}" alt="{{ $user->name }}"
                                             class="rounded-circle header-profile-user">
                                    </td>
                                    <td>
                                        <a class="text-decoration-underline"
                                           href="{{ route('user.show', [$user->id]) }}">
                                            {{ $user->name }}
                                        </a>
                                    </td>
                                    <td>
                                        @component('components.callable-phone', ['phone' => $user->phone])
                                        @endcomponent
                                    </td>
                                    <td>{{ $user->created_at }}</td>
                                    <td>
                                        @component('components.price', ['balance' => $user->balance])
                                        @endcomponent
                                    </td>
                                    <td class="text-uppercase">
                                        @if ($user->status == 'active')
                                            <button type="button"
                                                    class="btn btn-success btn-label waves-effect waves-light">
                                                <i class="ri-emotion-happy-line label-icon align-middle fs-16 me-2"></i>
                                                @lang('translation.'.$user->status)
                                            </button>
                                        @else
                                            <button type="button"
                                                    class="btn btn-danger btn-label waves-effect waves-light">
                                                <i
                                                    class="ri-emotion-unhappy-line label-icon align-middle fs-16 me-2"></i>
                                                @lang('translation.'.$user->status)
                                            </button>
                                        @endif
                                    </td>
                                    @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <form action="{{ route('user.activate', ['id' => $user->id]) }}"
                                                    method="POST">
                                                    @csrf
                                                    @method('put')
                                                    <button type="submit" data-bs-toggle="tooltip"
                                                        data-bs-placement="bottom"
                                                        class="btn btn-outline-primary btn-icon waves-effect waves-light"
                                                        title="Setting user status is active"
                                                    >
                                                        <i class="ri-emotion-happy-line"></i>
                                                        <!-- Buttons with Label -->
                                                    </button>
                                                </form>

                                                <form action="{{ route('user.block', ['id' => $user->id]) }}"
                                                      method="POST">
                                                    @csrf
                                                    @method('put')
                                                    <button type="submit"
                                                            class="btn btn-outline-info btn-icon waves-effect waves-light"
                                                            data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                            title="Setting user status is block">
                                                        <i class=" ri-emotion-unhappy-line"></i>
                                                    </button>
                                                </form>
                                            <a href="{{ route('user.show', [$user->id]) }}"
                                               data-bs-toggle="tooltip" data-bs-placement="bottom"
                                               title="View user profile page"
                                               class="btn btn-outline-warning btn-icon waves-effect waves-light">
                                                <i class="ri-eye-2-line"></i>
                                            </a>
                                            <span class="btn btn-outline-info btn-icon waves-effect waves-light">
                                                @component('components.delete-btn')
                                                    @slot('url')
                                                        {{ route('diller.destroy', [$user->id]) }}
                                                    @endslot
                                                @endcomponent
                                            </span>
                                        </div>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $dillers->withQueryString()->links() }}</div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
@endsection

@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
