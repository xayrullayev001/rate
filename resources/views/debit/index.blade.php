@extends('layouts.master')
@section('title')
    @lang('translation.debit')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.debit')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-body border border-dashed border-end-0 border-start-0">
                    <form autocomplete="false">
                        <div class="row g-3">
                            <form action="{{ route('debit.index') }}" method="get" autocomplete="off" novalidate>
                                <div class="col-xxl-4 col-sm-4">
                                    <div class="search-box">
                                        <input type="text" class="form-control search" name="search"
                                               value="{{ isset($filters['search']) ? $filters['search'] : '' }}"
                                               placeholder="@lang('translation.search-by-number-customer-owner')">
                                        <i class="ri-search-line search-icon"></i>
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-xxl-2 col-sm-6">
                                    <div>
                                        <input type="text" class="form-control flatpickr-input"
                                               data-provider="flatpickr"
                                               name="period" value="{{ $filters['period'] ?? '' }}"
                                               data-date-format="d.m.Y"
                                               data-range-date="true" id="demo-datepicker"
                                               placeholder="@lang('translation.select-period')" readonly="readonly">
                                    </div>
                                </div>
                                <!--end col-->
                                <div class="col-xxl-2 col-sm-4">
                                    <div>
                                        <select data-choices data-choices-search-false onchange="this.form.submit()"
                                                data-choices-multiple-groups="false" class="form-control" name="status">
                                            <option value="" selected disabled>
                                                @lang('translation.select-status')</option>
                                            <option value="nopaid">
                                                @lang('translation.nopaid')
                                            </option>
                                            <option value="paid">
                                                @lang('translation.paid')
                                            </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-xxl-2 col-sm-4">
                                    @if (Auth::user()->role != App\Enums\RoleEnum::DILLER->name)
                                        <div>
                                            <select data-choices data-choices-search-false
                                                    data-choices-multiple-groups="false" class="form-control"
                                                    name="appraiser" onchange="this.form.submit()">
                                                <option value="">@lang('translation.select-appraiser')</option>
                                                @foreach ($appraisers as $appraiser)
                                                    <option value="{{ $appraiser->id }}"
                                                        {{ isset($filters['appraiser']) && isset($filters['appraiser']) && $appraiser->name === $filters['appraiser'] ? 'selected' : '' }}>
                                                        {{ $appraiser->name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    @endif
                                </div>
                                <!--end col-->

                                <div class="col-xxl-1 col-sm-2">
                                    @component('components.page-size',['size' => $size])@endcomponent
                                </div>
                                <!--end col-->
                                <div class="col-xxl-1 col-sm-2">
                                    <div>
                                        <button type="submit" class="btn btn-primary w-100">
                                            <i class="ri-equalizer-fill me-1 align-bottom"></i>
                                        </button>
                                    </div>
                                </div>
                                <!--end col-->
                            </form>
                        </div>
                        <!--end row-->
                    </form>
                </div>
                <div class="card-body">
                    <div class="live-preview">
                        <div class="table-responsive">
                            <table class="table table-borderless align-middle table-nowrap mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">@lang('translation.order-number')</th>
                                    <th scope="col">@lang('translation.customer')</th>
                                    <th scope="col">@lang('translation.cost')</th>
                                    <th scope="col">@lang('translation.created-date')</th>
                                    <th scope="col">@lang('translation.due-date')</th>
                                    <th scope="col">@lang('translation.appraiser')</th>
                                    <th scope="col">@lang('translation.status')</th>
                                    <th scope="col">@lang('translation.actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($debits as $debit)
                                    <tr @if($debit->expired < date('d.m.Y',time()) && $debit->status=='nopaid') class="bg-danger" @endif>
                                        <td>

                                            {{ ($debits->currentPage() - 1) * $debits->perPage() + $loop->iteration }}
                                        </td>
                                        <td class="fw-medium">
                                            <a href="@if(\App\Enums\OrderTypeEnum::AUTO->name==$debit->order_type) {{route('auto.show', [$debit->order_id])}} @else {{route('estate.show', [$debit->order_id])}} @endif">
                                                {{ $debit->order->number }}
                                            </a>
                                        </td>
                                        <td>{{ $debit->customer }}</td>
                                        <td>
                                            @component('components.price', ['balance' => $debit->cost])
                                            @endcomponent
                                        </td>
                                        <td>{{ $debit->created_at }}</td>
                                        <td>{{ $debit->expired }}</td>
                                        <td>
                                            <a class="text-decoration-underline"
                                               href="{{ route('user.show',[$debit->appraiser->id]) }}">
                                                {{ $debit->appraiser->name }}
                                            </a>
                                        </td>
                                        <td>
                                            @if($debit->status=='paid')
                                                <span class="badge badge-gradient-warning">
                                                @lang('translation.paid')
                                            </span>
                                            @else
                                                <span class="badge badge-soft-success">
                                                @lang('translation.nopaid')
                                            </span>
                                            @endif
                                        </td>
                                        @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                        <td>
                                            <div class="hstack gap-3 fs-15">
                                                <form method="post"
                                                      action="{{ route('debit.update', ['debit' => $debit->id]) }}">
                                                    @method('put')
                                                    @csrf
                                                    <button type="submit"
                                                            data-bs-toggle="tooltip"
                                                            data-bs-placement="bottom"
                                                            title="To'langan qilib belgilash"
                                                            class="btn btn-outline-warning btn-icon waves-effect waves-light">
                                                        <i class="ri-pencil-line"></i>
                                                    </button>
                                                </form>
                                            </div>
                                        </td>
                                        @endif
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {{ $debits->withQueryString()->links() }}

                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
        @endsection
        @section('script')
            <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script>

            <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
