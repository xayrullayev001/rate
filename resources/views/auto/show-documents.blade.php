@extends('layouts.master')
@section('title')
    @lang('translation.overview')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.order-breadcrumbs', ['order' => $order, 'active' => 'documents'])
    @endcomponent
    <!-- end row -->
    <div class="chat-wrapper d-lg-flex gap-1 mx-n4 mt-n4 p-1">
        <div class="file-manager-sidebar" data-simplebar>
            <div class="p-3 d-flex flex-column h-100">
                <div class="mb-3">
                    <h5 class="mb-0 fw-semibold">
                        @lang('translation.documents')
                    </h5>
                </div>
                <div class="search-box  my-4">
                    <input type="text" class="form-control bg-light border-light"
                           placeholder="@lang('translation.search-here')">
                    <i class="ri-search-2-line search-icon"></i>
                </div>
                <div class="mt-auto mx-n4 px-4">
                    <ul class="list-unstyled file-manager-menu">
                        <li>
                            <a data-bs-toggle="collapse" href="#collapseExample" role="button"
                               aria-expanded="{{ $type === 'ALL' ? 'true' : 'false' }}" aria-controls="collapseExample"
                               @if ($type === 'ALL') class="text-info" @endif>
                                <i class="ri-folder-2-line align-bottom me-2"></i>
                                <span class="file-list-link">
{{--                                     @lang(\App\Enums\MediaTypeEnum::ALL)--}}
                                </span>
                            </a>
                            <div class="collapse {{ $type === 'ALL' ? 'show' : 'false' }}" id="collapseExample">
                                <ul class="sub-menu list-unstyled">
                                    @foreach (\App\Enums\MediaTypeEnum::cases() as $allList)
                                        <li>
                                            @if ($allList->name == 'ALL')
                                                <a href="{{ route('auto.show-documents', [$order->id]) }}"
                                                   class="text-info">
                                                    @lang('translation.see',[__($allList->value)])
                                                </a>
                                            @else
                                                @lang($allList->value)
                                            @endif
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::PASSPORT_CUSTOMER->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::PASSPORT_CUSTOMER->name) class="text-info" @endif>
                                <i class="ri-file-list-2-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                    @lang(\App\Enums\MediaTypeEnum::PASSPORT_CUSTOMER->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::OBJECT_FILES->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::OBJECT_FILES->name) class="text-info" @endif>
                                <i class="ri-file-list-2-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::OBJECT_FILES->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::COMPARES->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::COMPARES->name) class="text-info" @endif>
                                <i class="ri-image-2-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                    @lang(\App\Enums\MediaTypeEnum::COMPARES->value)
                                </span>
                            </a>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::OBJECT_PHOTO->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::OBJECT_PHOTO->name) class="text-info" @endif>
                                <i class="ri-history-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                    @lang(\App\Enums\MediaTypeEnum::OBJECT_PHOTO->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::PARTICIPATE_CERTIFICATE->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::PARTICIPATE_CERTIFICATE->name) class="text-info" @endif>
                                <i class="ri-star-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::PARTICIPATE_CERTIFICATE->value)
                                </span>
                            </a>
                        </li>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::APPRAISER_CERTIFICATE->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::APPRAISER_CERTIFICATE->name) class="text-info" @endif>
                                <i class="ri-delete-bin-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::APPRAISER_CERTIFICATE->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::INSURANCE_POLICY->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::INSURANCE_POLICY->name) class="text-info" @endif>
                                <i class="ri-delete-bin-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::INSURANCE_POLICY->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::CERTIFICATE->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::CERTIFICATE->name) class="text-info" @endif>
                                <i class="ri-delete-bin-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::CERTIFICATE->value)
                                </span>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('auto.show-documents', [$order->id, 'type' => \App\Enums\MediaTypeEnum::ADDITIONAL->name]) }}"
                               @if ($type === \App\Enums\MediaTypeEnum::ADDITIONAL->name) class="text-info" @endif>
                                <i class="ri-delete-bin-line align-bottom me-2"></i>
                                <span class="file-list-link">
                                     @lang(\App\Enums\MediaTypeEnum::ADDITIONAL->value)
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="mt-auto mx-n4 px-4">
                    <form class="dropzone" id="create-file-form" action="{{ route('attach.files') }}" novalidate>
                    @csrf
                    <!-- end dropzon-preview -->
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <input type="hidden" name="order_type" value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}">
                        <input type="hidden" class="form-control" name="file_type" value="{{ $type }}">

                        <div class="dropzone">
                            <div class="fallback">
                                <input name="file" type="file" multiple="multiple">
                            </div>
                            <div class="dz-message needsclick">
                                <div class="mb-3">
                                    <i class="display-6 text-muted ri-upload-cloud-2-fill"></i>
                                </div>
                                <h5>Faylni bu yerga qo'ying yoki tanlang</h5>
                            </div>
                        </div>
                        <!-- end card body -->
                    </form>
                </div>
                {{-- @if ($type != 'ALL') --}}
                <div class="mt-auto mx-n4 px-4 pt-5">
                    <div class="mt-auto mx-n4 px-4">
                        <h6 class="fs-11 text-muted text-uppercase mb-3">
                            @lang('translation.storage-status')
                        </h6>
                        <div class="d-flex align-items-center">
                            <div class="flex-shrink-0">
                                <i class="ri-database-2-line fs-17"></i>
                            </div>
                            <div class="flex-grow-1 ms-3 overflow-hidden">
                                <div class="progress mb-2 progress-sm">
                                    <div class="progress-bar bg-success" role="progressbar" style="width: 25%"
                                         aria-valuenow="25" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                                <span class="text-muted fs-12 d-block text-truncate">
                                    @lang('translation.used-of',["<b>119</b>GB","<b>".\App\Models\File::getFileSizeInMB($sizeInStorage)."</b>"])
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- @endif --}}
            </div>
        </div>
        <div class="file-manager-content w-100 p-3 py-0">
            <div class="mx-n3 pt-4 px-4 file-manager-content-scrollss">
                <div>
                    <div class="d-flex align-items-center mb-3">
                        <h5 class="flex-grow-1 fs-16 mb-0" id="filetype-title">
                            @lang(\App\Enums\MediaTypeEnum::getLabel($type))
                        </h5>
                        <div class="flex-shrink-0">
                            <button class="btn btn-success createFile-modal" data-bs-toggle="modal"
                                    data-bs-target="#createFileModal"><i class="ri-add-line align-bottom me-1"></i>
                                @lang('translation.create-file')
                            </button>
                        </div>
                    </div>
                    <div class="mb-4">
                        <ul class="list-unstyled mb-0" id="dropzone-preview">
                            <li class="mt-2" id="dropzone-preview-list">
                                <!-- This is used as the file preview template -->
                                <div class="border rounded">
                                    <div class="d-flex p-2">
                                        <div class="flex-shrink-0 me-3">
                                            <div class="avatar-sm bg-light rounded">
                                                <img data-dz-thumbnail class="img-fluid rounded d-block" src="#"
                                                     alt="Dropzone-Image"/>
                                            </div>
                                        </div>
                                        <div class="flex-grow-1">
                                            <div class="pt-1">
                                                <h5 class="fs-14 mb-1" data-dz-name>&nbsp;</h5>
                                                <p class="fs-13 text-muted mb-0" data-dz-size></p>
                                                <strong class="error text-danger" data-dz-errormessage></strong>
                                            </div>
                                        </div>
                                        <div class="flex-shrink-0 ms-3">
                                            <button data-dz-remove
                                                    class="btn btn-sm btn-danger">@lang('translation.delete')</button>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="table-responsive" data-simplebar>
                        <table class="table align-middle table-nowrap mb-0">
                            <thead class="table-active">
                            <tr>
                                <th scope="col">@lang('translation.file-name')</th>
                                <th scope="col">@lang('translation.file-item')</th>
                                <th scope="col">@lang('translation.file-size')</th>
                                <th scope="col">@lang('translation.recent-date')</th>
                                <th scope="col" class="text-center">@lang('translation.actions')</th>
                            </tr>
                            </thead>
                            <tbody id="file-list">
                            @if(isset($certificates))
                                @foreach ($certificates as $certificate)
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 fs-17 me-2 filelist-icon">
                                                    <i
                                                        class="ri-file-pdf-fill align-bottom text-success"></i>
                                                </div>
                                                <div class="flex-grow-1 filelist-name">
                                                    <a href="{{ Storage::url($certificate->path) }}">
                                                        @lang( \App\Enums\MediaTypeEnum::getLabel($type))
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @lang( \App\Enums\MediaTypeEnum::getLabel($certificate->type) )
                                        </td>
                                        <td class="filelist-size">2 MB</td>
                                        <td class="filelist-create">{{ $certificate->created_at }}</td>
                                        <td>
                                            <div class="d-flex gap-3 justify-content-center">
                                                <button type="button"
                                                        class="btn btn-ghost-primary btn-icon btn-sm favourite-btn ">
                                                    <i class="ri-star-fill fs-13 align-bottom"></i>
                                                </button>
                                                <div class="dropdown">
                                                    <button class="btn btn-light btn-icon btn-sm dropdown" type="button"
                                                            data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="ri-more-fill align-bottom"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                        <li>
                                                            <a class="dropdown-item viewfile-list"
                                                               href="#">@lang('translation.view')</a>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item edit-list" href="#createFileModal"
                                                               data-bs-toggle="modal" data-edit-id="1"
                                                               role="button">@lang('translation.rename')</a>
                                                        </li>
                                                        <li class="dropdown-divider"></li>
                                                        <li>
                                                            @component('components.delete-btn')
                                                                @slot('url')
                                                                    {{ route('certificates.destroy', [$certificate->id]) }}
                                                                @endslot
                                                            @endcomponent
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @else
                                @foreach ($files as $file)
                                    <tr>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="flex-shrink-0 fs-17 me-2 filelist-icon">
                                                    <i
                                                        class="ri-file-{{ $file->extension }}-fill align-bottom text-success"></i>
                                                </div>
                                                <div class="flex-grow-1 filelist-name">
                                                    <a href="{{ Storage::url($file->path) }}">
                                                        {{ $file->name }}
                                                    </a>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @lang( \App\Enums\MediaTypeEnum::getLabel($file->type) )
                                        </td>
                                        <td class="filelist-size">{{ $file->size }}</td>
                                        <td class="filelist-create">{{ $file->created_at }}</td>
                                        <td>
                                            <div class="d-flex gap-3 justify-content-center">
                                                <button type="button"
                                                        class="btn btn-ghost-primary btn-icon btn-sm favourite-btn ">
                                                    <i class="ri-star-fill fs-13 align-bottom"></i>
                                                </button>
                                                <div class="dropdown">
                                                    <button class="btn btn-light btn-icon btn-sm dropdown" type="button"
                                                            data-bs-toggle="dropdown" aria-expanded="false">
                                                        <i class="ri-more-fill align-bottom"></i>
                                                    </button>
                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                        <li>
                                                            <a class="dropdown-item viewfile-list"
                                                               href="#">@lang('translation.view')</a>
                                                        </li>
                                                        <li>
                                                            <a class="dropdown-item edit-list" href="#createFileModal"
                                                               data-bs-toggle="modal" data-edit-id="1"
                                                               role="button">Rename</a>
                                                        </li>
                                                        <li class="dropdown-divider"></li>
                                                        <li>
                                                            @component('components.delete-btn')
                                                                @slot('url')
                                                                    {{ route('file.delete', [$file->id]) }}
                                                                @endslot
                                                            @endcomponent
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            @endif
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $files->withQueryString()->links() }}</div>
                </div>
            </div>
        </div>
        <!-- By default be open file manager detail modal-->
        <div class="file-manager-detail-content p-3 py-0">
            <div class="mx-n3 pt-3 px-3 file-detail-content-scroll" data-simplebar>
                <div id="folder-overview">
                    <div class="d-flex align-items-center pb-3 border-bottom border-bottom-dashed">
                        <h5 class="flex-grow-1 fw-semibold mb-0">@lang('translation.overview')</h5>
                        <div>
                            <button type="button" class="btn btn-soft-danger btn-icon btn-sm fs-16 close-btn-overview">
                                <i class="ri-close-fill align-bottom"></i>
                            </button>
                        </div>
                    </div>
                    <div id="simple_dount_chart"
                         data-colors='["--vz-info", "--vz-danger", "--vz-primary", "--vz-success"]'
                         class="apex-charts mt-3" dir="ltr"></div>
                    <div class="mt-4">
                        <ul class="list-unstyled vstack gap-4">
                            <li>
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded bg-soft-secondary text-secondary">
                                                <i class="ri-file-text-line fs-17"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <h5 class="mb-1 fs-15">Documents</h5>
                                        <p class="mb-0 fs-12 text-muted">{{ $countDocs }} files</p>
                                    </div>
                                    <b>27.01 GB</b>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded bg-soft-success text-success">
                                                <i class="ri-gallery-line fs-17"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <h5 class="mb-1 fs-15">Media</h5>
                                        <p class="mb-0 fs-12 text-muted">{{ $countMediaFiles }} files</p>
                                    </div>
                                    <b>20.87 GB</b>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded bg-soft-warning text-warning">
                                                <i class="ri-folder-2-line fs-17"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <h5 class="mb-1 fs-15">Projects</h5>
                                        <p class="mb-0 fs-12 text-muted">7 files</p>
                                    </div>
                                    <b>4.10 GB</b>
                                </div>
                            </li>
                            <li>
                                <div class="d-flex align-items-center">
                                    <div class="flex-shrink-0">
                                        <div class="avatar-xs">
                                            <div class="avatar-title rounded bg-soft-primary text-primary">
                                                <i class="ri-error-warning-line fs-17"></i>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="flex-grow-1 ms-3">
                                        <h5 class="mb-1 fs-15">Others</h5>
                                        <p class="mb-0 fs-12 text-muted">{{ $otherFiles }} files</p>
                                    </div>
                                    <b>33.54 GB</b>
                                </div>
                            </li>
                        </ul>
                    </div>
                    <div class="pb-3 mt-auto">
                        <div class="alert alert-danger d-flex align-items-center mb-0">
                            <div class="flex-shrink-0">
                                <i class="ri-cloud-line text-danger align-bottom display-5"></i>
                            </div>
                            <div class="flex-grow-1 ms-3">
                                <h5 class="text-danger fs-14">Upgrade to Pro</h5>
                                <p class="text-muted mb-2">Get more space for your...</p>
                                <button class="btn btn-sm btn-danger"><i class="ri-upload-cloud-line align-bottom"></i>
                                    Upgrade Now
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="file-overview" class="h-100">
                    <div class="d-flex h-100 flex-column">
                        <div class="d-flex align-items-center pb-3 border-bottom border-bottom-dashed mb-3 gap-2">
                            <h5 class="flex-grow-1 fw-semibold mb-0">File Preview</h5>
                            <div>
                                <button type="button" class="btn btn-ghost-primary btn-icon btn-sm fs-16 favourite-btn">
                                    <i class="ri-star-fill align-bottom"></i>
                                </button>
                                <button type="button"
                                        class="btn btn-soft-danger btn-icon btn-sm fs-16 close-btn-overview">
                                    <i class="ri-close-fill align-bottom"></i>
                                </button>
                            </div>
                        </div>

                        <div class="pb-3 border-bottom border-bottom-dashed mb-3">
                            <div class="file-details-box bg-light p-3 text-center rounded-3 border border-light mb-3">
                                <div class="display-4 file-icon">
                                    <i class="ri-file-text-fill text-secondary"></i>
                                </div>
                            </div>
                            <button type="button" class="btn btn-icon btn-sm btn-ghost-success float-end fs-16"><i
                                    class="ri-share-forward-line"></i></button>
                            <h5 class="fs-16 mb-1 file-name">html.docx</h5>
                            <p class="text-muted mb-0 fs-12"><span class="file-size">0.3 KB</span>, <span
                                    class="create-date">19 Apr, 2022</span></p>
                        </div>
                        <div>
                            <h5 class="fs-12 text-uppercase text-muted mb-3">File Description :</h5>

                            <div class="table-responsive">
                                <table class="table table-borderless table-nowrap table-sm">
                                    <tbody>
                                    <tr>
                                        <th scope="row" style="width: 35%;">File Name :</th>
                                        <td class="file-name">html.docx</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">File Type :</th>
                                        <td class="file-type">Documents</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Size :</th>
                                        <td class="file-size">0.3 KB</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Created :</th>
                                        <td class="create-date">19 Apr, 2022</td>
                                    </tr>
                                    <tr>
                                        <th scope="row">Path :</th>
                                        <td class="file-path">
                                            <div class="user-select-all text-truncate">*:\projects\src\assets\images
                                            </div>
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>

                            <div>
                                <h5 class="fs-12 text-uppercase text-muted mb-3">Share Information:</h5>
                                <div class="table-responsive">
                                    <table class="table table-borderless table-nowrap table-sm">
                                        <tbody>
                                        <tr>
                                            <th scope="row" style="width: 35%;">Share Name :</th>
                                            <td class="share-name">\\*\Projects</td>
                                        </tr>
                                        <tr>
                                            <th scope="row">Share Path :</th>
                                            <td class="share-path">velzon:\Documents\</td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>

                        <div class="mt-auto border-top border-top-dashed py-3">
                            <div class="hstack gap-2">
                                <button type="button" class="btn btn-soft-primary w-100">
                                    <i class="ri-download-2-line align-bottom me-1"></i>
                                    @lang('translation.download')
                                </button>
                                <button type="button" class="btn btn-soft-danger w-100 remove-file-overview"
                                        data-remove-id="" data-bs-toggle="modal" data-bs-target="#removeFileItemModal">
                                    <i class="ri-close-fill align-bottom me-1"></i>
                                    @lang('translation.delete')
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- START CREATE FILE MODAL -->
    <div class="modal fade zoomIn" id="createFileModal" tabindex="-1" aria-labelledby="createFileModalLabel"
         aria-hidden="true" validate>
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0">
                <div class="modal-header p-3 bg-soft-success">
                    <h5 class="modal-title" id="createFileModalLabel">
                        @lang('translation.create-file')
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" id="addFileBtn-close"
                            aria-label="Close">
                    </button>
                </div>
                <div class="modal-body">
                    <form class="dropzone needs-validation" id="create-file-modal-form"
                          action="{{ route('attach.files') }}" validate>
                    @csrf
                    <!-- end dropzon-preview -->
                        <input type="hidden" name="order_id" value="{{ $order->id }}">
                        <input type="hidden" name="order_type" value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}">
                        <div class="mb-4">
                            <label for="">
                                <p class="text-muted">
                                    Eslatma ! Faylni yuklashdan oldin iltimos fayl turini tanlang
                                </p>
                            </label>
                            <select class="form-control" name="file_type" id="file_type" required>
                                @foreach (App\Enums\MediaTypeEnum::cases() as $case)
                                    <option value="{{ $case->name }}" {{ $case->name === $type ? 'selected' : '' }}>
                                        @lang($case->value)
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-4">
                            <div class="card-body">
                                <div class="dropzone">
                                    <div class="fallback">
                                        <input name="file" type="file" multiple="multiple">
                                    </div>
                                    <div class="dz-message needsclick">
                                        <div class="mb-3">
                                            <i class="display-4 text-muted ri-upload-cloud-2-fill"></i>
                                        </div>
                                        <h4>Faylni bu yerga qo'ying yoki tanlang</h4>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- END CREATE FILE MODAL -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/show-documents.js') }}"></script>
@endsection
