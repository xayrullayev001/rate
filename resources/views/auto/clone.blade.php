@extends('layouts.master')
@section('title')
    @lang('translation.create-auto-order')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.auto')
        @endslot
        @slot('title')
            @lang('translation.create-auto-order')

        @endslot
    @endcomponent
    <form action="{{ route('auto.store') }}" method="post" autocomplete="off" class="needs-validation" validate>
        @csrf
        <input type="hidden" name="status" value="{{ App\Enums\OrderStatusEnum::DRAFT->name }}">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-information')
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-number-input">
                                    @lang('translation.number')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('number') is-invalid @enderror"
                                       name="number" placeholder="@lang('translation.write-number')"
                                       id="order-number-input" value="{{ $order->number }}"
                                       tabindex="1">
                                @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.number')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label"
                                       for="auto-created_date-input">@lang('translation.created-date')</label>
                                <input type="text"
                                       class="form-control text-uppercase flatpickr-input  @error('created_at') is-invalid @enderror"
                                       id="auto-created_date-input"
                                       name="created_at" value="{{ $order->created_at }}"
                                       data-provider="flatpickr" data-date-format="d.m.Y"
                                       tabindex="2">
                                @error('created_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.created-date')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label for="choices-purpose-input" class="form-label">
                                    @lang('translation.select-purpose')
                                </label>
                                <select data-choices data-choices-search-false data-choices-multiple-groups="false"
                                        name="purpose_id" class="form-select @error('purpose_id') is-invalid @enderror"
                                        id="choices-purpose-input" tabindex="3">
                                    @foreach($purposeCases as $purposeItem)
                                        <option value="{{ $purposeItem->id }}"
                                                @if($order->purpose_id == $purposeItem->id)selected @endif>
                                            {{ $purposeItem->{str_replace('_', '-', app()->getLocale())} }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('purpose_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.purpose')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <!-- order customer card -->
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-customer')
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <!-- Inline Checkbox -->
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input " type="radio"
                                       @if($order->customer_type=='physical') checked @endif
                                       name="customer_type"
                                       onclick="handleCustomerClick(this.value);" value="physical"
                                       id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    @lang('translation.physical')
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input is " type="radio"
                                       @if($order->customer_type=='juridical') checked
                                       @endif  name="customer_type"
                                       onclick="handleCustomerClick(this.value);" value="juridical"
                                       id="flexRadioDefault1">
                                <label class="form-check-label" for="flexRadioDefault1">
                                    @lang('translation.juridical')
                                </label>
                            </div>
                        </div>
                        <div class="row gy-4 @if($order->customer_type=='juridical') d-none @endif" id="customer-block">

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_last_name-input">
                                    @lang('translation.customer-last-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_last_name') is-invalid @enderror"
                                       name="customer_last_name" id="order-customer_last_name-input"
                                       value="{{ $order->customer_last_name }}"
                                       placeholder="@lang('translation.write-customer-last-name')" tabindex="4">
                                @error('customer_last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-last-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_first_name-input">
                                    @lang('translation.customer-first-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_first_name') is-invalid @enderror"
                                       name="customer_first_name" id="order-customer_first_name-input"
                                       value="{{ $order->customer_first_name }}"
                                       placeholder="@lang('translation.write-customer-first-name')" tabindex="5">
                                @error('customer_first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-first-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_patronymic-input">
                                    @lang('translation.customer-patronymic')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_patronymic') is-invalid @enderror"
                                       name="customer_patronymic" id="order-customer_patronymic-input"
                                       value="{{ $order->customer_patronymic }}"
                                       placeholder="@lang('translation.write-customer-patronymic')" tabindex="6">
                                @error('customer_patronymic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-patronymic')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4 @if($order->customer_type=='physical') d-none @endif"
                             id="customer-company-block">
                            <div class="mb-3">
                                <label class="form-label" for="order-customer_company-input">
                                    @lang('translation.customer-juridical-name')
                                </label>
                                <input type="text" class="form-control text-uppercase" name="customer_company"
                                       id="order-customer_company-input" value="{{ $order->customer_company }}"
                                       placeholder="@lang('translation.write-customer-juridical-name')" tabindex="3">
                                @error('customer_company')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-company')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->

                <!-- order owner card -->
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-owner')
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <!-- Inline Checkbox -->
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input" type="radio" @if($order->owner_type=='physical') checked
                                       @endif  name="owner_type"
                                       onclick="handleOwnerClick(this.value);" value="physical"
                                       id="flexRadioDefault3">
                                <label class="form-check-label" for="flexRadioDefault3">
                                    @lang('translation.physical')
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input is " type="radio"
                                       @if($order->owner_type=='juridical') checked
                                       @endif  name="owner_type"
                                       onclick="handleOwnerClick(this.value);" value="juridical"
                                       id="flexRadioDefault4">
                                <label class="form-check-label" for="flexRadioDefault4">
                                    @lang('translation.juridical')
                                </label>
                            </div>
                        </div>
                        <div class="row gy-4  @if($order->owner_type=='juridical')d-none @endif" id="owner-block">

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-owner_last_name-input">
                                    @lang('translation.owner-last-name')
                                </label>
                                <input type="text" value="{{ $order->owner_last_name }}"
                                       class="form-control text-uppercase @error('owner_last_name') is-invalid @enderror"
                                       name="owner_last_name" id="order-owner_last_name-input"
                                       placeholder="@lang('translation.write-auto-owner-last-name')" tabindex="7">
                                @error('owner_last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-last-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6" id="owner-block">
                                <label class="form-label" for="order-owner_first_name-input">
                                    @lang('translation.owner-first-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('owner_first_name') is-invalid @enderror"
                                       name="owner_first_name" value="{{ $order->owner_first_name }}"
                                       id="order-owner_first_name-input"
                                       placeholder="@lang('translation.write-auto-owner-first-name')" tabindex="8">
                                @error('owner_first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-first-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-owner_patronymic-input">
                                    @lang('translation.owner-patronymic')
                                </label>
                                <input type="text" value="{{ $order->owner_patronymic }}"
                                       class="form-control text-uppercase @error('owner_patronymic') is-invalid @enderror"
                                       name="owner_patronymic" id="order-owner_patronymic-input"
                                       placeholder="@lang('translation.write-auto-owner-patronymic')" tabindex="9">
                                @error('owner_patronymic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-patronymic')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4 @if($order->owner_type=='physical')d-none @endif"
                             id="owner-company-block">
                            <div class="mb-3">
                                <label class="form-label" for="order-owner_company-input">
                                    @lang('translation.owner-company')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('owner_company') is-invalid @enderror"
                                       name="owner_company" value="{{ $order->owner_company }}"
                                       id="order-owner_company-input"
                                       placeholder="@lang('translation.write-auto-owner-juridical-name')" tabindex="">
                                @error('owner_company')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-company')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        @lang('translation.auto-information')
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="choose-order-car-category">
                                    @lang('translation.select-car-category')
                                </label>
                                <select data-choices data-choices-multiple-groups="false"
                                        class="form-control @error('car_category') is-invalid @enderror"
                                        id="choose-order-car-category" name="car_category" tabindex="10">
                                    @foreach ($concerns as $concern)
                                        <option value="{{ $concern->id }}"
                                            {{ $order->car_category == $concern->id ? 'selected' : '' }}>
                                            @lang($concern->{str_replace('_', '-', app()->getLocale())})
                                        </option>
                                    @endforeach
                                </select>
                                @error('car_category')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.car_category')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-car_mark-input">
                                    @lang('translation.car-mark')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('car_mark') is-invalid @enderror"
                                       id="auto-car_mark-input" name="car_mark" value="{{ $order->car_mark }}"
                                       placeholder="@lang('translation.write-car-mark')" tabindex="12">
                                @error('car_mark')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.car-mark')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class=" col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-color-input">
                                    @lang('translation.color')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('color') is-invalid @enderror"
                                       id="auto-color-input" name="color" value="{{ $order->color }}"
                                       placeholder="@lang('translation.write-car-color')" tabindex="22">
                                @error('color')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.color')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-made_date-input">
                                    @lang('translation.made-date')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('made_date') is-invalid @enderror"
                                       id="auto-made_date-input" name="made_date"
                                       data-provider="flatpickr" data-date-format="d.m.Y"
                                       value="{{ $order->made_date }}"
                                       placeholder="@lang('translation.write-car-made-date')"
                                       tabindex="13">
                                @error('made_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.made-date')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-body-input">
                                    @lang('translation.body')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('body') is-invalid @enderror"
                                       id="auto-body-input" name="body" value="{{ $order->body }}"
                                       placeholder="@lang('translation.write-car-body')" tabindex="14">
                                @error('body')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.body')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-engine-input">
                                    @lang('translation.engine')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('engine') is-invalid @enderror"
                                       id="auto-engine-input" name="engine" value="{{ $order->engine }}"
                                       placeholder="@lang('translation.write-car-engine')" tabindex="15">
                                @error('engine')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.engine')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-car_number-input">
                                    @lang('translation.car-number')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('car_number') is-invalid @enderror"
                                       id="auto-car_number-input" name="car_number" value="{{ $order->car_number }}"
                                       placeholder="@lang('translation.write-car-number')" tabindex="16">
                                @error('car_number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.car-number')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-type-input">
                                    @lang('translation.type')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('type') is-invalid @enderror"
                                       id="auto-type-input" name="type" value="{{ $order->type }}"
                                       placeholder="@lang('translation.write-car-type')" tabindex="17">
                                @error('type')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.type')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-shassi-input">
                                    @lang('translation.shassi')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('shassi') is-invalid @enderror"
                                       id="auto-shassi-input" name="shassi" value="{{ $order->shassi }}"
                                       placeholder="@lang('translation.write-car-shassi')" tabindex="18">
                                @error('shassi')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.shassi')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-tech_passport-input">
                                    @lang('translation.technical-passport')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('tech_passport') is-invalid @enderror"
                                       id="auto-tech_passport-input"
                                       name="tech_passport" value="{{ $order->tech_passport }}"
                                       placeholder="@lang('translation.write-car-technical-passport')" tabindex="19">
                                @error('tech_passport')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.technical-passport')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label"
                                       for="auto-tech_given_date-input">
                                    @lang('translation.technical-passport-given-date')
                                </label>
                                <input type="text"
                                       class="form-control flatpickr-input text-uppercase @error('tech_given_date') is-invalid @enderror"
                                       id="auto-tech_given_date-input"
                                       name="tech_given_date"
                                       value="{{ $order->tech_given_date }}"
                                       data-provider="flatpickr" data-date-format="d.m.Y"
                                       placeholder="@lang('translation.write-car-technical-passport-given-date')"
                                       tabindex="20">
                                @error('tech_given_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.technical-passport-given-date')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label"
                                       for="auto-tech_given_whom-input">
                                    @lang('translation.technical-passport-given-by-whom')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('tech_given_whom') is-invalid @enderror"
                                       id="auto-tech_given_whom-input"
                                       name="tech_given_whom" value="{{ $order->tech_given_whom }}"
                                       placeholder="@lang('translation.write-technical-passport-given-by-whom')"
                                       tabindex="21">
                                @error('tech_given_whom')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.technical-passport-given-by-whom')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                    </div>
                </div>
                <!-- end card -->
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="text-start mb-3 btn-block">
                        <button type="submit" class="btn btn-success w-sm" name="save"
                                tabindex="">@lang('translation.save')</button>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <!-- end card -->
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">@lang('translation.diller')</h5>
                    </div>
                    <div class="card-body">
                        <label for="choices-diller-input" class="form-label">
                            @lang('translation.select-diller')
                        </label>
                        <select data-choices data-choices-search-false data-choices-multiple-groups="false"
                                name="diller_id" class="form-select @error('diller_id') is-invalid @enderror"
                                id="choices-diller-input"
                                tabindex="23">
                            <option value="" selected> @lang('translation.select-diller')</option>
                            @foreach($dillers as $diller)
                                <option value="{{ $diller->id}}" @if($order->diller_id== $diller->id) selected @endif>
                                    {{ $diller->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('diller_id')
                        <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.diller')])</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.ordered-customer')
                        </h5>
                    </div>
                    <div class="card-body">
                        <label for="auto-ordered_customer-input" class="form-label">
                            @lang('translation.ordered-customer-name')
                        </label>
                        <input type="text"
                               class="form-control text-uppercase @error('ordered_customer') is-invalid @enderror"
                               id="auto-ordered_customer-input"
                               name="ordered_customer" value="{{ $order->ordered_customer }}"
                               placeholder="@lang('translation.write-car-ordered-customer')" tabindex="24">
                        @error('ordered_customer')
                        <span class="invalid-feedback" role="alert">
                            <strong>@lang($message,['attribute'=>trans('translation.ordered-customer')])</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <label for="cleave-phone" class="form-label">
                            @lang('translation.ordered-customer-phone')
                        </label>
                        <input type="text"
                               class="form-control text-uppercase @error('ordered_customer_phone') is-invalid @enderror"
                               name="ordered_customer_phone" id="cleave-phone"
                               value="{{ $order->ordered_customer_phone }}"
                               placeholder="@lang('translation.write-car-ordered-customer-phone')" tabindex="25">
                        @error('ordered_customer_phone')
                        <span class="invalid-feedback" role="alert">
                                <strong>@lang($message,['attribute'=>trans('translation.ordered-customer-phone')])</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.cost')
                        </h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <label class="form-label" for="product-cost-addon">
                                @lang('translation.order-cost-note')
                            </label>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="product-cost-addon">uzs</span>
                                <input type="text" class="form-control @error('cost') is-invalid @enderror" name="cost"
                                       id="cleave-numeral" value="{{ $order->cost }}"
                                       placeholder="123,450" aria-label="Price"
                                       aria-describedby="product-price-addon"
                                       tabindex="26">
                                @error('cost')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.cost')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">@lang('translation.note')</h5>
                    </div>
                    <div class="card-body">
                        <label for="auto-note-input" class="text-muted mb-2">
                            @lang('translation.note-value')
                        </label>
                        <textarea class="form-control text-uppercase @error('note') is-invalid @enderror" name="note"
                                  id="auto-note-input" value="{{ $order->note }}"
                                  placeholder="@lang('translation.write-cost-note')"
                                  rows="3" tabindex="27"></textarea>
                        @error('note')
                        <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.note')])</strong>
                                </span>
                        @enderror
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
        </div>
        <!-- end row -->
    </form>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    {{--    <script src="{{ URL::asset('/assets/js/pages/orders/show.js') }}"></script>--}}
    <script src="{{ URL::asset('/assets/js/pages/orders/create.js') }}"></script>
@endsection
