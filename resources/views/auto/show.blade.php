@extends('layouts.master')
@section('title')
    @lang('translation.overview')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.order-breadcrumbs', ['order' => $order, 'active' => 'index'])
    @endcomponent

    <!-- Page content row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content text-muted ">
                <div class="tab-pane fade show active" id="project-overview" role="tabpanel">
                    <div class="row">
                        <div class="col-xl-9 col-lg-8">
                            <div class="card">
                                <div class="card-body">
                                    <div class="text-muted ">
                                        <h6 class="mb-3 fw-semibold text-uppercase">@lang('translation.summary')</h6>
                                        <p>{{ $order->note }}</p>
                                        <div class="col-lg-12">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <ul class="ps-4 vstack gap-2">
                                                        <li>
                                                            @lang('translation.car_category') :
                                                            {{ $order->concernOne->{str_replace('_', '-', app()->getLocale())} }}
                                                        </li>
                                                        <li>@lang('translation.car-mark') :
                                                            {{ $order->car_mark }}
                                                        </li>
                                                        <li>@lang('translation.car-number') :
                                                            {{ $order->car_number }}
                                                        </li>
                                                        <li>@lang('translation.made-date') :
                                                            {{ $order->made_date }}
                                                        </li>
                                                        <li>@lang('translation.color') :
                                                            {{ $order->color }}
                                                        </li>
                                                        <li>@lang('translation.body') :
                                                            {{ $order->body }}
                                                        </li>
                                                        <li>@lang('translation.engine') :
                                                            {{ $order->engine }}
                                                        </li>
                                                        <li>@lang('translation.technical-passport') :
                                                            {{ $order->tech_passport }}
                                                        </li>
                                                        <li>@lang('translation.technical-passport-given-date') :
                                                            {{ $order->tech_given_date }}
                                                        </li>
                                                        <li>@lang('translation.technical-passport-given-by-whom') :
                                                            {{ $order->tech_given_whom }}
                                                        </li>
                                                        <li>@lang('translation.type') :
                                                            {{ $order->type }}
                                                        </li>
                                                        <li>@lang('translation.shassi') :
                                                            {{ $order->shassi }}
                                                        </li>

                                                    </ul>
                                                </div>
                                                <div class="col-lg-6">
                                                    <ul class="ps-4 vstack gap-2">
                                                        <li>@lang('translation.customer')
                                                            : {{ $order->customer }}</li>
                                                        <li>@lang('translation.owner') : {{ $order->ownerName }}</li>
                                                        <li>@lang('translation.ordered-customer-name')
                                                            : {{ $order->ordered_customer }}</li>
                                                        <li>@lang('translation.ordered-customer-phone')
                                                            : {{ $order->ordered_customer_phone }}</li>
                                                        <li>@lang('translation.purpose') : <span
                                                                class="text-uppercase">{{  $order->purposeOne->{str_replace('_', '-', app()->getLocale())} }} </span>
                                                        </li>
                                                        <li>@lang('translation.diller') : {{ $order->diller }}</li>
                                                        <li>@lang('translation.cost')
                                                            : @component('components.price',['balance'=>$order->cost]) @endcomponent</li>
                                                        <li>@lang('translation.price')
                                                            : @component('components.price',['balance'=>$order->price]) @endcomponent</li>

                                                    </ul>
                                                </div>

                                            </div>
                                        </div>
                                        <div class="pt-3 border-top border-top-dashed mt-4">
                                            <div class="row">
                                                <div class="col-lg-3 col-sm-6">
                                                    <div>
                                                        <p class="mb-2 text-uppercase fw-medium">@lang('translation.created-date') :</p>
                                                        <h5 class="fs-15 mb-0">{{ $order->created_at }}</h5>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-sm-6">
                                                    <div>
                                                        <p class="mb-2 text-uppercase fw-medium">@lang('translation.updated-date') :</p>
                                                        <h5 class="fs-15 mb-0">{{ $order->created_at }}</h5>
                                                    </div>
                                                </div>
                                                <div class="col-lg-3 col-sm-6">
                                                    <div>
                                                        <p class="mb-2 text-uppercase fw-medium">
                                                            @lang('translation.status')
                                                        </p>
                                                        <div class="badge bg-warning fs-12">
                                                            @lang(\App\Enums\OrderStatusEnum::getLabel($order->status))
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="pt-3 border-top border-top-dashed mt-4">
                                            <div class="card-header align-items-center d-flex">
                                                <h4 class="card-title mb-0 flex-grow-1">
                                                    {{ __('translation.conclusions') }}
                                                </h4>
                                                <div class="flex-shrink-0">
                                                    @if(in_array($order->status,[\App\Enums\OrderStatusEnum::STARTED->name,\App\Enums\OrderStatusEnum::REJECTED->name])&& $isAppraisers)
                                                        <a href="{{ route('conclusion.add', ['id' => $order->id,'type'=>\App\Enums\OrderTypeEnum::AUTO->name]) }}"
                                                           type="button" class="btn btn-soft-info btn-sm">
                                                            <i class="ri-upload-2-fill me-1 align-bottom"></i>
                                                            {{ __('translation.upload') }}
                                                        </a>
                                                    @endif
                                                    @if($order->status == \App\Enums\OrderStatusEnum::FINISHED->name &&  in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                                        <button type="button" class="btn btn-soft-info btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#givingBonusModal">
                                                            <i class="ri-upload-2-fill me-1 align-bottom"></i>
                                                            {{ __('translation.give-bonus') }}
                                                        </button>
                                                        <button type="button" class="btn btn-danger btn-sm"
                                                                data-bs-toggle="modal"
                                                                data-bs-target="#rejectConclusionModal">
                                                            <i class="ri-upload-2-fill me-1 align-bottom"></i>
                                                            {{ __('translation.conclusion-reject') }}
                                                        </button>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="row pt-3 g-3">
                                                @foreach ($conclusions as $conclusion)
                                                    <div class="col-xxl-4 col-lg-6">
                                                        <div class="border rounded border-dashed p-2">
                                                            <div class="d-flex align-items-center">
                                                                <div class="flex-shrink-0 me-3">
                                                                    <div class="avatar-sm">
                                                                        <div
                                                                            class="avatar-title bg-light text-secondary rounded fs-24">
                                                                            <i class="ri-file-{{ $conclusion->extension }}-line"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-grow-1 overflow-hidden">
                                                                    <h5 class="fs-13 mb-1">
                                                                        <a data-bs-file="{{ Storage::url($conclusion->path) }}"
                                                                           data-bs-file-name="{{ $conclusion->name }}"
                                                                           data-bs-toggle="modal"
                                                                           data-bs-target="#conclusionViewModal"
                                                                           href="#"
                                                                           class="text-body text-truncate d-block">
                                                                            {{ $conclusion->name }}
                                                                        </a>
                                                                    </h5>
                                                                    <div>{{ $conclusion->size }}MB</div>
                                                                </div>
                                                                <div class="flex-shrink-0 ms-2">
                                                                    <div class="d-flex gap-1">
                                                                        <a href="{{ route('download', ['file' => $conclusion->id]) }}"
                                                                           class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                                                            <i class="ri-download-2-line"></i>
                                                                        </a>
                                                                        <div class="dropdown">
                                                                            <button
                                                                                class="btn btn-icon text-muted btn-sm fs-18 dropdown"
                                                                                type="button" data-bs-toggle="dropdown"
                                                                                aria-expanded="false">
                                                                                <i class="ri-more-fill"></i>
                                                                            </button>
                                                                            <ul class="dropdown-menu">
                                                                                <li>
                                                                                    <a data-bs-file="{{ Storage::url($conclusion->path) }}"
                                                                                       data-bs-file-name="{{ $conclusion->name }}"
                                                                                       data-bs-toggle="modal"
                                                                                       data-bs-target="#conclusionViewModal"
                                                                                       class="dropdown-item">
                                                                                        <i class="ri-fullscreen-line align-bottom me-2 text-muted"></i>
                                                                                        @lang('translation.view')
                                                                                    </a>
                                                                                </li>
                                                                                <li>
                                                                                    @component('components.delete-btn')
                                                                                        @slot('url')
                                                                                            {{ route('file.delete', [$conclusion->id]) }}
                                                                                        @endslot
                                                                                    @endcomponent
                                                                                </li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                            @endforeach
                                            <!-- end col -->
                                            </div>
                                            <!-- end row -->
                                        </div>
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- ene col -->
                        <div class="col-xl-3 col-lg-4">
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">
                                        @lang('translation.ordered-customer')
                                    </h5>
                                </div>
                                <div class="card-body">
                                    <label for="auto-ordered_customer-input" class="form-label">
                                        @lang('translation.ordered-customer-name')
                                    </label>
                                    <div class="badge fw-medium badge-soft-secondary">
                                        {{ $order->ordered_customer }}
                                    </div>
                                    <br />
                                    <label for="cleave-phone" class="form-label">
                                        @lang('translation.ordered-customer-phone')
                                    </label>
                                    <div class="badge fw-medium badge-soft-secondary">
                                        {{ $order->ordered_customer_phone }}
                                    </div>
                                    <br />
                                    @component('components.tg-link',['phone'=>$order->ordered_customer_phone])@endcomponent
                                </div>
                            </div>
                            <!-- end card -->
                            <div class="card">
                                <div class="card-header">
                                    <h5 class="card-title mb-0">
                                        @lang('translation.diller')
                                    </h5>
                                </div>
                                <div class="card-body">
                                    <label for="auto-ordered_customer-input" class="form-label">
                                        @lang('translation.diller-name')
                                    </label>
                                    <div class="badge fw-medium badge-soft-secondary">
                                        {{ $order->diller }}
                                    </div>
                                    @if(isset($order->dillerUser))<br/>
                                    <label for="cleave-phone" class="form-label">
                                        @lang('translation.phone')
                                    </label>
                                    <div class="badge fw-medium badge-soft-secondary">
                                        {{ $order->dillerUser->phone }}
                                    </div>
                                    <br/>
                                    @component('components.tg-link',['phone'=>$order->dillerUser->phone])@endcomponent
                                    @endif
                                </div>
                            </div>
                            <!-- end card -->
                            <div class="card">
                                <div class="card-header align-items-center d-flex border-bottom-dashed">
                                    <h4 class="card-title mb-0 flex-grow-1">@lang('translation.cost')</h4>
                                </div>
                                <div class="card-body">
                                    <label for="auto-ordered_customer-input" class="form-label">
                                        @lang('translation.cost')
                                    </label>
                                    <div class="badge fw-medium badge-soft-secondary">
                                        @component('components.price',['balance'=>$order->cost]) @endcomponent
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                            @if ($order->status === \App\Enums\OrderStatusEnum::FINISHED->name)
                                <div class="card">
                                    <div class="card-header align-items-center d-flex border-bottom-dashed">
                                        <h4 class="card-title mb-0 flex-grow-1">@lang('translation.object-price') </h4>
                                    </div>
                                    <div class="card-body">

                                        <div class="d-flex flex-wrap gap-2 fs-16">
                                            <div class="badge fw-medium badge-soft-secondary">
                                                @lang('translation.object-price')
                                            </div>
                                            <div class="badge fw-medium badge-soft-secondary">
                                                @component('components.price',['balance'=>$order->object_price]) @endcomponent
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end card body -->
                                </div>
                                <!-- end card -->
                            @endif
                            <div class="card">
                                <div class="card-header align-items-center d-flex border-bottom-dashed">
                                    <h4 class="card-title mb-0 flex-grow-1"> @lang('translation.member-appraisers')</h4>
                                    @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                        <div class="flex-shrink-0">
                                            <button type="button" class="btn btn-soft-danger btn-sm"
                                                    data-bs-toggle="modal"
                                                    data-bs-target="#inviteMembersModal"><i
                                                    class="ri-share-line me-1 align-bottom"></i>
                                                @lang('translation.invite-member')
                                            </button>
                                        </div>
                                    @endif
                                </div>

                                <div class="card-body">
                                    <div class="mx-n3 px-3">
                                        <div class="vstacks gap-3">
                                            @foreach ($members as $member)
                                                <div class="d-flex align-items-center">
                                                    <div class="avatar-xs flex-shrink-0 me-3">
                                                        <!-- member avatar assets/images/users/avatar-2.jpg -->
                                                        <img src="{{ URL::asset("{$member->avatar}") }}" alt=""
                                                             class="img-fluid rounded-circle">
                                                    </div>
                                                    <div class="flex-grow-1">
                                                        <h5 class="fs-13 mb-0">
                                                            <a href="{{ route('user.show', [$member->id]) }}"
                                                               class="text-body d-block">
                                                                {{ $member->name }}
                                                            </a>
                                                        </h5>
                                                    </div>
                                                    <div class="flex-shrink-0">
                                                        <div class="d-flex align-items-center gap-1">
                                                            @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                                                <form name="order-member{{ $member->id }}"
                                                                      action="{{ route('member.remove') }}"
                                                                      method="post"
                                                                      novalidate>
                                                                    @csrf
                                                                    @method('post')
                                                                    <input type="hidden" value="{{ $member->id }}"
                                                                           name="id">
                                                                    <input type="hidden" value="{{ $order->id }}"
                                                                           name="order_id">
                                                                    <input type="hidden"
                                                                           value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}"
                                                                           name="order_type">
                                                                    <button type="submit" class="btn btn-light btn-sm">
                                                                        <i
                                                                            class="ri-delete-bin-5-fill text-muted me-2 align-bottom"></i>
                                                                        @lang('translation.delete')
                                                                    </button>
                                                                </form>

                                                            @endif
                                                            <div class="dropdown">
                                                                <button
                                                                    class="btn btn-icon btn-sm fs-16 text-muted dropdown"
                                                                    type="button" data-bs-toggle="dropdown"
                                                                    aria-expanded="false">
                                                                    <i class="ri-more-fill"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item"
                                                                           href="{{ route('user.show', [$member->id]) }}">
                                                                            <i
                                                                                class="ri-eye-fill text-muted me-2 align-bottom"></i>
                                                                            @lang('translation.account')
                                                                        </a>
                                                                    </li>
                                                                    <li><a class="dropdown-item"
                                                                           href="{{ route('user.favourite', ['id' => $member->id]) }}">
                                                                            <i
                                                                                class="ri-star-fill text-muted me-2 align-bottom"></i>
                                                                            @lang('translation.favourite')
                                                                        </a>
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                        @endforeach
                                        <!-- end member item -->

                                        </div>
                                        <!-- end list -->
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->

                            <div class="card">

                                <div class="card-header align-items-center d-flex border-bottom-dashed">
                                    <h4 class="card-title mb-0 flex-grow-1">@lang('translation.attachments')</h4>
                                    <div class="flex-shrink-0">
{{--                                        @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))--}}
                                        <button type="button" class="btn btn-soft-info btn-sm" data-bs-toggle="modal"
                                                data-bs-target="#attachModal"><i
                                                class="ri-upload-2-fill me-1 align-bottom"></i>
                                            @lang('translation.upload')
                                        </button>
{{--                                        @endif--}}
                                    </div>
                                </div>

                                <div class="card-body">
                                    <div class="vstack gap-2">
                                        @foreach ($files as $attachment)
                                            <div class="border rounded border-dashed p-2">
                                                <div class="d-flex align-items-center">
                                                    <div class="flex-shrink-0 me-3">
                                                        <div class="avatar-sm">
                                                            <div
                                                                class="avatar-title bg-light text-secondary rounded fs-24">
                                                                @if (in_array($attachment->extension, ['jpg', 'jpg', 'png']))
                                                                    {{--                                                                    <img src="{{ URL::asset("{$attachment->path}") }}" alt="" class="rounded avatar-sm"> --}}
                                                                    <i class="ri-image-2-line"></i>
                                                                @else
                                                                    <i
                                                                        class="ri-file-{{ $attachment->extension }}-line"></i>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="flex-grow-1 overflow-hidden">
                                                        <h5 class="fs-13 mb-1">
                                                            <a href="{{ Storage::url($attachment->path) }}"
                                                               class="text-body text-truncate d-block">
                                                                {{ $attachment->name }}
                                                            </a>
                                                        </h5>
                                                        <div> {{ $attachment->size }} MB</div>
                                                    </div>
                                                    <div class="flex-shrink-0 ms-2">
                                                        <div class="d-flex gap-1">
                                                            <div class="dropdown">
                                                                <a href="{{ route('download', ['file' => $attachment->id]) }}"
                                                                   class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                                                    <i class="ri-download-2-line"></i>
                                                                </a>
                                                                <button
                                                                    class="btn btn-icon text-muted btn-sm fs-18 dropdown"
                                                                    type="button" data-bs-toggle="dropdown"
                                                                    aria-expanded="false">
                                                                    <i class="ri-more-fill"></i>
                                                                </button>
                                                                <ul class="dropdown-menu">
                                                                    <li>
                                                                        <a class="dropdown-item"
                                                                           href="{{ Storage::url($attachment->path) }}"><i
                                                                                class="ri-eye-line align-bottom me-2 text-muted"></i>
                                                                            @lang('translation.view')
                                                                        </a>
                                                                    </li>
                                                                    <li>
                                                                        @component('components.delete-btn')
                                                                            @slot('url')
                                                                                {{ route('file.delete', [$attachment->id]) }}
                                                                            @endslot
                                                                        @endcomponent
                                                                    </li>
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                        <div class="mt-2 text-center">
                                            <a href="{{ route('auto.show-documents',[$order->id]) }}" type="button"
                                               class="btn btn-success">
                                                @lang('translation.view-more')
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <!-- end card body -->
                            </div>
                            <!-- end card -->
                        </div>
                        <!-- end col -->
                    </div>
                    <!-- end row -->
                </div>
                <!-- end tab pane -->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- Page content end row -->

    @if(Auth::user()->role!=\App\Enums\RoleEnum::USER->name)
        <!-- Attachment adding Modal -->
        <div class="modal fade" id="attachModal" tabindex="-1" aria-labelledby="attachModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header p-3 ps-4 bg-soft-success">
                        <h5 class="modal-title" id="attachModalLabel">@lang('translation.attachments')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body p-4">
                        <div class="card">
                            <div class="card-body">
                                <form class="dropzone" id="createfile-form" action='{{ route('attach.files') }}'
                                      novalidate>
                                    @csrf
                                    <input type="hidden" name="order_id" value="{{ $order->id }}">
                                    <input type="hidden" name="order_type"
                                           value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}">
                                    <input type="hidden" name="file_type"
                                           value="{{ \App\Enums\MediaTypeEnum::ADDITIONAL->name }}">

                                    <p class="text-muted"></p>

                                    <div class="dropzone">
                                        <div class="fallback">
                                            <input name="file" type="file" multiple="multiple">
                                        </div>
                                        <div class="dz-message needsclick">
                                            <div class="mb-3">
                                                <i class="display-4 text-muted ri-upload-cloud-2-fill"></i>
                                            </div>

                                            <h4>Faylni bu yerga qo'ying yoki tanlang.</h4>
                                        </div>
                                    </div>

                                    <ul class="list-unstyled mb-0" id="dropzone-preview">
                                        <li class="mt-2" id="dropzone-preview-list">
                                            <!-- This is used as the file preview template -->
                                            <div class="border rounded">
                                                <div class="d-flex p-2">
                                                <div class="flex-shrink-0 me-3">
                                                    <div class="avatar-sm bg-light rounded">
                                                        <img data-dz-thumbnail class="img-fluid rounded d-block"
                                                             src="#" alt="Dropzone-Image"/>
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1">
                                                    <div class="pt-1">
                                                        <h5 class="fs-14 mb-1" data-dz-name>&nbsp;</h5>
                                                        <p class="fs-13 text-muted mb-0" data-dz-size></p>
                                                        <strong class="error text-danger" data-dz-errormessage></strong>
                                                    </div>
                                                </div>
                                                <div class="flex-shrink-0 ms-3">
                                                    <button data-dz-remove
                                                            class="btn btn-sm btn-danger">@lang('translation.delete')</button>
                                                </div>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                    <!-- end dropzon-preview -->
                                </form>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light w-xs" data-bs-dismiss="modal">
                            @lang('translation.cancel')
                        </button>
                        <button type="button" class="btn btn-success w-xs" data-bs-dismiss="modal">
                            @lang('translation.save')
                        </button>
                    </div>

                </div>
                <!-- end modal-content -->
            </div>
            <!-- modal-dialog -->
        </div>
        <!-- Attachment adding end modal -->
    @endif
    @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
        <!-- Invite members Modal -->
        <div class="modal fade" id="inviteMembersModal" tabindex="-1" aria-labelledby="inviteMembersModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header p-3 ps-4 bg-soft-success">
                        <h5 class="modal-title" id="inviteMembersModalLabel">@lang('translation.member-appraisers')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form name="members-invite" action="{{ route('members.invite') }}" method="post">
                        @csrf
                        <div class="modal-body p-4">
                            <input type="hidden" value="{{ $order->id }}" name="order_id">
                            <input type="hidden" value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}" name="order_type">
                            <div class="search-box mb-3">
                                <input type="text" class="form-control bg-light border-light"
                                       placeholder="@lang('translation.search-here')">
                                <i class="ri-search-line search-icon"></i>
                            </div>

                            <div class="mb-4 d-flex align-items-center">
                                <div class="me-2">
                                    <h5 class="mb-0 fs-13">@lang('translation.member-appraisers')</h5>
                                </div>
                                <div class="avatar-group justify-content-center">
                                    @foreach ($members as $member)
                                        <a href="{{ route('user.show', [$member->id]) }}" class="avatar-group-item"
                                           data-bs-toggle="tooltip"
                                           data-bs-trigger="hover" data-bs-placement="top" title="{{ $member->name }}">
                                            <div class="avatar-xs">
                                                <img src="{{ URL::asset($member->avatar) }}" alt=""
                                                     class="rounded-circle img-fluid">
                                            </div>
                                        </a>
                                    @endforeach

                                </div>
                            </div>
                            <div class="mx-n4 px-4" data-simplebar style="max-height: 225px;">
                                <div class="vstack gap-3">
                                    @foreach ($appraisers as $appraiser)
                                        <div class="d-flex align-items-center">
                                            <div class="avatar-xs flex-shrink-0 me-3">
                                                <img src="{{ URL::asset("{$appraiser->avatar}") }}"
                                                     alt="{{ $appraiser->name }}" class="img-fluid rounded-circle">
                                            </div>
                                            <div class="flex-grow-1">
                                                <h5 class="fs-13 mb-0">
                                                    <a href="#" class="text-body d-block">
                                                        {{ $appraiser->name }}
                                                    </a>
                                                </h5>
                                            </div>
                                            <!-- Outlined Styles -->
                                            <div class="flex-shrink-0">
                                                <div class="hstack gap-2 flex-wrap">
                                                    <input type="checkbox" name="{{ $appraiser->id }}" class="btn-check"
                                                           id="btn-check-outlined-{{ $appraiser->id }}">
                                                    <label class="btn btn-sm btn-outline-primary"
                                                           for="btn-check-outlined-{{ $appraiser->id }}">@lang('translation.add')</label>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end member item -->
                                    @endforeach
                                </div>
                                <!-- end list -->
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-light w-xs"
                                    data-bs-dismiss="modal">@lang('translation.cancel')</button>
                            <button type="submit" class="btn btn-success w-xs">@lang('translation.invite')</button>
                        </div>
                    </form>
                </div>
                <!-- end modal-content -->
            </div>
            <!-- modal-dialog -->
        </div>
        <!-- Invite members end modal -->

        <!-- Give bonus to Appraiser Modal -->
        <div class="modal fade" id="givingBonusModal" tabindex="-1" aria-labelledby="givingBonusModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header p-3 ps-4 bg-soft-success">
                        <h5 class="modal-title" id="givingBonusModalLabel">@lang('translation.set-price')</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="{{ route('bonus.store') }}" method="POST">
                        <div class="modal-body p-4">
                            <div class="card-body">
                                @csrf
                                <input type="hidden" value="{{ $order->id }}" name="order_id" id="order_id">
                                <!-- Input with Label -->
                                <div class="m-3">
                                    <select class="form-select mb-3" name="appraiser_id"
                                            aria-label="Select appraiser or many appraisers" data-choices
                                            data-choices-search-false
                                            data-choices-removeItem required
                                            @error('appraiser_id') is-invalid @enderror>
                                        @foreach ($members as $appraiser)
                                            <option class="text-uppercase" value="{{ $appraiser->id }}">
                                                {{ $appraiser->name }}
                                            </option>
                                        @endforeach
                                    </select>
                                    @error('appraiser_id')
                                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <!-- Input with Label -->
                            <div class="m-3">
                                <label for="cleave-numeral"
                                       class="form-label">@lang('translation.setting-bonus')</label>
                                <input type="text" name="amount" class="form-control " id="cleave-numeral" required
                                       @error('amount') is-invalid @enderror>
                                @error('amount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light w-xs" data-bs-dismiss="modal">
                            @lang('translation.cancel')
                        </button>
                        <button type="submit" class="btn btn-success w-xs">
                            @lang('translation.save')
                        </button>
                    </div>
                    </form>
                </div>
                <!-- end modal-content -->
            </div>
            <!-- modal-dialog -->
        </div>
        <!-- Give bonus to Appraiser end modal -->

        <!-- Reject conclusion Modal -->
        <div class="modal fade" id="rejectConclusionModal" tabindex="-1" aria-labelledby="rejectConclusionModalLabel"
             aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content border-0">
                    <div class="modal-header p-3 ps-4 bg-soft-success">
                        <h5 class="modal-title" id="rejectConclusionModalLabel">
                            @lang('translation.conclusion-reject')
                        </h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form action="{{ route('conclusion.reject') }}" method="POST">
                        @csrf
                        @method('put')
                        <div class="modal-body p-4">
                            <div class="card-body">
                                <input type="hidden" value="{{ $order->id }}" name="order_id" id="order_id">
                                <input type="hidden" value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}"
                                       name="order_type" id="order_type">
                                <!-- Input with Label -->
                                <div class="m-3">
                                    <div class="form-group">
                                        <label for="reject-node">@lang('translation.reject-note-comment')</label>
                                        <textarea class="form-control form-control-lg" name="note"
                                                  id="reject-node"></textarea>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light w-xs"
                                data-bs-dismiss="modal">@lang('translation.cancel')</button>
                        <button type="submit" class="btn btn-success w-xs">
                            @lang('translation.save')
                        </button>
                    </div>
                </form>
            </div>
            <!-- end modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
    <!--  Reject conclusion  end modal -->
    @endif
    <!-- View conclusion Scrollable Modal -->
    <div class="modal-dialog">
        <div id="conclusionViewModal" class="modal fade" tabindex="-1" aria-labelledby="conclusionViewModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="conclusionViewModalLabel">Modal Heading</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <object data="" type="application/pdf" style="height: 100vh;border: none;"
                                standby="loading" width="100%"></object>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>
    <!-- View conclusion end Modal-->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/orders/show.js') }}"></script>

@endsection
