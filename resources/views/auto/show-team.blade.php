@extends('layouts.master')
@section('title')
    @lang('translation.overview')
@endsection
@section('content')
    @component('components.order-breadcrumbs', ['order' => $order, 'active' => 'team'])
    @endcomponent
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content text-muted">
                <div class="tab-pane fade show active" id="project-team" role="tabpanel">
                    <div class="row g-4 mb-3">
                        <div class="col-sm">
                            <div class="d-flex">
                                <div class="search-box me-2">
                                    <input type="text" class="form-control"
                                           placeholder="@lang('translation.search-here')">
                                    <i class="ri-search-line search-icon"></i>
                                </div>
                            </div>
                        </div>
                        @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                            <div class="col-sm-auto">
                                <div>
                                    <button type="button" class="btn btn-danger" data-bs-toggle="modal"
                                            data-bs-target="#inviteMembersModal"><i
                                            class="ri-share-line me-1 align-bottom"></i>
                                        @lang('translation.invite-member')
                                    </button>
                                </div>
                            </div>
                        @endif
                    </div>
                    <!-- end row -->

                    <div class="team-list list-view-filter">
                        @foreach ($members as $teamMember)
                            <div class="card team-box">
                                <div class="card-body px-4">
                                    <div class="row align-items-center team-row">
                                        <div class="col team-settings">
                                            <div class="row align-items-center">
                                                <div class="col">
                                                    <div class="flex-shrink-0 me-2">
                                                        <button type="button" class="btn fs-16 p-0 favourite-btn">
                                                            <i class="ri-star-fill"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                                <div class="col text-end dropdown">
                                                    <a href="javascript:void(0);" data-bs-toggle="dropdown"
                                                       aria-expanded="false">
                                                        <i class="ri-more-fill fs-17"></i>
                                                    </a>
                                                    <ul class="dropdown-menu dropdown-menu-end">
                                                        <li><a class="dropdown-item"
                                                               href="{{ route('user.show', [$teamMember->id]) }}"><i
                                                                    class="ri-eye-fill text-muted me-2 align-bottom"></i>@lang('translation.view')
                                                            </a>
                                                        </li>
                                                        @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                                            <li>
                                                                <form name="order-member{{ $teamMember->id }}"
                                                                      action="{{ route('member.remove') }}"
                                                                      method="post"
                                                                      novalidate>
                                                                    @csrf
                                                                    @method('post')
                                                                    <input type="hidden" value="{{ $teamMember->id }}"
                                                                           name="id">
                                                                    <input type="hidden" value="{{ $order->id }}"
                                                                           name="order_id">
                                                                    <input type="hidden"
                                                                           value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}"
                                                                           name="order_type">
                                                                    <button type="submit" class="dropdown-item ">
                                                                        <i
                                                                            class="ri-delete-bin-5-fill text-muted me-2 align-bottom"></i>
                                                                        {{ __('translation.delete') }}
                                                                    </button>
                                                                </form>
                                                            </li>
                                                        @endif
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-3 col">
                                            <div class="team-profile-img">
                                                <div class="avatar-lg img-thumbnail rounded-circle">
                                                    <img src="{{ URL::asset($teamMember->avatar) }}" alt=""
                                                         class="img-fluid d-block rounded-circle"/>
                                                </div>
                                                <div class="team-content">
                                                    <a href="{{ route('user.show', [$teamMember->id]) }}"
                                                       class="d-block">
                                                        <h5 class="fs-16 mb-1">
                                                            {{ $teamMember->name }}
                                                        </h5>
                                                    </a>
                                                    <p class="text-muted text-uppercase mb-0">
                                                        {{ $teamMember->role }}
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-5 col">
                                            <div class="row text-muted text-center">
                                                <div class="col-3 border-end border-end-dashed">
                                                    <h5 class="mb-1">{{ $teamMember->started }}</h5>
                                                    <p class="text-muted mb-0">@lang('translation.tasks-started')</p>
                                                </div>
                                                <div class="col-3 border-end border-end-dashed">
                                                    <h5 class="mb-1">{{ $teamMember->finished }}</h5>
                                                    <p class="text-muted mb-0">@lang('translation.projects-finished')</p>
                                                </div>
                                                <div class="col-3 border-end border-end-dashed">
                                                    <h5 class="mb-1">{{ $teamMember->approved }}</h5>
                                                    <p class="text-muted mb-0">@lang('translation.orders-approved')</p>
                                                </div>
                                                <div class="col-3">
                                                    <h5 class="mb-1">{{ $teamMember->rejected }}</h5>
                                                    <p class="text-muted mb-0">@lang('translation.orders-rejected')</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-2 col">
                                            <div class="text-end">
                                                <a href="{{ route('user.show', [$teamMember->id]) }}"
                                                   class="btn btn-light view-btn">@lang('translation.view-profile')</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--end card-->
                        @endforeach
                    </div>
                    <!-- end team list -->
                </div>
                <!-- end tab pane -->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
    @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
    <!-- Modal -->
    <div class="modal fade" id="inviteMembersModal" tabindex="-1" aria-labelledby="inviteMembersModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0">
                <div class="modal-header p-3 ps-4 bg-soft-success">
                    <h5 class="modal-title" id="inviteMembersModalLabel">@lang('translation.members')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="members-invite" action="{{ route('members.invite') }}" method="post">
                    @csrf
                    <div class="modal-body p-4">
                        <input type="hidden" value="{{ $order->id }}" name="order_id">
                        <input type="hidden" value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}" name="order_type">
                        <div class="search-box mb-3">
                            <input type="text" class="form-control bg-light border-light"
                                   placeholder="@lang('translation.search-here')">
                            <i class="ri-search-line search-icon"></i>
                        </div>

                        <div class="mb-4 d-flex align-items-center">
                            <div class="me-2">
                                <h5 class="mb-0 fs-13">@lang('translation.members') :</h5>
                            </div>
                            <div class="avatar-group justify-content-center">
                                @foreach ($members as $member)
                                    <a href="{{ route('user.show', [$teamMember->id]) }}" class="avatar-group-item"
                                       data-bs-toggle="tooltip"
                                       data-bs-trigger="hover" data-bs-placement="top" title="{{ $member->name }}">
                                        <div class="avatar-xs">
                                            <img src="{{ URL::asset($member->avatar) }}" alt=""
                                                 class="rounded-circle img-fluid">
                                        </div>
                                    </a>
                                @endforeach
                            </div>
                        </div>
                        <div class="mx-n4 px-4" data-simplebar style="max-height: 225px;">
                            <div class="vstack gap-3">
                                @foreach ($appraisers as $appraiser)
                                    <div class="d-flex align-items-center">
                                        <div class="avatar-xs flex-shrink-0 me-3">
                                            <img src="{{ URL::asset("{$appraiser->avatar}") }}"
                                                 alt="{{ $appraiser->name }}" class="img-fluid rounded-circle">
                                        </div>
                                        <div class="flex-grow-1">
                                            <h5 class="fs-13 mb-0">
                                                <a href="#" class="text-body d-block">
                                                    {{ $appraiser->name }}
                                                </a>
                                            </h5>
                                        </div>
                                        <!-- Outlined Styles -->
                                        <div class="flex-shrink-0">
                                            <div class="hstack gap-2 flex-wrap">
                                                <input type="checkbox" name="{{ $appraiser->id }}" class="btn-check"
                                                       id="btn-check-outlined-{{ $appraiser->id }}">
                                                <label class="btn btn-sm btn-outline-primary"
                                                       for="btn-check-outlined-{{ $appraiser->id }}">@lang('translation.add')</label>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- end member item -->
                                @endforeach
                            </div>
                            <!-- end list -->
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light w-xs"
                                data-bs-dismiss="modal">@lang('translation.cancel')</button>
                        <button type="submit" class="btn btn-success w-xs">@lang('translation.invite')</button>
                    </div>
                </form>
            </div>
            <!-- end modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
    <!-- end modal -->
    @endif
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
