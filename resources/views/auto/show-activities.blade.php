@extends('layouts.master')
@section('title')
    @lang('translation.overview')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.order-breadcrumbs', ['order' => $order, 'active' => 'activities'])
    @endcomponent
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content text-muted">
                <div class="tab-pane fade show active" id="project-activities" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"> @lang('translation.activities') </h5>
                            <div class="acitivity-timeline py-3">
                                @foreach ($actions as $action)
                                    @component('components.actions.order_created',['action'=>$action])@endcomponent
                                    @component('components.actions.attachment',['action'=>$action])@endcomponent
                                    @component('components.actions.order_updated',['action'=>$action])@endcomponent
                                    @component('components.actions.members_added',['action'=>$action])@endcomponent
                                    @component('components.actions.completed',['action'=>$action])@endcomponent
                                    @component('components.actions.rejected',['action'=>$action])@endcomponent
                                @endforeach
                            </div>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!-- end tab pane -->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/activities.js') }}"></script>

@endsection
