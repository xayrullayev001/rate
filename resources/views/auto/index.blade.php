@extends('layouts.master')
@section('title')
    @lang('translation.auto')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.auto')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <form action="{{ route('auto.index') }}" method="get" autocomplete="off" novalidate>
                    <div class="card-header  border-0">
                        <div class="d-flex align-items-center">
                            <h5 class="card-title mb-0 flex-grow-1">@lang('translation.order-list')</h5>
                            <div class="flex-shrink-0 mx-2">
                                <a href="{{ route('auto.create') }}" type="button" class="btn btn-primary add-btn">
                                    <i class="ri-add-line align-bottom me-1"></i>
                                    @lang('translation.create-order')

                                </a>
                            </div>
                            <div class="flex-shrink-0">
                                @component('components.page-size',['size'=>$size])@endcomponent

                            </div>
                        </div>
                    </div>
                    <div class="card-body border border-dashed border-end-0 border-start-0">
                        <div class="row g-3">
                            <div class="col-xxl-3 col-sm-6">
                                <div class="search-box">
                                    <input type="text" class="form-control search" name="search"
                                           {{--                                           oninput="this.form.submit()"--}}
                                           value="{{ isset($filters['search']) ? $filters['search'] : '' }}"
                                           placeholder="@lang('translation.search-by-number-customer-owner')">
                                    <i class="ri-search-line search-icon"></i>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-2 col-sm-6">
                                <div>
                                    <input type="text" class="form-control flatpickr-input"
                                           data-provider="flatpickr" onchange="this.form.submit()"
                                           name="period" value="{{ $filters['period'] ?? '' }}"
                                           data-date-format="d.m.Y"
                                           data-range-date="true" id="demo-datepicker"
                                           placeholder="@lang('translation.select-period')" readonly="readonly">
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-3 col-sm-4">

                                <div>
                                    <select data-choices data-choices-search-false onchange="this.form.submit()"
                                            data-choices-multiple-groups="false" class="form-control"
                                            name="purpose_id">
                                        <option value="">
                                            @lang('translation.select-purpose')
                                        </option>
                                        @foreach ($purposeCases as $case)
                                            <option value="{{ $case->id }}"
                                                {{ isset($filters['purpose_id']) && $case->id == $filters['purpose_id'] ? 'selected' : '' }}>
                                                @lang($case->{str_replace('_', '-', app()->getLocale())})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--end col-->

                            <div class="col-xxl-3 col-sm-4">
                                <div>
                                    <select data-choices data-choices-multiple-groups="false"
                                            onchange="this.form.submit()"
                                            class="form-control"
                                            name="car_category">
                                        @foreach ($concerns as $concern)
                                            <option value="{{ $concern->id }}"
                                                {{ isset($filters['car_category']) && $concern->id == $filters['car_category'] ? 'selected' : '' }}>
                                                @lang($concern->{str_replace('_', '-', app()->getLocale())})
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <!--end col-->
                            <div class="col-xxl-1 col-sm-4">
                                <div>
                                    <button type="submit" class="btn btn-primary w-100">
                                        <i class="ri-equalizer-fill me-1 align-bottom"></i>
                                    </button>
                                </div>
                            </div>
                            <!--end col-->
                        </div>
                        <!--end row-->
                    </div>
                </form>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-success mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->get('status') === null ? 'active' : '' }}"
                               id="ALL"
                               href="{{ route('auto.index', ['status' => null]) }}">
                                <i class="ri-store-2-fill me-1 align-bottom"></i>
                                @lang('translation.orders')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === \App\Enums\OrderStatusEnum::DRAFT->name ? 'active' : '' }}"
                               id="DRAFT"
                               href="{{ route('auto.index', ['status' => \App\Enums\OrderStatusEnum::DRAFT->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang(\App\Enums\OrderStatusEnum::DRAFT->value)
                                <span
                                    class="badge bg-info align-middle ms-1">{{ $stats->get(\App\Enums\OrderStatusEnum::DRAFT->name) }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === \App\Enums\OrderStatusEnum::STARTED->name ? 'active' : '' }}"

                               href="{{ route('auto.index', ['status' => \App\Enums\OrderStatusEnum::STARTED->name]) }}">
                                <i class="ri-truck-line me-1 align-bottom"></i>
                                @lang(\App\Enums\OrderStatusEnum::STARTED->value)
                                <span
                                    class="badge bg-primary align-middle ms-1">{{ $stats->get(\App\Enums\OrderStatusEnum::STARTED->name) }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === \App\Enums\OrderStatusEnum::FINISHED->name ? 'active' : '' }}"
                               id="FINISHED"
                               href="{{ route('auto.index', ['status' => \App\Enums\OrderStatusEnum::FINISHED->name]) }}">
                                <i class="ri-arrow-left-right-fill me-1 align-bottom"></i>
                                @lang(\App\Enums\OrderStatusEnum::FINISHED->value)
                                <span
                                    class="badge bg-warning align-middle ms-1">{{ $stats->get(\App\Enums\OrderStatusEnum::FINISHED->name) }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === \App\Enums\OrderStatusEnum::REJECTED->name ? 'active' : '' }}"
                               id="REJECTED"
                               href="{{ route('auto.index', ['status' => \App\Enums\OrderStatusEnum::REJECTED->name]) }}">
                                <i class="ri-close-circle-line me-1 align-bottom"></i>
                                @lang(\App\Enums\OrderStatusEnum::REJECTED->value)
                                <span
                                    class="badge bg-danger align-middle ms-1">{{ $stats->get(\App\Enums\OrderStatusEnum::REJECTED->name) }}</span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === \App\Enums\OrderStatusEnum::APPROVED->name ? 'active' : '' }}"
                               id="APPROVED"
                               href="{{ route('auto.index', ['status' => \App\Enums\OrderStatusEnum::APPROVED->name]) }}">
                                <i class="ri-close-circle-line me-1 align-bottom"></i>
                                @lang(\App\Enums\OrderStatusEnum::APPROVED->value)
                                <span
                                    class="badge bg-success align-middle ms-1">{{ $stats->get(\App\Enums\OrderStatusEnum::APPROVED->name) }}</span>
                            </a>
                        </li>
                    </ul>
                </div>
                <div class="table-responsive">
                    <table class="table border table-borderless align-middle table-nowrap mb-0">
                        <thead>
                        <tr>
                            <th scope="col">№</th>
                            <th scope="col">@lang('translation.order-number')</th>
                            <th scope="col">@lang('translation.created-date')</th>
                            <th scope="col">@lang('translation.object-name')</th>
                            <th scope="col">@lang('translation.customer')</th>
                            <th scope="col">@lang('translation.owner')</th>
                            <th scope="col">@lang('translation.purpose')</th>
                            <th scope="col">@lang('translation.diller')</th>
                            <th scope="col">@lang('translation.ordered-customer')</th>
                            <th scope="col">@lang('translation.status')</th>
                            <th scope="col">@lang('translation.actions')</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($orders as $order)
                            <tr>
                                <td class="fw-medium"> {{($orders->currentPage() - 1) * $orders->perPage() + $loop->iteration}}</td>
                                <td class="fw-medium">
                                    <a href="{{ route('auto.show', [$order->id]) }}">
                                        {{ $order->number }}
                                    </a>
                                </td>
                                <td>{{ $order->created_at }}</td>
                                <td>{{ $order->car_mark }}</td>
                                <td>{{ $order->customer }}</td>
                                <td>{{ $order->ownerName }}</td>
                                <td>{{ $order->purposeOne->{str_replace('_', '-', app()->getLocale())} }}</td>
                                <td>@if(isset($order->dillerUser)){{ $order->dillerUser->name }}@endif</td>
                                <td>
                                    {{ $order->ordered_customer }}
                                    <br/>{{ $order->ordered_customer_phone }}
                                </td>
                                <td>
                                    <span class="badge badge-soft-success">
                                        @lang(\App\Enums\OrderStatusEnum::getLabel($order->status))
                                    </span>
                                </td>
                                <td>

                                    <div class="dropdown">
                                        <button
                                            class="btn btn-link text-muted p-1 mt-n2 py-0 text-decoration-none fs-15"
                                            data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                            <i class="ri-more-fill align-bottom me-2 text-muted"></i>
                                        </button>

                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a href="{{ route('auto.show', [$order->id]) }}"
                                               class="dropdown-item">
                                                <i class="ri-eye-fill align-bottom me-2 text-muted"></i>
                                                @lang('translation.view')
                                            </a>
                                            @if ($order->status === \App\Enums\OrderStatusEnum::DRAFT->name)
                                                <a class="dropdown-item" href="{{ route('auto.edit', [$order->id]) }}">
                                                    <i class="ri-pencil-fill align-bottom me-2 text-muted"></i>
                                                    @lang('translation.edit')
                                                </a>
                                            @endif
                                            <div class="dropdown-divider"></div>

                                            <form action="{{ route('auto.clone', [$order->id]) }}" method="post"
                                                  novalidate>
                                                @csrf
                                                @method('put')
                                                <input type="hidden" value="{{ $order->id }}" name="id">
                                                <button type="submit" title="Start this order"
                                                        class="dropdown-item bg-white link-info">
                                                    <i class=" ri-file-copy-line align-middle"></i>
                                                    @lang('translation.clone')
                                                </button>
                                            </form>
                                            @if ($order->status === \App\Enums\OrderStatusEnum::DRAFT->name)

                                                <form action="{{ route('members.invite') }}" method="post" novalidate>
                                                    @csrf
                                                    @method('post')
                                                    <input type="checkbox" name="{{ auth()->id() }}" checked hidden>
                                                    <input type="hidden" value="{{ $order->id }}" name="order_id">
                                                    <input type="hidden"
                                                           value="{{ \App\Enums\OrderTypeEnum::AUTO->name }}"
                                                           name="order_type">
                                                    <button type="submit" title="Start this order"
                                                            class="dropdown-item bg-white link-info">
                                                        <i class=" ri-fingerprint-line align-middle"></i>
                                                        @lang('translation.start')
                                                    </button>
                                                </form>
                                            @endif
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="mt-3">{{ $orders->withQueryString()->links() }}</div>
        </div>
        <!-- end card-body -->
    </div>
    <!-- end card -->
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
