@extends('layouts.master')
@section('title') @lang('translation.dashboards') @endsection
{{--@section('css')--}}
{{--    <link href="{{ URL::asset('assets/libs/swiper/swiper.min.css')}}" rel="stylesheet" type="text/css"/>--}}
{{--@endsection--}}
@section('content')
    @component('components.breadcrumb')
        @slot('li_1') @lang('translation.dashboards') @endslot
        @slot('title')  @endslot
    @endcomponent
    <div class="row">
        <div class="col">

            <div class="h-100">
                <div class="row mb-3 pb-1">
                    <div class="col-12">
                        <div class="d-flex align-items-lg-center flex-lg-row flex-column">
                            <div class="flex-grow-1">
                                <h4 class="fs-16 mb-1">@lang('translation.good-morning'), {{ Auth()->user()->name }}
                                    !</h4>
                                <p class="text-muted mb-0">@lang('translation.happening-today')</p>
                            </div>
                            <div class="mt-3 mt-lg-0">
                                <form action="javascript:void(0);">
                                    <div class="row g-3 mb-0 align-items-center">
                                        <div class="col-sm-auto">
                                            <div class="input-group">
                                                <input type="text"
                                                       class="form-control border-0 dash-filter-picker shadow"
                                                       data-provider="flatpickr" data-range-date="true"
                                                       data-date-format="d M, Y"
                                                       data-deafult-date="01 Jan 2022 to 31 Jan 2022">
                                                <div
                                                    class="input-group-text bg-primary border-primary text-white">
                                                    <i class="ri-calendar-2-line"></i>
                                                </div>
                                            </div>
                                        </div>
                                        <!--end col-->
                                        <div class="col-auto">
                                            <a href="{{ route('auto.create') }}" type="button"
                                               class="btn btn-soft-success"><i
                                                    class="ri-add-circle-line align-middle me-1"></i>
                                                @lang('translation.auto')
                                            </a>
                                        </div>
                                        <!--end col-->
                                        <div class="col-auto">
                                            <a href="{{ route('estate.create') }}" type="button"
                                               class="btn btn-soft-success"><i
                                                    class="ri-add-circle-line align-middle me-1"></i>
                                                @lang('translation.estate')
                                            </a>
                                        </div>
                                        <!--end col-->
                                        <div class="col-auto">
                                            <button type="button"
                                                    class="btn btn-soft-info btn-icon waves-effect waves-light layout-rightside-btn">
                                                <i
                                                    class="ri-pulse-line"></i></button>
                                        </div>
                                        <!--end col-->
                                    </div>
                                    <!--end row-->
                                </form>
                            </div>
                        </div><!-- end card header -->
                    </div>
                    <!--end col-->
                </div>
                <!--end row-->

                <div class="row">


                    @component('components.home.summary-price',['summary'=>$summaryPrice])@endcomponent
                    @component('components.home.auto-orders',['count'=>$autoOrders])@endcomponent
                    @component('components.home.estate-orders',['count'=>$estateOrders])@endcomponent
                    @component('components.home.balance')@endcomponent
                    @component('components.home.evaluators',['labels'=>json_encode($appraiserChart[0]),'values'=>json_encode($appraiserChart[1])])@endcomponent
                    @component('components.home.recent-orders',['orders'=>$recentOrders])@endcomponent
                </div> <!-- end row-->
                <div class="row">
                    <div class="col-xl-6">
                        <div class="card">
                            <div class="card-header align-items-center d-flex">
                                <h4 class="card-title mb-0 flex-grow-1">@lang('translation.unmoderated-conclusions')</h4>
                                <div class="flex-shrink-0">
                                    <div class="dropdown card-header-dropdown">
                                        <a class="text-reset dropdown-btn" href="#"
                                           data-bs-toggle="dropdown" aria-haspopup="true"
                                           aria-expanded="false">
                                        <span class="fw-semibold text-uppercase fs-12">Sort by:
                                        </span><span class="text-muted">Today<i
                                                    class="mdi mdi-chevron-down ms-1"></i></span>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-end">
                                            <a class="dropdown-item" href="#">Today</a>
                                            <a class="dropdown-item" href="#">Yesterday</a>
                                            <a class="dropdown-item" href="#">Last 7 Days</a>
                                            <a class="dropdown-item" href="#">Last 30 Days</a>
                                            <a class="dropdown-item" href="#">This Month</a>
                                            <a class="dropdown-item" href="#">Last Month</a>
                                        </div>
                                    </div>
                                </div>
                            </div><!-- end card header -->

                            <div class="card-body">
                                <div class="table-responsive table-card">
                                    <table
                                        class="table table-hover table-centered align-middle table-nowrap mb-0">
                                        <tbody>
                                        @foreach($forModerate as $moderate)
                                            <tr>
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div
                                                            class="avatar-sm bg-light rounded p-1 me-2">
                                                            <img
                                                                @if($moderate->type=='estate_')
                                                                src="{{ URL::asset('storage/attachments/estate_'.$moderate->id.'/qr.png') }}"
                                                                @else
                                                                src="{{ URL::asset('storage/attachments/auto_'.$moderate->id.'/qr.png') }}"
                                                                @endif
                                                                alt="" class="img-fluid d-block"/>
                                                        </div>
                                                        <div>
                                                            <h5 class="fs-14 my-1">
                                                                <a href="@if($moderate->type=='estate_') {{ route('estate.show',[$moderate->id]) }} @else {{ route('auto.show',[$moderate->id]) }}@endif"
                                                                   class="text-reset">
                                                                    {{ $moderate->number }}
                                                                </a>
                                                            </h5>
                                                            <span class="text-muted">{{ $moderate->updated_at }}</span>
                                                        </div>
                                                    </div>
                                                </td>
                                                <td>
                                                    <h5 class="fs-14 my-1 fw-normal">
                                                        @component('components.price',['balance'=>$moderate->cost]) @endcomponent
                                                    </h5>
                                                    <span class="text-muted">@lang('translation.summary-fee')</span>
                                                </td>
                                                <td>
                                                    <h5 class="fs-14 my-1 fw-normal">
                                                        @component('components.price',['balance'=>$moderate->price]) @endcomponent
                                                    </h5>
                                                    <span class="text-muted">
                                                        @lang('translation.amount-paid')
                                                    </span>
                                                </td>
                                                <td>
                                                    <h5 class="fs-14 my-1 fw-normal">
                                                        @component('components.price',['balance'=>$moderate->object_price]) @endcomponent
                                                    </h5>
                                                    <span
                                                        class="text-muted">@lang('translation.conclusion-price')</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end row-->


            </div> <!-- end .h-100-->

        </div> <!-- end col -->

        <div class="col-auto layout-rightside-col d-block">
            <div class="overlay"></div>
            <div class="layout-rightside">
                <div class="card h-100 rounded-0">
                    <div class="card-body p-0">
                        <div class="p-3">
                            <h6 class="text-muted mb-0 text-uppercase fw-semibold">@lang('translation.recent-activity')</h6>
                        </div>
                        <div data-simplebar style="max-height: 410px;" class="p-3 pt-0">
                            <div class="acitivity-timeline acitivity-main">
                                @foreach ($actions as $action)
                                    @component('components.actions.order_created',['action'=>$action])@endcomponent
                                    @component('components.actions.members_added',['action'=>$action])@endcomponent
                                    @component('components.actions.order_updated',['action'=>$action])@endcomponent
                                    @component('components.actions.attachment',['action'=>$action])@endcomponent
                                    @component('components.actions.completed',['action'=>$action])@endcomponent
                                    @component('components.actions.rejected',['action'=>$action])@endcomponent
                                @endforeach
                            </div>
                        </div>
                        @if(isset($topEstateCategories))
                            <div class="p-3 mt-2">
                                <h6 class="text-muted mb-3 text-uppercase fw-semibold">
                                    @lang('translation.top-estate-categories')
                                </h6>
                                <ol class="ps-3 text-muted">
                                    @foreach($topEstateCategories as $category)
                                        <li class="py-1">
                                            <a href="#" class="text-muted">
                                                @lang($category->purpose)
                                                <span class="float-end">
                                                    ({{ $category->qty }})
                                                </span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ol>
                                <div class="mt-3 text-center">
                                    <a href="{{ route('estate.index') }}"
                                       class="text-muted text-decoration-underline">@lang('translation.view-all-categories')</a>
                                </div>
                            </div>
                        @endif
                        @if(isset($topAutoCategories))
                            <div class="p-3 mt-2">
                                <h6 class="text-muted mb-3 text-uppercase fw-semibold">
                                    @lang('translation.top-auto-categories')
                                </h6>
                                <ol class="ps-3 text-muted">
                                    @foreach($topAutoCategories as $category)
                                        <li class="py-1">
                                            <a href="#" class="text-muted">
                                                @lang($category->purpose)
                                                <span class="float-end">
                                                    ({{ $category->qty }})
                                                </span>
                                            </a>
                                        </li>
                                    @endforeach
                                </ol>
                                <div class="mt-3 text-center">
                                    <a href="{{ route('auto.index') }}"
                                       class="text-muted text-decoration-underline">@lang('translation.view-all-categories')</a>
                                </div>
                            </div>
                        @endif
                        <div class="card sidebar-alert bg-light border-0 text-center mx-4 mb-0 mt-3">
                            <div class="card-body">
                                <img src="{{ URL::asset('assets/images/giftbox.png') }}" alt="">
                                <div class="mt-4">
                                    <h5>Invite New Seller</h5>
                                    <p class="text-muted lh-base">Refer a new seller to us and earn $100
                                        per refer.</p>
                                    <button type="button"
                                            class="btn btn-primary btn-label rounded-pill"><i
                                            class="ri-mail-fill label-icon align-middle rounded-pill fs-16 me-2"></i>
                                        Invite Now
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div> <!-- end card-->
            </div> <!-- end .rightbar-->

        </div> <!-- end col -->
    </div>

@endsection
@section('script')

    <!-- apexcharts -->
    <script src="{{ URL::asset('/assets/libs/apexcharts/apexcharts.min.js') }}"></script>
    <!-- dashboard init -->
    <script src="{{ URL::asset('/assets/js/plugins.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/dashboard.js') }}"></script>

    <script>

        /* Toastify({
             text: "This is a toast",
             duration: 3000,
             destination: "https://github.com/apvarun/toastify-js",
             newWindow: true,
             close: true,
             gravity: "top", // `top` or `bottom`
             position: "right", // `left`, `center` or `right`
             stopOnFocus: true, // Prevents dismissing of toast on hover
             style: {
                 background: "#405189",
             },
             onClick: function () {
             } // Callback after click
         }).showToast();*/


    </script>
@endsection
