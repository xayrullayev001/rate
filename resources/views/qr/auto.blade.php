@extends('layouts.master-without-nav')
@section('title')
    @lang('translation.qr')
@endsection
@section('content')


    <div class="auth-page-wrapper pt-5">
        <!-- auth page bg -->
        <div class="auth-one-bg-position auth-one-bg" id="auth-particles">
            <div class="bg-overlay"></div>

            <div class="shape">
                <svg xmlns="http://www.w3.org/2000/svg" version="1.1"
                     viewBox="0 0 1440 120">
                    <path d="M 0,36 C 144,53.6 432,123.2 720,124 C 1008,124.8 1296,56.8 1440,40L1440 140L0 140z"></path>
                </svg>
            </div>
        </div>

        <!-- qr page content -->
        <div class="auth-page-content">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center mt-sm-5 mb-4 text-white-50">
                            <div>
                                <h1 class="text-white">"Sifat Baxolash" MCHJ</h1>
                                <h1 class="text-white">@lang('translation.order-number'): {{ $order->number }}</h1>
                                <h1 class="text-white">@lang('translation.date'): {{ $order->created_at }}</h1>
                                <h1 class="badge rounded-pill bg-info fs-18">
                                    @lang(\App\Enums\OrderStatusEnum::getLabel($order->status))
                                </h1>
                                @if($conclusion)
                                    <h1 class="badge rounded-pill bg-info fs-18">
                                        <a href="{{ route('download', ['file' => $conclusion->id]) }}"
                                           class="text-white">
                                            @lang("translation.download")
                                        </a>
                                    </h1>
                                @endif
                            </div>
                            <p class="mt-3 fs-15 fw-medium">{{ $order->name }}</p>
                        </div>
                    </div>
                </div>
                <!-- end row -->

                <div class="row justify-content-center text-uppercase">
                    <div class="col-md-8 col-lg-6 col-xl-10">
                        <div class="card mt-4">
                            <div class="card-body p-4">
                                <div class="text-center mt-2">
                                    <img width="100" src="{{ URL::asset('storage/attachments/auto_'.$order->id.'/qr.png') }}"
                                         alt="Order qr code image">
                                </div>
                                <div class="p-2 mt-4">
                                    <div class="col-lg-12">
                                        <div class="row">
                                            <div class="col-lg-6">
                                                <ul class="ps-4 vstack gap-2">
                                                    <li>
                                                        @lang('translation.car_category') :
                                                        {{ $order->concernOne->{str_replace('_', '-', app()->getLocale())} }}
                                                    </li>
                                                    <li>@lang('translation.car-mark') :
                                                        {{ $order->car_mark }}
                                                    </li>
                                                    <li>@lang('translation.car-number') :
                                                        {{ $order->car_number }}
                                                    </li>
                                                    <li>@lang('translation.made-date') :
                                                        {{ $order->made_date }}
                                                    </li>
                                                    <li>@lang('translation.color') :
                                                        {{ $order->color }}
                                                    </li>
                                                    <li>@lang('translation.body') :
                                                        {{ $order->body }}
                                                    </li>
                                                    <li>@lang('translation.technical-passport') :
                                                        {{ $order->tech_passport }}
                                                    </li>
                                                    <li>@lang('translation.technical-passport-given-date') :
                                                        {{ $order->tech_given_date }}
                                                    </li>
                                                    <li>@lang('translation.technical-passport-given-by-whom') :
                                                        {{ $order->tech_given_whom }}
                                                    </li>
                                                    <li>@lang('translation.type') :
                                                        {{ $order->type }}
                                                    </li>
                                                </ul>
                                            </div>
                                            <div class="col-lg-6">
                                                <ul class="ps-4 vstack gap-2">
                                                    <li>@lang('translation.owner') : {{ $order->ownerName }}</li>
                                                    <li>@lang('translation.customer'): {{ $order->customer }}</li>
                                                    <li>@lang('translation.purpose')
                                                        : {{ $order->purposeOne->{str_replace('_', '-', app()->getLocale())} }}</li>
                                                    <li>
                                                        @lang('translation.object-price')
                                                        : @component('components.price',['balance'=>$order->object_price]) @endcomponent
                                                    </li>

                                                </ul>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end card body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
                <!-- end row -->
            </div>
            <!-- end container -->
        </div>
        <!-- end auth page content -->

        <!-- footer -->
        <footer class="footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="text-center">
                            {{ $order->created_at }}
                            Buyurtma baxolandi
                            <i class="mdi mdi-heart text-danger"></i> by Sifat baxolash</p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- end Footer -->
    </div>
    <!-- end auth-page-wrapper -->
@endsection
