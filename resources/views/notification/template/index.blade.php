@extends('layouts.master')
@section('title') @lang('translation.notification-template')  @endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1') Pages @endslot
        @slot('title') @lang('translation.notification-template')  @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header align-items-center d-flex">
                    <h4 class="card-title mb-0 flex-grow-1">Tables Without Borders</h4>
                    <div class="flex-shrink-0">
                        <div class="form-check form-switch form-switch-right form-switch-md">
                            <label for="tables-without-border-showcode" class="form-label text-muted">Show Code</label>
                            <input class="form-check-input code-switcher" type="checkbox"
                                   id="tables-without-border-showcode">
                        </div>
                    </div>
                </div><!-- end card header -->

                <div class="card-body">
                    <p class="text-muted">Use <code>table-borderless</code> to set a table without borders.</p>
                    <div class="live-preview">
                        <div class="table-responsive">
                            <table class="table table-borderless align-middle table-nowrap mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">ID</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Job Title</th>
                                    <th scope="col">Date</th>
                                    <th scope="col">Status</th>
                                    <th scope="col"></th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td class="fw-medium">01</td>
                                    <td>Annette Black</td>
                                    <td>Industrial Designer</td>
                                    <td>10, Nov 2021</td>
                                    <td><span class="badge badge-soft-success">Active</span></td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <a href="javascript:void(0);" class="link-primary"><i
                                                    class="ri-settings-4-line"></i></a>
                                            <a href="javascript:void(0);" class="link-danger"><i
                                                    class="ri-delete-bin-5-line"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-medium">02</td>
                                    <td>Bessie Cooper</td>
                                    <td>Graphic Designer</td>
                                    <td>13, Nov 2021</td>
                                    <td><span class="badge badge-soft-success">Active</span></td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <a href="javascript:void(0);" class="link-primary"><i
                                                    class="ri-settings-4-line"></i></a>
                                            <a href="javascript:void(0);" class="link-danger"><i
                                                    class="ri-delete-bin-5-line"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-medium">03</td>
                                    <td>Leslie Alexander</td>
                                    <td>Product Manager</td>
                                    <td>17, Nov 2021</td>
                                    <td><span class="badge badge-soft-success">Active</span></td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <a href="javascript:void(0);" class="link-primary"><i
                                                    class="ri-settings-4-line"></i></a>
                                            <a href="javascript:void(0);" class="link-danger"><i
                                                    class="ri-delete-bin-5-line"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="fw-medium">04</td>
                                    <td>Lenora Sandoval</td>
                                    <td>Applications Engineer</td>
                                    <td>25, Nov 2021</td>
                                    <td><span class="badge badge-soft-danger">Disabled</span></td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <a href="javascript:void(0);" class="link-primary"><i
                                                    class="ri-settings-4-line"></i></a>
                                            <a href="javascript:void(0);" class="link-danger"><i
                                                    class="ri-delete-bin-5-line"></i></a>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
        @endsection
        @section('script')
            <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script>

            <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
