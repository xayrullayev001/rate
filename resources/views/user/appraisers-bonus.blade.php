@extends('layouts.master')
@section('title')
    @lang('translation.appraisers-bonus')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang("translation.pages")
        @endslot
        @slot('title')
            @lang('translation.appraisers-bonus')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1"> @lang('translation.appraisers-bonuses',[$appraiser->name]) </h5>
                        <form action="{{ route('appraisers.bonus') }}" method="get" autocomplete="off" novalidate>
                            <div class="flex-shrink-0">
                                <input type="hidden" name="appraiser_id" value="{{  $appraiser->id }}">
                                @component('components.page-size',['size'=>$size])@endcomponent
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">@lang('translation.order-number')</th>
                                <th scope="col">@lang('translation.order-customer')</th>
                                <th scope="col">@lang('translation.amount')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($bonuses as $bonus)
                                <tr>
                                    <td class="fw-medium">{{($bonuses->currentPage() - 1) * $bonuses->perPage() + $loop->iteration}}</td>
                                    <td>
                                        <a href="{{ route('auto.show', [$bonus->order_id]) }}">
                                            {{  $bonus->number }}
                                        </a>
                                    </td>
                                    <td>
                                        {{ $bonus->customer }}
                                    </td>
                                    <td>
                                        @component('components.price', ['balance' => $bonus->amount])
                                        @endcomponent
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $bonuses->withQueryString()->links() }}</div>

                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
@endsection
