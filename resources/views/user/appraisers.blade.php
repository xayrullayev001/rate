@extends('layouts.master')
@section('title')
    @lang('translation.appraisers')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.appraisers')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-0">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.order-list')</h5>

                        @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                            <div class="flex-shrink-0">
                                <a href="javascript:void(0);" data-bs-toggle="modal"
                                   title="Update user informations" data-bs-target="#givingBonusModal"
                                   class="btn btn-outline-warning waves-effect waves-light mx-3">
                                    @lang('translation.give-bonus')
                                </a>
                            </div>
                        @endif
                        <div class="flex-shrink-0">
                            <form action="{{ route('user.appraisers') }}" method="get" autocomplete="off" novalidate>
                                @component('components.page-size',['size'=>$size])@endcomponent
                            </form>
                        </div>
                    </div>

                    <div class="card-body">
                        <ul class="nav nav-tabs nav-tabs-custom nav-success mb-3" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link py-3 {{ request()->get('status') === null ? 'active' : '' }}"
                                   href="{{ route('user.appraisers', ['status' => null]) }}">
                                    <i class="ri-store-2-fill me-1 align-bottom"></i>
                                    @lang('translation.appraisers')
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-3 {{ request()->status === 'active' ? 'active' : '' }}"
                                   href="{{ route('user.appraisers', ['status' => 'active']) }}">
                                    <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                    @lang('translation.active')
                                    <span class="badge bg-info bg-gradient align-middle ms-1">
                                    {{ $statusStats['active'] ?? 0 }}
                                </span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link py-3 {{ request()->status === 'inactive' ? 'active' : '' }}"
                                   href="{{ route('user.appraisers', ['status' => 'inactive']) }}">
                                    <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                    @lang('translation.inactive')
                                    <span class="badge bg-danger align-middle ms-1">
                                    {{ $statusStats['inactive'] ?? 0 }}
                                </span>
                                </a>
                            </li>
                        </ul>
                        <div class="table-responsive">
                            <table class="table table-borderless align-middle table-nowrap mb-0">
                                <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">@lang('translation.avatar')</th>
                                    <th scope="col">@lang('translation.user-name')</th>
                                    <th scope="col">@lang('translation.phone-number')</th>
                                    <th scope="col">@lang('translation.registered-date')</th>
                                    <th scope="col">@lang('translation.amount')</th>
                                    <th scope="col">@lang('translation.status')</th>
                                    <th scope="col">@lang('translation.actions')</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td class="fw-medium">{{($users->currentPage() - 1) * $users->perPage() + $loop->iteration}}</td>
                                        <td>
                                            <img class="rounded-circle header-profile-user" alt="200x200"
                                                 src="{{ URL::asset($user->avatar) }}">
                                        </td>
                                        <td>
                                            <a class="text-decoration-underline"
                                               href="{{ route('user.show', [$user->id]) }}">
                                                {{ $user->name }}
                                            </a>
                                        </td>
                                        <td>
                                            @component('components.callable-phone',['phone'=>$user->phone]) @endcomponent</td>
                                        <td>{{ $user->created_at }}</td>
                                        <td>
                                            <form method="get" action="{{ route('appraisers.bonus') }}">
                                                <input type="hidden" name="appraiser_id" value="{{ $user->id }}">
                                                <button
                                                    data-bs-toggle="tooltip"
                                                    data-bs-placement="bottom" style="border: none;background: none"
                                                    class="waves-effect waves-light text-decoration-underline border-none"
                                                    title="@lang('translation.view-all-bonuses')">
                                                    @component('components.price',['balance'=>$user->balance]) @endcomponent
                                                </button>
                                            </form>
                                        </td>
                                        <td class="text-uppercase">
                                            @if ($user->status == 'active')
                                                <button type="button"
                                                        class="btn btn-success btn-label waves-effect waves-light">
                                                    <i class="ri-emotion-happy-line label-icon align-middle fs-16 me-2"></i>
                                                    @lang('translation.'.$user->status)
                                                </button>
                                            @else
                                                <button type="button"
                                                        class="btn btn-danger btn-label waves-effect waves-light">
                                                    <i
                                                        class="ri-emotion-unhappy-line label-icon align-middle fs-16 me-2"></i>
                                                    @lang('translation.'.$user->status)
                                                </button>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="hstack gap-3 fs-15">
                                                @if(in_array(Auth::user()->role,['admin',\App\Enums\RoleEnum::MANAGER->name]))
                                                    <form action="{{ route('user.activate', ['id' => $user->id]) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('put')
                                                        <button type="submit" data-bs-toggle="tooltip"
                                                                data-bs-placement="bottom"
                                                                class="btn btn-outline-primary btn-icon waves-effect waves-light"
                                                                title="Setting user status is active">
                                                            <i class="ri-emotion-happy-line"></i>
                                                            <!-- Buttons with Label -->
                                                        </button>
                                                    </form>
                                                    <form action="{{ route('user.block', ['id' => $user->id]) }}"
                                                          method="POST">
                                                        @csrf
                                                        @method('put')
                                                        <button type="submit"
                                                                class="btn btn-outline-info btn-icon waves-effect waves-light"
                                                                data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                                title="Setting user status is block">
                                                            <i class=" ri-emotion-unhappy-line"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                                <a href="{{ route('user.show', ['id' => $user->id]) }}"
                                                   data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                   title="View user profile page"
                                                   class="btn btn-outline-warning btn-icon waves-effect waves-light">
                                                    <i class="ri-eye-line"></i>
                                                </a>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="mt-3">{{ $users->withQueryString()->links() }}</div>
                    </div><!-- end card-body -->
                </div><!-- end card -->
            </div><!-- end col -->
        </div>
    </div>

    <!-- Give bonus to Appraiser Modal -->
    <div class="modal fade" id="givingBonusModal" tabindex="-1" aria-labelledby="givingBonusModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content border-0">
                <div class="modal-header p-3 ps-4 bg-soft-success">
                    <h5 class="modal-title" id="givingBonusModalLabel">@lang('translation.set-price')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('credit.store') }}" method="POST">
                    @csrf
                    @method('put')

                    <div class="modal-body p-4">
                        <div class="card-body">
                            <input type="hidden" value="1-13333" name="order_id" id="order_id">
                            <input type="hidden" value="null" name="order_type" id="order_type">
                            <!-- Input with Label -->
                            <div class="m-3">
                                <label for="appraiser"
                                       class="form-label">@lang('translation.select-appraiser')</label>
                                <select class="form-select mb-3" name="appraiser_id" id="appraiser"
                                        aria-label="Select appraiser or many appraisers" data-choices
                                        data-choices-search-true
                                        data-choices-removeItem required @error('appraiser_id') is-invalid @enderror>
                                    @foreach ($appraisers as $appraiser)
                                        <option class="text-uppercase" value="{{ $appraiser->id }}">
                                            {{ $appraiser->name }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('appraiser_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <!-- Input with Label -->
                            <div class="m-3">
                                <label for="cleave-numeral"
                                       class="form-label">@lang('translation.setting-bonus')</label>
                                <input type="text" name="amount" class="form-control " id="cleave-numeral" required
                                       @error('amount') is-invalid @enderror>
                                @error('amount')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <!-- Input with Label -->
                            <div class="m-3">
                                <label for="note" class="form-label">
                                    @lang('translation.note')
                                </label>
                                <textarea type="text" name="note" class="form-control " id="note"> </textarea>
                            </div>
                        </div>
                        <!-- end card body -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light w-xs" data-bs-dismiss="modal">
                            @lang('translation.cancel')
                        </button>
                        <button type="submit" class="btn btn-success w-xs">
                            @lang('translation.save')
                        </button>
                    </div>
                </form>
            </div>
            <!-- end modal-content -->
        </div>
        <!-- modal-dialog -->
    </div>
    <!-- Give bonus to Appraiser end modal -->

@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
