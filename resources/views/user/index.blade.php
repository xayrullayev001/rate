@extends('layouts.master')
@section('title')
    @lang('translation.users')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.users')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-1">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.users')</h5>
                        <div class="flex-shrink-0 mx-2">
                            <button data-bs-toggle="modal" data-bs-target="#userCreateModal" type="button"
                                    class="btn btn-primary add-btn">
                                <i class="ri-add-line align-bottom me-1"></i>
                                @lang('translation.create-user-account')
                            </button>
                        </div>
                        <div class="flex-shrink-0 ">
                            <form action="{{ route('user.index') }}" method="get" autocomplete="off" novalidate>
                                @component('components.page-size',['size'=>$size]) @endcomponent
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-success mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->get('status') === null ? 'active' : '' }}"
                               href="{{ route('user.index', ['status' => null]) }}">
                                <i class="ri-store-2-fill me-1 align-bottom"></i>
                                @lang('translation.users')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === 'active' ? 'active' : '' }}"
                               href="{{ route('user.index', ['status' => 'active']) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang('translation.active')
                                <span class="badge bg-info bg-gradient align-middle ms-1">
                                    {{ $statusStats['active'] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === 'inactive' ? 'active' : '' }}"
                               href="{{ route('user.index', ['status' => 'inactive']) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang('translation.inactive')
                                <span class="badge bg-danger align-middle ms-1">
                                    {{ $statusStats['inactive'] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item my-3">|
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::USER->name ? 'active' : '' }}"
                               href="{{ route('user.index', ['role' => \App\Enums\RoleEnum::USER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang(\App\Enums\RoleEnum::USER->value)
                                <span class="badge bg-success bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::USER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::APPRAISER->name ? 'active' : '' }}"
                               href="{{ route('user.index', ['role' => \App\Enums\RoleEnum::APPRAISER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang( \App\Enums\RoleEnum::APPRAISER->value)
                                <span class="badge bg-primary bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::APPRAISER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::DILLER->name ? 'active' : '' }}"
                               href="{{ route('user.index', ['role' => \App\Enums\RoleEnum::DILLER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang( \App\Enums\RoleEnum::DILLER->value )
                                <span class="badge bg-success bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::DILLER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>

                    </ul>
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">@lang('translation.avatar')</th>
                                <th scope="col">@lang('translation.username')</th>
                                <th scope="col">@lang('translation.phone-number')</th>
                                <th scope="col">@lang('translation.registered-date')</th>
                                <th scope="col">@lang('translation.role')</th>
                                <th scope="col">@lang('translation.status')</th>
                                <th scope="col">@lang('translation.actions')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($users as $user)
                                <tr>
                                    <td class="fw-medium">{{($users->currentPage() - 1) * $users->perPage() + $loop->iteration}}</td>
                                    <td>
                                        <img class="rounded-circle header-profile-user" alt="200x200"
                                             src="{{ URL::asset($user->avatar) }}">
                                    </td>
                                    <td>
                                        <a class="text-decoration-underline"
                                           href="{{ route('user.show', [$user->id]) }}">
                                            {{ $user->name }}
                                        </a>
                                    </td>
                                    <td>
                                        @component('components.callable-phone',['phone'=>$user->phone])@endcomponent
                                    </td>
                                    <td>{{ $user->created_at }}</td>
                                    <td class="text-uppercase">
                                        <span class="badge badge-soft-info text-uppercase">
                                        @lang('translation.'.$user->role)
                                        </span>
                                    </td>
                                    <td class="text-uppercase">
                                        @if ($user->status == 'active')
                                            <button type="button"
                                                    class="btn btn-success btn-label waves-effect waves-light">
                                                <i class="ri-emotion-happy-line label-icon align-middle fs-16 me-2"></i>
                                                @lang('translation.'.$user->status)
                                            </button>
                                        @else
                                            <button type="button"
                                                    class="btn btn-danger btn-label waves-effect waves-light">
                                                <i
                                                    class="ri-emotion-unhappy-line label-icon align-middle fs-16 me-2"></i>
                                                @lang('translation.'.$user->status)
                                            </button>
                                        @endif
                                    </td>
                                    <td>
                                        <div class="hstack gap-3 fs-15">
                                            <form action="{{ route('user.activate', ['id' => $user->id]) }}"
                                                  method="POST">
                                                @csrf
                                                @method('put')
                                                <button type="submit" data-bs-toggle="tooltip"
                                                        data-bs-placement="bottom"
                                                        class="btn btn-outline-success btn-icon waves-effect waves-light"
                                                        title="Setting user status is active">
                                                    <i class="ri-emotion-happy-line"></i>
                                                    <!-- Buttons with Label -->
                                                </button>
                                            </form>

                                            <form action="{{ route('user.block', ['id' => $user->id]) }}"
                                                  method="POST">
                                                @csrf
                                                @method('put')
                                                <button type="submit"
                                                        class="btn btn-outline-danger btn-icon waves-effect waves-light"
                                                        data-bs-toggle="tooltip" data-bs-placement="bottom"
                                                        title="Setting user status is block">
                                                    <i class=" ri-emotion-unhappy-line"></i>
                                                </button>
                                            </form>
                                            <a href="{{ route('user.show', ['id' => $user->id]) }}"
                                               data-bs-toggle="tooltip" data-bs-placement="bottom"
                                               title="View user profile page"
                                               class="btn btn-outline-info btn-icon waves-effect waves-light">
                                                <i class="ri-eye-line"></i>
                                            </a>
                                            <a href="javascript:void(0);" data-bs-toggle="modal"
                                               title="Update user informations" data-bs-target="#userUpdateModal"
                                               data-bs-user-id="{{ $user->id }}"
                                               data-bs-user-name="{{ $user->name }}"
                                               data-bs-user-phone="{{ $user->phone }}"
                                               class="btn btn-outline-warning btn-icon waves-effect waves-light">
                                                <i class="ri-pencil-line"></i>
                                            </a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $users->withQueryString()->links() }}</div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>

    <!-- Varying modal content -->
    <div class="modal fade" id="userCreateModal" tabindex="-1" aria-labelledby="userCreateModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userCreateModalLabel">
                        @lang('translation.create-user-account')
                    </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="user-create-modal" class="needs-validation" novalidate method="post"
                      action="{{ route('register') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="modal-body">
                        <div class="mb-3">
                            <label for="cleave-phone" class="form-label">
                                @lang('translation.phone')
                                <span class="text-danger">*</span>
                            </label>
                            <input type="phone" class="form-control @error('phone') is-invalid @enderror"
                                   name="phone" id="cleave-phone" placeholder="@lang('translation.enter-phone')"
                                   required>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="invalid-feedback">
                                @lang('translation.enter-phone')
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="username" class="form-label">
                                @lang('translation.username')
                                <span class="text-danger">*</span>
                            </label>
                            <input type="text" class="form-control @error('name') is-invalid @enderror" name="name"
                                   id="username" placeholder="@lang('translation.enter-username')" required>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="invalid-feedback">
                                @lang('translation.enter-username')
                            </div>
                        </div>

                        <div class="mb-2">
                            <label for="userpassword" class="form-label">
                                @lang('translation.password')
                                <span class="text-danger">*</span>
                            </label>
                            <input type="password" class="form-control @error('password') is-invalid @enderror"
                                   name="password" id="userpassword" placeholder="@lang('translation.enter-password')"
                                   required>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="invalid-feedback">
                                @lang('translation.enter-password')
                            </div>
                        </div>
                        <div class=" mb-4">
                            <label for="input-password">@lang('translation.confirm-password')</label>
                            <input type="password"
                                   class="form-control @error('password_confirmation') is-invalid @enderror"
                                   name="password_confirmation" id="input-password"
                                   placeholder="@lang('translation.enter-confirm-password')"
                                   required>
                            @error('password_confirmation')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="form-floating-icon">
                                <i data-feather="lock"></i>
                            </div>
                        </div>
                        <div class=" mb-4">
                            <input type="file" class="form-control @error('avatar') is-invalid @enderror"
                                   name="avatar" id="edit-input-avatar" required>
                            @error('avatar')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="">
                                <i data-feather="file"></i>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="w-auto btn btn-block btn-light" data-bs-dismiss="modal">
                            @lang('translation.cancel')
                        </button>
                        <button type="submit" class="btn btn-block btn-success w-auto">
                            @lang('translation.save')
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Varying modal content -->
    <div class="modal fade" id="userUpdateModal" tabindex="-1" aria-labelledby="userUpdateModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userUpdateModalLabel"> {{ __('translation.update') }} </h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form name="user-update-modal" action="{{ route('user.update') }}" method="post"
                      enctype="multipart/form-data" needs-validation>
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="hidden" name="user_id" id="edit-user-id">
                        <div class="mb-3">
                            <label for="cleave-phone1" class="form-label"> {{ __('translation.phone') }} <span
                                    class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-text" id="basic-phone">
                                <i class="ri-file-user-line"></i>
                                </span>
                                <input type="text"
                                       class=" form-control @error('phone') is-invalid @enderror"
                                       aria-label="Username" aria-describedby="basic-phone" name="phone"
                                       id="cleave-phone1" value="" placeholder="@lang('translation.enter-phone')" required>
                            </div>
                            @error('phone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="invalid-feedback">
                                Please enter phone
                            </div>
                        </div>
                        <div class="mb-3">
                            <label for="edit-user-name" class="form-label">
                                {{ __('translation.username') }}
                                <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-text" id="basic-name">
                                    <i class="ri-file-user-line"></i>
                                </span>
                                <input type="text" class="form-control @error('name') is-invalid @enderror"
                                       name="name" id="edit-user-name" aria-label="Username"
                                       aria-describedby="basic-name" placeholder="@lang('translation.enter-username')"
                                       required>
                            </div>
                            @error('name')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                            <div class="invalid-feedback">
                                Please enter username
                            </div>
                        </div>

                        <div class="mb-2">
                            <label for="edit-user-password" class="form-label">
                                {{ __('translation.password') }}
                                <span class="text-danger">*</span></label>
                            <div class="input-group">
                                <span class="input-group-text" id="basic-password1">
                                <i class="ri-file-user-line"></i>
                                </span>
                                <input type="password" class="form-control @error('password') is-invalid @enderror"
                                       aria-label="Username" aria-describedby="basic-password" name="password"
                                       id="edit-user-password" placeholder="@lang('translation.enter-password')">
                            </div>
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            <div class="invalid-feedback">
                                Please enter password
                            </div>
                        </div>
                        <div class=" mb-4">
                            <label for="edit-input-password">
                                {{ __('translation.confirm-password') }}
                            </label>
                            <div class="input-group">
<span class="input-group-text" id="basic-confirmation">
<i class="ri-file-user-line"></i>
</span>
                                <input type="password"
                                       class="form-control @error('password_confirmation') is-invalid @enderror"
                                       name="password_confirmation" id="edit-input-password2" aria-label="Username"
                                       aria-describedby="basic-confirmation"
                                       placeholder="@lang('translation.enter-confirm-password')">
                            </div>

                            <div class="form-floating-icon">
                                <i data-feather="lock"></i>
                            </div>
                        </div>
                        <div class=" mb-4">
                            <input type="file" class="form-control @error('avatar') is-invalid @enderror"
                                   name="avatar" id="input-avatar">
                            @error('avatar')
                            <span class="invalid-feedback" role="alert">
<strong>{{ $message }}</strong>
</span>
                            @enderror
                            <div class="">
                                <i data-feather="file"></i>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light"
                                data-bs-dismiss="modal">{{ __('translation.back') }}</button>
                        <button type="submit" class="btn btn-primary">{{ __('translation.save') }}</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    {{-- <script src="{{ URL::asset('assets/libs/prismjs/prismjs.min.js') }}"></script> --}}
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('assets/js/pages/form-validation.init.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/form-masks.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script>
        if (document.querySelector("#cleave-phone1")) {

            new Cleave('#cleave-phone1', {
                prefix: '+998',
                delimiters: ['-', '-', '-', '-'],
                blocks: [4, 2, 3, 2, 2]
            });
        }
    </script>
    <script>
        var userUpdateModal = document.getElementById('userUpdateModal')
        userUpdateModal.addEventListener('show.bs.modal', function (event) {
// Button that triggered the modal
            var button = event.relatedTarget;
// Extract info from data-bs-* attributes
            var userNameAttr = button.getAttribute('data-bs-user-name');
            var userIdAttr = button.getAttribute('data-bs-user-id');
            var userPhoneAttr = button.getAttribute('data-bs-user-phone');
            console.log(userPhoneAttr);
// If necessary, you could initiate an AJAX request here
// and then do the updating in a callback.
//
// Update the modal's content.
            var modalTitle = userUpdateModal.querySelector('.modal-title');
            var modalBodyUserName = userUpdateModal.querySelector('.modal-body input#edit-user-name');
            var modalBodyUserId = userUpdateModal.querySelector('.modal-body input#edit-user-id');
            var modalBodyUserPhone = userUpdateModal.querySelector('.modal-body input#cleave-phone1');

            modalTitle.textContent += userNameAttr;
            modalBodyUserName.value = userNameAttr;
            modalBodyUserId.value = userIdAttr;
            modalBodyUserPhone.value = userPhoneAttr;
        })
    </script>
@endsection
