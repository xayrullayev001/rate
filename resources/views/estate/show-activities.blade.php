@extends('layouts.master')
@section('title')
    @lang('translation.overview')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.estate-order-breadcrumbs', ['order' => $order, 'active' => 'activities'])
    @endcomponent
    <!-- end row -->
    <div class="row">
        <div class="col-lg-12">
            <div class="tab-content text-muted">
                <div class="tab-pane fade show active" id="project-activities" role="tabpanel">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"> @lang('translation.activities') </h5>
                            <div class="acitivity-timeline py-3">
                                @foreach ($actions as $action)
                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::REJECTED->name)
                                        @if ($params = json_decode("{$action->params}", true))
                                            <div class="acitivity-item py-3 d-flex">
                                                <div class="flex-shrink-0 avatar-xs activity-avatar">
                                                    <div
                                                        class="avatar-title bg-soft-success text-success rounded-circle">
                                                        <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                                                             alt=""
                                                             class="avatar-xs rounded-circle activity-avatar"/>
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6 class="mb-1">
                                                        {{ $params['user']['name'] }}
                                                        <span
                                                            class="badge bg-soft-secondary text-secondary align-middle">
                                                        @lang(App\Enums\TrackingActionTypeEnum::REJECTED->value)
                                                    </span>
                                                    </h6>
                                                    <p class="text-muted mb-2">
                                                        <i class="ri-file-text-line align-middle ms-2"></i>
                                                        {{ __("{$action->msg}") }}
                                                    </p>
                                                    <div class="avatar-group mb-2">
                                                        <p> {{ $params['attachments'][0]. ' '.$params['attachments'][1] }} </p>
                                                    </div>
                                                    <small class="mb-0 text-muted">{{ $action->created_at }}</small>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                <!-- end item -->
                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::ORDER_CREATED->name)
                                        <div class="acitivity-item d-flex">
                                            @if ($params = json_decode("{$action->params}", true))
                                                <div class="flex-shrink-0">
                                                    <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                                                         alt="" class="avatar-xs rounded-circle activity-avatar"/>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6 class="mb-1">
                                                        {{ $params['user']['name'] }}
                                                        <span class="badge bg-soft-primary text-primary align-middle">
                                                            @lang('translation.'.$action->type)
                                                        </span>
                                                    </h6>
                                                    <p class="text-muted mb-2">
                                                        {{ __($action->msg, $params['attachments']) }}
                                                    </p>
                                                    <small class="mb-0 text-muted">
                                                        {{ $action->created_at }}
                                                    </small>
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                <!-- end item -->
                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::MEMBERS_ADDED->name)
                                        <div class="acitivity-item py-3 d-flex">
                                            <div class="flex-shrink-0 avatar-xs activity-avatar">
                                                <div class="avatar-title bg-soft-success text-success rounded-circle">
                                                    N
                                                </div>
                                            </div>
                                            <div class="flex-grow-1 ms-3">
                                                <h6 class="mb-1">
                                                    Nancy Martino
                                                    <span class="badge bg-soft-secondary text-secondary align-middle">
                                                        @lang('translation.'.App\Enums\TrackingActionTypeEnum::MEMBERS_ADDED->name)
                                                    </span>
                                                </h6>
                                                <p class="text-muted mb-2">
                                                    <i class="ri-file-text-line align-middle ms-2"></i>
                                                    {{ __("{$action->msg}") }}
                                                </p>
                                                <div class="avatar-group mb-2">
                                                    @if ($params = json_decode("{$action->params}", true))
                                                        @foreach ($params['attachments'] as $key => $value)
                                                            <a href="{{ route('user.show', [$value[0]]) }}"
                                                               class="avatar-group-item" data-bs-toggle="tooltip"
                                                               data-bs-placement="top" title="{{ $value[1] }}"
                                                               data-bs-original-title="{{ $value[1] }}">
                                                                <img src="{{ URL::asset("{$value[2]}") }}" alt=""
                                                                     class="rounded-circle avatar-xs"/>
                                                            </a>
                                                        @endforeach
                                                    @endif
                                                </div>
                                                <small class="mb-0 text-muted">{{ $action->created_at }}</small>
                                            </div>
                                        </div>
                                    @endif
                                <!-- end item -->

                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::ATTACHMENTS->name)
                                        <div class="acitivity-item py-3 d-flex">
                                            @if ($params = json_decode("{$action->params}", true))
                                                <div class="flex-shrink-0">
                                                    <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                                                         alt="" class="avatar-xs rounded-circle activity-avatar"/>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6 class="mb-1">
                                                        {{ $params['user']['name'] }}
                                                        <span class="badge bg-soft-success text-success align-middle">
                                                            @lang('translation.'.\App\Enums\TrackingActionTypeEnum::ATTACHMENTS->name)
                                                        </span>
                                                    </h6>
                                                    <p class="text-muted mb-2">
                                                        @lang(\App\Enums\TrackingActionTypeEnum::ATTACHMENTS->value)
                                                    </p>
                                                    <div class="row">
                                                        <div class="col-xxl-4">
                                                            <div class="row border border-dashed gx-2 p-2 mb-2">
                                                                @foreach ($params['attachments'] as $attachedItem)
                                                                    <div class="col-4">
                                                                        <img src="{{ URL::asset("{$attachedItem}") }}"
                                                                             alt="" class="img-fluid rounded"/>
                                                                    </div>
                                                            @endforeach
                                                            <!--end col-->
                                                            </div>
                                                            <!--end row-->
                                                        </div>
                                                    </div>
                                                    <small class="mb-0 text-muted">
                                                        {{ $action->created_at }}
                                                    </small>
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                <!-- end item -->

                                    <!-- START: ORDER_UPDATED -->
                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::ORDER_UPDATED->name)
                                        @if ($params = json_decode("{$action->params}", true))
                                            <div class="acitivity-item py-3 d-flex">
                                                <div class="flex-shrink-0 avatar-xs activity-avatar">
                                                    <div
                                                        class="avatar-title bg-soft-success text-success rounded-circle">
                                                        <img src="{{ URL::asset("{$params['user']['avatar']}") }}"
                                                             alt=""
                                                             class="avatar-xs rounded-circle activity-avatar"/>
                                                    </div>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6 class="mb-1">
                                                        {{ $params['user']['name'] }}
                                                        <span
                                                            class="badge bg-soft-secondary text-secondary align-middle">
                                                            @lang('translation.'.App\Enums\TrackingActionTypeEnum::ORDER_UPDATED->name)
                                                        </span>
                                                    </h6>
                                                    <p class="text-muted mb-2">
                                                        <i class="ri-file-text-line align-middle ms-2"></i>
                                                        @lang("{$action->msg}", [$params['user']['name']])
                                                    </p>
                                                    <div class="avatar-group mb-2">
                                                        <ul>
                                                            @foreach ($params['attachments'] as $key => $value)
                                                                <li> {{ $value }}</li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                    <small class="mb-0 text-muted">{{ $action->created_at }}</small>
                                                </div>
                                            </div>
                                        @endif
                                    @endif
                                <!-- END: ORDER_UPDATED -->

                                    @if ($action->type == App\Enums\TrackingActionTypeEnum::COMPLETED->name)
                                        <div class="acitivity-item py-3 d-flex">
                                            @if ($params = json_decode("{$action->params}", true))
                                                <div class="flex-shrink-0">
                                                    <img src="{{ asset("{$params['user']['avatar']}") }}"
                                                         alt="" class="avatar-xs rounded-circle activity-avatar"/>
                                                </div>
                                                <div class="flex-grow-1 ms-3">
                                                    <h6 class="mb-1">
                                                        {{ $params['user']['name'] }}
                                                        <span class="badge bg-soft-success text-success align-middle">
                                                           @lang('translation.'.\App\Enums\TrackingActionTypeEnum::COMPLETED->name)
                                                        </span>
                                                    </h6>
                                                    <p class="text-muted mb-2">
                                                        {{ __(\App\Enums\TrackingActionTypeEnum::COMPLETED->value, [$params['attachments'][0], $params['attachments'][1]]) }}
                                                    </p>
                                                    <div class="col-xxl-4 col-lg-6">
                                                        <div class="border rounded border-dashed p-2">
                                                            <div class="d-flex align-items-center">
                                                                <div class="flex-shrink-0 me-3">
                                                                    <div class="avatar-sm">
                                                                        <div
                                                                            class="avatar-title bg-light text-secondary rounded fs-24">
                                                                            <i class="ri-folder-zip-line"></i>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-grow-1 overflow-hidden">
                                                                    <h5 class="fs-13 mb-1">
                                                                        <span class="text-body text-truncate d-block">
                                                                            {{ $params['attachments'][3] }}
                                                                        </span>
                                                                    </h5>
                                                                    <div>
                                                                        <span class="text-body text-truncate d-block">
                                                                            {{ $params['attachments'][5] }}
                                                                        </span>
                                                                    </div>
                                                                </div>
                                                                <div class="flex-shrink-0 ms-2">
                                                                    <div class="d-flex gap-1">
                                                                        <a href="{{ route('download', [$params['attachments'][2]]) }}"
                                                                           class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                                                            <i class="ri-download-2-line"></i>
                                                                        </a>
                                                                        <a data-bs-file="{{ Storage::url($params['attachments'][4]) }}"
                                                                           data-bs-file-name="{{ $params['attachments'][3] }}"
                                                                           data-bs-toggle="modal"
                                                                           data-bs-target="#conclusionViewModal"
                                                                           class="text-body text-truncate d-block btn btn-icon text-muted btn-sm fs-18">
                                                                            <i class="ri-fullscreen-line"></i>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <small class="mb-0 text-muted">
                                                        {{ $action->created_at }}
                                                    </small>
                                                </div>
                                            @endif
                                        </div>
                                    @endif
                                <!-- end item -->
                                @endforeach
                            </div>
                        </div>
                        <!--end card-body-->
                    </div>
                    <!--end card-->
                </div>
                <!-- end tab pane -->
            </div>
        </div>
        <!-- end col -->
    </div>
    <!-- end row -->


    <!-- Scrollable Modal -->
    <div class="modal-dialog modal-dialog-scrollable ">
        <div id="conclusionViewModal" class="modal fade" tabindex="-1" aria-labelledby="conclusionViewModalLabel"
             aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-fullscreen">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="conclusionViewModalLabel">Modal Heading</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <object data="" type="application/pdf" style="height: 100vh;border: none;"
                                standby="loading" width="100%"></object>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
    </div>

@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/activities.js') }}"></script>

@endsection
