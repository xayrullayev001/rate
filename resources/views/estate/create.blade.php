@extends('layouts.master')
@section('title')
    @lang('translation.create-order')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.estate')
        @endslot
        @slot('title')
            @lang('translation.create-estate-order')

        @endslot
    @endcomponent
    <form action="{{ route('estate.store') }}" method="post" autocomplete="off" class="needs-validation" validate>
        @csrf
        <input type="hidden" name="status" value="{{ App\Enums\OrderStatusEnum::DRAFT->name }}">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-information')
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-number-input">
                                    @lang('translation.number')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('number') is-invalid @enderror"
                                       name="number" placeholder="@lang('translation.write-number')"
                                       id="order-number-input" value="{{ old('number') }}"
                                       tabindex="1">
                                @error('number')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.number')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label"
                                       for="auto-created_date-input">@lang('translation.created-date')</label>
                                <input type="text"
                                       class="form-control text-uppercase flatpickr-input  @error('created_at') is-invalid @enderror"
                                       id="auto-created_date-input"
                                       name="created_at" value="{{ old('created_at') }}"
                                       data-provider="flatpickr" data-date-format="d.m.Y"
                                       tabindex="2">
                                @error('created_at')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.created-date')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label for="choices-purpose-input" class="form-label">
                                    @lang('translation.select-purpose')
                                </label>
                                <select data-choices data-choices-search-false data-choices-multiple-groups="false"
                                        name="purpose_id" class="form-select @error('purpose_id') is-invalid @enderror"
                                        id="choices-purpose-input" tabindex="3">
                                    @foreach($purposeCases as $purposeItem)
                                        <option value="{{ $purposeItem->id }}"
                                                @if(old('purpose_id') == $purposeItem->id)selected @endif>
                                            {{ $purposeItem->{str_replace('_', '-', app()->getLocale())} }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('purpose_id')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.purpose')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>

                <!-- order customer card -->
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-customer')
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <!-- Inline Checkbox -->
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input " type="radio"
                                       @if(old('customer_type')=='physical') checked @endif
                                       name="customer_type"
                                       onclick="handleCustomerClick(this.value);" value="physical"
                                       id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    @lang('translation.physical')
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input is " type="radio"
                                       @if(old('customer_type')=='juridical') checked
                                       @endif  name="customer_type"
                                       onclick="handleCustomerClick(this.value);" value="juridical"
                                       id="flexRadioDefault1">
                                <label class="form-check-label" for="flexRadioDefault1">
                                    @lang('translation.juridical')
                                </label>
                            </div>
                        </div>
                        <div class="row gy-4 @if(old('customer_type')=='juridical') d-none @endif" id="customer-block">

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_last_name-input">
                                    @lang('translation.customer-last-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_last_name') is-invalid @enderror"
                                       name="customer_last_name" id="order-customer_last_name-input"
                                       value="{{ old('customer_last_name') }}"
                                       placeholder="@lang('translation.write-customer-last-name')" tabindex="4">
                                @error('customer_last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-last-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_first_name-input">
                                    @lang('translation.customer-first-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_first_name') is-invalid @enderror"
                                       name="customer_first_name" id="order-customer_first_name-input"
                                       value="{{ old('customer_first_name') }}"
                                       placeholder="@lang('translation.write-customer-first-name')" tabindex="5">
                                @error('customer_first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-first-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-customer_patronymic-input">
                                    @lang('translation.customer-patronymic')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('customer_patronymic') is-invalid @enderror"
                                       name="customer_patronymic" id="order-customer_patronymic-input"
                                       value="{{ old('customer_patronymic') }}"
                                       placeholder="@lang('translation.write-customer-patronymic')" tabindex="6">
                                @error('customer_patronymic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-patronymic')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4 @if(old('customer_type')=='physical') d-none @endif"
                             id="customer-company-block">
                            <div class="mb-3">
                                <label class="form-label" for="order-customer_company-input">
                                    @lang('translation.customer-juridical-name')
                                </label>
                                <input type="text" class="form-control text-uppercase" name="customer_company"
                                       id="order-customer_company-input" value="{{ old('customer_company') }}"
                                       placeholder="@lang('translation.write-customer-juridical-name')" tabindex="3">
                                @error('customer_company')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.customer-company')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->

                <!-- order owner card -->
                <div class="card">
                    <div class="card-header">
                        @lang('translation.order-owner')
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <!-- Inline Checkbox -->
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input" type="radio" @if(old('owner_type')=='physical') checked
                                       @endif  name="owner_type"
                                       onclick="handleOwnerClick(this.value);" value="physical"
                                       id="flexRadioDefault3">
                                <label class="form-check-label" for="flexRadioDefault3">
                                    @lang('translation.physical')
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input is " type="radio"
                                       @if(old('owner_type')=='juridical') checked
                                       @endif  name="owner_type"
                                       onclick="handleOwnerClick(this.value);" value="juridical"
                                       id="flexRadioDefault4">
                                <label class="form-check-label" for="flexRadioDefault4">
                                    @lang('translation.juridical')
                                </label>
                            </div>
                        </div>
                        <div class="row gy-4  @if(old('owner_type')=='juridical')d-none @endif" id="owner-block">

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-owner_last_name-input">
                                    @lang('translation.owner-last-name')
                                </label>
                                <input type="text" value="{{ old('owner_last_name') }}"
                                       class="form-control text-uppercase @error('owner_last_name') is-invalid @enderror"
                                       name="owner_last_name" id="order-owner_last_name-input"
                                       placeholder="@lang('translation.write-owner-last-name')" tabindex="7">
                                @error('owner_last_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-last-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6" id="owner-block">
                                <label class="form-label" for="order-owner_first_name-input">
                                    @lang('translation.owner-first-name')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('owner_first_name') is-invalid @enderror"
                                       name="owner_first_name" value="{{ old('owner_first_name') }}"
                                       id="order-owner_first_name-input"
                                       placeholder="@lang('translation.write-owner-first-name')" tabindex="8">
                                @error('owner_first_name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-first-name')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="order-owner_patronymic-input">
                                    @lang('translation.owner-patronymic')
                                </label>
                                <input type="text" value="{{ old('owner_patronymic') }}"
                                       class="form-control text-uppercase @error('owner_patronymic') is-invalid @enderror"
                                       name="owner_patronymic" id="order-owner_patronymic-input"
                                       placeholder="@lang('translation.write-owner-patronymic')" tabindex="9">
                                @error('owner_patronymic')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-patronymic')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="row gy-4 @if(old('owner_type')=='physical')d-none @endif"
                             id="owner-company-block">
                            <div class="mb-3">
                                <label class="form-label" for="order-owner_company-input">
                                    @lang('translation.owner-company')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('owner_company') is-invalid @enderror"
                                       name="owner_company" value="{{ old('owner_company') }}"
                                       id="order-owner_company-input"
                                       placeholder="@lang('translation.write-owner-juridical-name')" tabindex="">
                                @error('owner_company')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.owner-company')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        @lang('translation.estate-information')
                    </div>
                    <div class="card-body">
                        <div class="row gy-4">
                            {{--                            contract_date--}}
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="auto-contract_date-input">
                                    @lang('translation.contract-date')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('contract_date') is-invalid @enderror"
                                       id="auto-contract_date-input" name="contract_date"
                                       data-provider="flatpickr" data-date-format="d.m.Y"
                                       value="{{ old('contract_date') }}"
                                       placeholder="@lang('translation.write-estate-contract-date')"
                                       tabindex="13">
                                @error('contract_date')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.contract-date')])</strong>
                                </span>
                                @enderror
                            </div>
                            {{--                            name_of_object--}}
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-name-of-object-input">
                                    @lang('translation.name-of-object')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('name_of_object') is-invalid @enderror"
                                       id="estate-name-of-object-input" name="name_of_object"
                                       value="{{ old('name_of_object') }}"
                                       placeholder="@lang('translation.write-estate-name-of-object')" tabindex="14">
                                @error('name_of_object')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.name-of-object')])</strong>
                                </span>
                                @enderror
                            </div>
                            {{--                            region--}}

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label for="choices-purpose-input" class="form-label">
                                    @lang('translation.select-estate-region')
                                </label>
                                <select data-choices data-choices-search-true data-choices-multiple-groups="false"
                                        name="region" class="form-select @error('region') is-invalid @enderror"
                                        id="choices-region-input" tabindex="3">
                                    @foreach($regions as $region)
                                        <option value="{{ $region->id }}"
                                                @if(old('region') == $region->id)selected @endif>
                                            {{ $region->{str_replace('_', '-', app()->getLocale())} }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('region')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.region')])</strong>
                                </span>
                                @enderror
                            </div>

                            {{--  district--}}
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label for="choices-purpose-input" class="form-label">
                                    @lang('translation.select-estate-district')
                                </label>
                                <select data-choices data-choices-search-true data-choices-multiple-groups="false"
                                        name="district" class="form-select @error('district') is-invalid @enderror"
                                        id="choices-district-input" tabindex="3">
                                    @foreach($districts as $district)
                                        <option value="{{ $district->id }}"
                                                @if(old('district') == $district->id)selected @endif>
                                            {{ $district->{str_replace('_', '-', app()->getLocale())} }}
                                        </option>
                                    @endforeach
                                </select>
                                @error('district')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.district')])</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-address-input">
                                    @lang('translation.estate-address')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('address') is-invalid @enderror"
                                       id="estate-address-input" name="address"
                                       value="{{ old('address') }}"
                                       placeholder="@lang('translation.select-estate-address')" tabindex="11">
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.estate-address')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-home-input">
                                    @lang('translation.home')
                                </label>
                                <input type="text"
                                       class="form-control text-uppercase @error('home') is-invalid @enderror"
                                       id="estate-home-input" name="home" value="{{ old('home') }}"
                                       placeholder="@lang('translation.write-home')" tabindex="12">
                                @error('home')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.home')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="row gy-4">
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-area-input">
                                    @lang('translation.area')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('area') is-invalid @enderror"
                                       id="estate-area-input" name="area" value="{{ old('area') }}"
                                       placeholder="@lang('translation.write-estate-area')" tabindex="16">
                                @error('area')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.area')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-overall-area-input">
                                    @lang('translation.overall-area')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('overall_area') is-invalid @enderror"
                                       id="estate-overall-area-input" name="overall_area" value="{{ old('overall_area') }}"
                                       placeholder="@lang('translation.write-overall-area')" tabindex="17">
                                @error('overall_area')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.overall-area')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-usefull-area-input">
                                    @lang('translation.usefull-area')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('usefull_area') is-invalid @enderror"
                                       id="estate-usefull-area-input" name="usefull_area" value="{{ old('usefull_area') }}"
                                       placeholder="@lang('translation.write-estate-usefull-area')" tabindex="18">
                                @error('usefull_area')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.usefull-area')])</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="mb-3 col-xxl-4 col-md-6">
                                <label class="form-label" for="estate-live-area-input">
                                    @lang('translation.live-area')
                                </label>
                                <input type="text" class="form-control text-uppercase @error('live_area') is-invalid @enderror"
                                       id="estate-live-area-input" name="live_area" value="{{ old('live_area') }}"
                                       placeholder="@lang('translation.write-estate-live-area')" tabindex="18">
                                @error('live_area')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.live-area')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                </div>
                <!-- end card -->
                <div class="col-lg-4 col-sm-4 col-md-4">
                    <div class="text-start mb-3 btn-block">
                        <button type="submit" class="btn btn-success w-sm" name="save"
                                tabindex="">@lang('translation.save')</button>
                    </div>
                </div>
            </div>
            <!-- end col -->

            <!-- end card -->
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">@lang('translation.diller')</h5>
                    </div>
                    <div class="card-body">
                        <label for="choices-diller-input" class="form-label">
                            @lang('translation.select-diller')
                        </label>
                        <select data-choices data-choices-search-false data-choices-multiple-groups="false"
                                name="diller_id" class="form-select @error('diller_id') is-invalid @enderror"
                                id="choices-diller-input"
                                tabindex="23">
                            <option value="" selected> @lang('translation.select-diller')</option>
                            @foreach($dillers as $diller)
                                <option value="{{ $diller->id}}" @if(old('diller_id')== $diller->id) selected @endif>
                                    {{ $diller->name }}
                                </option>
                            @endforeach
                        </select>
                        @error('diller_id')
                        <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.diller')])</strong>
                                </span>
                        @enderror
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.ordered-customer')
                        </h5>
                    </div>
                    <div class="card-body">
                        <label for="auto-ordered_customer-input" class="form-label">
                            @lang('translation.ordered-customer-name')
                        </label>
                        <input type="text" class="form-control text-uppercase @error('ordered_customer') is-invalid @enderror"
                               id="auto-ordered_customer-input"
                               name="ordered_customer" value="{{ old('ordered_customer') }}"
                               placeholder="@lang('translation.write-car-ordered-customer')" tabindex="24">
                        @error('ordered_customer')
                        <span class="invalid-feedback" role="alert">
                            <strong>@lang($message,['attribute'=>trans('translation.ordered-customer')])</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="card-body">
                        <label for="cleave-phone" class="form-label">
                            @lang('translation.ordered-customer-phone')
                        </label>
                        <input type="text" class="form-control text-uppercase @error('ordered_customer_phone') is-invalid @enderror"
                               name="ordered_customer_phone" id="cleave-phone"
                               value="{{ old('ordered_customer_phone') }}"
                               placeholder="@lang('translation.write-car-ordered-customer-phone')" tabindex="25">
                        @error('ordered_customer_phone')
                        <span class="invalid-feedback" role="alert">
                                <strong>@lang($message,['attribute'=>trans('translation.ordered-customer-phone')])</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.cost')
                        </h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <label class="form-label" for="product-cost-addon">
                                @lang('translation.order-cost-note')
                            </label>
                            <div class="input-group mb-3">
                                <span class="input-group-text" id="product-cost-addon">uzs</span>
                                <input type="text" class="form-control @error('cost') is-invalid @enderror" name="cost"
                                       id="cleave-numeral" value="{{ old('cost') }}"
                                       placeholder="123,450" aria-label="Price"
                                       aria-describedby="product-price-addon"
                                       tabindex="26">
                                @error('cost')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.cost')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->

                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">@lang('translation.note')</h5>
                    </div>
                    <div class="card-body">
                        <label for="estate-note-input" class="text-muted mb-2">
                            @lang('translation.note-value')
                        </label>
                        <textarea class="form-control text-uppercase @error('note') is-invalid @enderror" name="note"
                                  id="estate-note-input" value="{{ old('note') }}"
                                  placeholder="@lang('translation.write-cost-note')"
                                  rows="3" tabindex="27"></textarea>
                        @error('note')
                        <span class="invalid-feedback" role="alert">
                                <strong>@lang($message,['attribute'=>trans('translation.note')])</strong>
                            </span>
                        @enderror
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
        </div>
        <!-- end row -->
    </form>
@endsection
@section('script')
    <script src="{{ URL::asset('assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/orders/show.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/pages/orders/create.js') }}"></script>
@endsection
