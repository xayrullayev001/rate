@extends('layouts.master')
@section('title')
    @lang('translation.appraisers-bonus')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.appraisers-bonus')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <form action="{{ route('bonus.index') }}" method="get" autocomplete="off" novalidate>
                    <div class="card-header  border-0">
                        <div class="d-flex align-items-center">
                            <h5 class="card-title mb-0 flex-grow-1">@lang('translation.order-list')</h5>
                            <div class="flex-shrink-0">
                                @component('components.page-size',['size'=>$size])@endcomponent
                            </div>
                        </div>
                    </div>
                </form>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-borderless align-middle table-nowrap mb-0">
                            <thead>
                            <tr>
                                <th scope="col">№</th>
                                <th scope="col">@lang('translation.appraisers')</th>
                                <th scope="col">@lang('translation.order-number')</th>
                                <th scope="col">@lang('translation.amount')</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($bonuses as $bonus)
                                <tr>
                                    <td class="fw-medium">{{($bonuses->currentPage() - 1) * $bonuses->perPage() + $loop->iteration}}</td>
                                    <td>
                                        <a class="text-decoration-underline"
                                           href="{{ route('user.show', [$bonus->appraiser_id]) }}">
                                            {{  $bonus->appraiser->name }}
                                        </a>
                                    </td>

                                    <td>
                                        <a class="text-decoration-underline"
                                           href="@if(\App\Enums\OrderTypeEnum::AUTO->name==$bonus->order_type) {{route('auto.show', [$bonus->order_id])}} @else {{route('estate.show', [$bonus->order_id])}} @endif">
                                            @if(isset($bonus->order)){{ $bonus->order->number }}@endif
                                        </a>
                                    </td>
                                    <td>
                                        @component('components.price', ['balance' => $bonus->amount])
                                        @endcomponent
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $bonuses->withQueryString()->links() }}</div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>
@endsection

@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
@endsection
