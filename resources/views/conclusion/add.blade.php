@extends('layouts.master')
@section('title')
    @lang('translation.conclusion')
@endsection
@section('css')
    <link href="{{ URL::asset('assets/libs/dropzone/dropzone.min.css') }}" rel="stylesheet">
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.conclusion')
        @endslot
        @slot('title')
            @lang('translation.conclusion-upload')
        @endslot
    @endcomponent

    <form action="{{ route('conclusion.store') }}" method="post" id="create-conclusion-form" autocomplete="off"
          class="needs-validation" enctype="multipart/form-data" validate>
        <input type="hidden" name="order_id" value="{{ $order->id }}">
        @csrf
        <input type="hidden" value="{{ $type }}" name="order_type">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.give-you-lend')
                        </h5>
                    </div>
                    <div class="card-body">
                        <div class="mb-3">
                            <label for="choices-publish-status-input" class="form-label">
                                @lang('translation.lend-note')
                                <span class="badge badge-outline-secondary"> @component('components.price',['balance'=>$order->cost]) @endcomponent</span>
                                @lang('translation.lend-cost-note') <p
                                    class='text-muted mb-2'>@lang('translation.price-currency-note')</p>
                            </label> <br>
                            <!-- Inline Checkbox -->
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input is " type="radio" name="is_diller"
                                       onclick="handleClick(this.value);" value="no" checked id="flexRadioDefault1">
                                <label class="form-check-label" for="flexRadioDefault1">
                                    @lang('translation.no')
                                </label>
                            </div>
                            <div class="form-check form-check-inline mb-2">
                                <input class="form-check-input" type="radio" name="is_diller"
                                       onclick="handleClick(this.value);" value="yes" id="flexRadioDefault2">
                                <label class="form-check-label" for="flexRadioDefault2">
                                    @lang('translation.yes')
                                </label>
                            </div>
                        </div>
                        <div>
                            <label class="form-label" for="cleave-price">
                                @lang('translation.object-price-note')
                                <p class="text-muted mb-2">
                                    @lang('translation.object-price-currency-note')
                                </p>
                            </label>
                            <div class="input-group has-validation mb-3">
                                <input type="text" class="form-control  @error('object_price') is-invalid @enderror"
                                       name="object_price" id="cleave-price"
                                       aria-label="Order object-price" aria-describedby="order-object-price">
                                <span class="input-group-text" id="order-object-price-addon">so'm</span>
                                @error('object_price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.object-price')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.conclusion-file')
                        </h5>
                    </div>
                    <div class="card-body">
                        <p class="text-muted mb-2"> @lang('translation.conclusion-file-pdf') </p>
                        <input type="file" class="form-control @error('file') is-invalid @enderror" name="file">
                        @error('file')
                        <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.file')])</strong>
                                </span>
                        @enderror

                        <br/>
                        <p class="text-muted mb-2">
                            @lang('translation.note')
                        </p>
                        <textarea class="form-control @error('note') is-invalid @enderror" name="note"
                                  placeholder="@lang('translation.note-value')"
                                  rows="3"></textarea>
                        @error('note')
                        <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.note')])</strong>
                                </span>
                        @enderror
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <!-- end col -->
            <div class="col-lg-4">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.price-information')
                        </h5>
                    </div>
                    <!-- end card body -->
                    <div class="card-body">
                        <label class="form-label">
                            @lang('translation.price-for-pay') :
                        </label>
                        <span class="badge badge-outline-secondary">@component('components.price',['balance'=>$order->cost]) @endcomponent</span>
                        <br/>
                        <label class="form-label">
                            @lang('translation.owner') :
                        </label>
                        <span class="badge badge-outline-secondary"> {{ $order->owner }}</span> <br/>
                        <label class="form-label">
                            @lang('translation.customer') :
                        </label>
                        <span class="badge badge-outline-secondary"> {{ $order->customer }}</span>
                    </div>
                </div>
                <!-- end card -->


                <div class="card" id="set_price">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.price')
                        </h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <label class="form-label" for="cleave-price">
                                @lang('translation.price-for-app')
                                <p class="text-muted mb-2">
                                    @lang('translation.object-price-currency-note')
                                </p>
                            </label>
                            <div class="input-group has-validation mb-3">
                                <input type="text" class="form-control @error('price') is-invalid @enderror"
                                       name="price" id="cleave-price2"
                                       aria-label="Order price" aria-describedby="order-price">
                                <span class="input-group-text" id="order-price-addon">so'm</span>
                                @error('price')
                                <span class="invalid-feedback" role="alert">
                                    <strong>@lang($message,['attribute'=>trans('translation.price')])</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->

                <div class="card d-none" id="lend_card">
                    <div class="card-header">
                        <h5 class="card-title mb-0">
                            @lang('translation.give-you-lend')
                        </h5>
                    </div>
                    <div class="card-body">
                        <div>
                            <label class="form-label" for="order-price-input">
                                @lang('translation.price-lend-cost')
                                <span class="badge badge-outline-secondary">
                                        @component('components.price',['balance'=>$order->cost]) @endcomponent
                                    </span>
                                @lang('translation.lender')
                                <p class="text-muted mb-2">
                                    {{$order->diller}}
                                </p>
                            </label>
                        </div>
                        <div>
                            <label class="form-label" for="conclusion-lend-due-date">
                                @lang('translation.select-lend-due-date')
                            </label>
                            <input type="text" name="expired" id="conclusion-lend-due-date"
                                   class="form-control flatpickr-input @error('expired') is-invalid @enderror"
                                   data-provider="flatpickr" data-date-format="d.m.Y"
                                   {{--                                   data-minDate="{{ Carbon\Carbon::now()->format('d.m.Y') }}"--}}
                                   data-maxDate="{{ Carbon\Carbon::now('Asia/Tashkent')->add(5, 'day')->format('d.m.Y') }}"
                                   data-default-date="{{ Carbon\Carbon::now('Asia/Tashkent')->add(3, 'day')->format('d.m.Y') }}">
                            @error('expired')
                            <span class="invalid-feedback" role="alert">
                                <strong>@lang($message,['attribute'=>trans('translation.due-date')])</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <!-- end card body -->
                </div>
                <!-- end card -->
            </div>
            <div class="col-lg-4 col-sm-4 col-md-4">
                <div class="text-start mb-3 btn-block">
                    <button type="submit" class="btn btn-success mh-0 w-sm">
                        @lang('translation.save')
                    </button>
                </div>
            </div>
        </div>
        <!-- end row -->
    </form>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/libs/cleave.js/cleave.js.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script src="{{ URL::asset('/assets/js/conclusion-add.js') }}"></script>
@endsection
