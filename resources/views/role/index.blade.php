@extends('layouts.master')
@section('title')
    @lang('translation.role')
@endsection
@section('content')
    @component('components.breadcrumb')
        @slot('li_1')
            @lang('translation.pages')
        @endslot
        @slot('title')
            @lang('translation.role')
        @endslot
    @endcomponent
    <div class="row">
        <div class="col-xl-12">
            <div class="card">
                <div class="card-header  border-1">
                    <div class="d-flex align-items-center">
                        <h5 class="card-title mb-0 flex-grow-1">@lang('translation.users')</h5>
                        <div class="flex-shrink-0 ">
                            <form action="{{ route('role.index') }}" method="get" autocomplete="off" novalidate>
                                @component('components.page-size',['size'=>$size]) @endcomponent
                            </form>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <ul class="nav nav-tabs nav-tabs-custom nav-success mb-3" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->get('status') === null ? 'active' : '' }}"
                               href="{{ route('role.index', ['status' => null]) }}">
                                <i class="ri-store-2-fill me-1 align-bottom"></i>
                                @lang('translation.users')
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === 'active' ? 'active' : '' }}"
                               href="{{ route('role.index', ['status' => 'active']) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang('translation.active')
                                <span class="badge bg-info bg-gradient align-middle ms-1">
                                    {{ $statusStats['active'] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->status === 'inactive' ? 'active' : '' }}"
                               href="{{ route('role.index', ['status' => 'inactive']) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang('translation.inactive')
                                <span class="badge bg-danger align-middle ms-1">
                                    {{ $statusStats['inactive'] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item my-3">|
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::USER->name ? 'active' : '' }}"
                               href="{{ route('role.index', ['role' => \App\Enums\RoleEnum::USER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang(\App\Enums\RoleEnum::USER->value)
                                <span class="badge bg-success bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::USER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::APPRAISER->name ? 'active' : '' }}"
                               href="{{ route('role.index', ['role' => \App\Enums\RoleEnum::APPRAISER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang( \App\Enums\RoleEnum::APPRAISER->value)
                                <span class="badge bg-primary bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::APPRAISER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link py-3 {{ request()->role === \App\Enums\RoleEnum::DILLER->name ? 'active' : '' }}"
                               href="{{ route('role.index', ['role' => \App\Enums\RoleEnum::DILLER->name]) }}">
                                <i class="ri-checkbox-circle-line me-1 align-bottom"></i>
                                @lang( \App\Enums\RoleEnum::DILLER->value )
                                <span class="badge bg-success bg-gradient align-middle ms-1">
                                    {{ $roleStats[\App\Enums\RoleEnum::DILLER->name] ?? 0 }}
                                </span>
                            </a>
                        </li>
                    </ul>
                    <div class="table-responsive ">
                        <table class="table table-borderless table-hover align-middle table-nowrap mb-0">
                            <thead>
                                <tr>
                                    <th scope="col">№</th>
                                    <th scope="col">@lang('translation.avatar')</th>
                                    <th scope="col">@lang('translation.username')</th>
                                    <th scope="col">@lang('translation.phone-number')</th>
                                    <th scope="col">@lang('translation.registered-date')</th>
                                    <th scope="col">@lang('translation.role')</th>
                                    <th scope="col">@lang('translation.status')</th>
                                    <th scope="col">@lang('translation.actions')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $user)
                                    <tr>
                                        <td class="fw-medium">{{($users->currentPage() - 1) * $users->perPage() + $loop->iteration}}</td>
                                        <td>
                                            <img src="{{ URL::asset($user->avatar) }}" alt="{{ $user->name }}"
                                                 class="rounded-circle header-profile-user">
                                        </td>
                                        <td>
                                            <a class="text-decoration-underline"
                                               href="{{ route('user.show', [$user->id]) }}">
                                                {{ $user->name }}
                                            </a>
                                        </td>
                                        <td>
                                            <span class="phone-number">
                                                @component('components.callable-phone ',['phone'=>$user->phone])@endcomponent
                                            </span>
                                        </td>
                                        <td>{{ $user->created_at }}</td>
                                        <td>
                                            <span class="badge badge-soft-info text-uppercase">
                                                @lang('translation.'.$user->role)
                                             </span>
                                        </td>
                                        <td class="text-uppercase">
                                            @if ($user->status == 'active')
                                                <button type="button"
                                                        class="btn btn-success btn-label waves-effect waves-light">
                                                    <i class="ri-emotion-happy-line label-icon align-middle fs-16 me-2"></i>
                                                    @lang('translation.'.$user->status)
                                                </button>
                                            @else
                                                <button type="button"
                                                        class="btn btn-danger btn-label waves-effect waves-light">
                                                    <i
                                                        class="ri-emotion-unhappy-line label-icon align-middle fs-16 me-2"></i>
                                                    @lang('translation.'.$user->status)
                                                </button>
                                            @endif
                                        </td>
                                        <td>
                                            <div class="flex-shrink-0">
                                                <span class="border-3 bg-info text-white rounded p-1 px-3"
                                                    data-bs-toggle="modal" data-bs-user-name="{{ $user->name }}"
                                                    data-bs-user-id="{{ $user->id }}" data-bs-target="#userPermitModal">
                                                    <i class="ri-flag-line align-bottom me-1"></i>
                                                    @lang('translation.permit-user')
                                                </span>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="mt-3">{{ $users->withQueryString()->links() }}</div>
                </div><!-- end card-body -->
            </div><!-- end card -->
        </div><!-- end col -->
    </div>

    <!-- Varying modal content -->
    <div class="modal fade" id="userPermitModal" tabindex="-1" aria-labelledby="userPermitModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="userPermitModalLabel">@lang('translation.permit-user')</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form action="{{ route('role.permit') }}" method="post">
                    @csrf
                    @method('put')
                    <div class="modal-body">
                        <input type="hidden" name="user_id" id="user-id">
                        <div class="mb-3">
                            <label for="user-name" class="col-form-label">@lang('translation.user-name'):</label>
                            <input type="text" class="form-control" id="user-name">
                        </div>
                        <div class="mb-3">
                            <label for="role" class="form-label">@lang('translation.role')</label>
                            <select name="role" id="role" class="form-control">
                                @foreach (\App\Enums\RoleEnum::cases() as $role)
                                    <option class="text-uppercase" value="{{ $role->name }}">
                                        @lang($role->value)
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-bs-dismiss="modal">@lang('translation.cancel')</button>
                        <button type="submit" class="btn btn-primary">@lang('translation.permit-user')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{ URL::asset('/assets/js/app.min.js') }}"></script>
    <script>
        var userPermitModal = document.getElementById('userPermitModal')
        userPermitModal.addEventListener('show.bs.modal', function(event) {
            // Button that triggered the modal
            var button = event.relatedTarget;
            // Extract info from data-bs-* attributes
            var userNameAttr = button.getAttribute('data-bs-user-name');
            var userIdAttr = button.getAttribute('data-bs-user-id');
            // If necessary, you could initiate an AJAX request here
            // and then do the updating in a callback.
            //
            // Update the modal's content.
            var modalTitle = userPermitModal.querySelector('.modal-title');
            var modalBodyUserName = userPermitModal.querySelector('.modal-body input#user-name');
            var modalBodyUserId = userPermitModal.querySelector('.modal-body input#user-id');

            modalTitle.textContent = 'Foydalanuvchiga rol berish ' + userNameAttr;
            modalBodyUserName.value = userNameAttr;
            modalBodyUserId.value = userIdAttr;
        })
    </script>
@endsection
