if (document.querySelector(".cleave-price")) {
    var cleaveNumeral = new Cleave('.cleave-price', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        delimiter: '·'
    });
} // Dropzone
var conclusionViewModalLabel = document.getElementById('conclusionViewModal');
conclusionViewModalLabel.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget; // Extract info from data-bs-* attributes

    var file = button.getAttribute('data-bs-file'); // If necessary, you could initiate an AJAX request here
    var fileName = button.getAttribute('data-bs-file-name'); // If necessary, you could initiate an AJAX request here
    // and then do the updating in a callback.
    //
    // Update the modal's content.

    var modalTitle = conclusionViewModalLabel.querySelector('.modal-title');
    var modalBodyInput = conclusionViewModalLabel.querySelector('.modal-body object');
    modalTitle.textContent = 'Opening file ' + fileName;
    modalBodyInput.setAttribute("data", file);
});
