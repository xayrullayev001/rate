var dropzonePreviewNode = document.querySelector("#dropzone-preview-list");
dropzonePreviewNode.itemid = "1236";
var previewTemplate = dropzonePreviewNode.parentNode.innerHTML;
dropzonePreviewNode.parentNode.removeChild(dropzonePreviewNode);
var _token = document.querySelector('input[name="_token"]').value;
var _action = document.querySelector("#create-file-form").action;
Dropzone.autoDiscover = false;

let dropzoneForm = new Dropzone("#create-file-form", {
    url: _action,
    previewTemplate: previewTemplate,
    previewsContainer: "#dropzone-preview",
    headers: {
        'X-CSRF-TOKEN': _token,
    }
});
let dropzoneModalForm = new Dropzone("#create-file-modal-form", {
    url: _action,
    previewTemplate: previewTemplate,
    previewsContainer: "#dropzone-preview",
    headers: {
        'X-CSRF-TOKEN': _token,
    }
});
// dropzone.on("addedfile", file => {
//     console.log(`File added: ${file.name}`);
// });
