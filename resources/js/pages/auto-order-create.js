/*
Template Name: Velzon - Admin & Dashboard Template
Author: Themesbrand
Website: https://Themesbrand.com/
Contact: Themesbrand@gmail.com
File: Ecommerce product create Js File
*/


if (document.querySelector("#cleave-numeral")) {
    var cleaveNumeral = new Cleave('#cleave-numeral', {
        numeral: true,
        numeralThousandsGroupStyle: 'thousand',
        delimiter: '·',
    });
}

// Dropzone
var dropzonePreviewNode = document.querySelector("#dropzone-preview-list");
dropzonePreviewNode.itemid = "321321321";
var previewTemplate = dropzonePreviewNode.parentNode.innerHTML;
dropzonePreviewNode.parentNode.removeChild(dropzonePreviewNode);
var files = document.getElementById('files');
var _token = document.querySelector('input[name=\'_token\']').value;
var _type = document.querySelector('input[name=\'type\']').value;
var _orderId = document.querySelector('input[name=\'order_id\']').value;
var _orderPrice = document.querySelector('input[name=\'price\']').value;

var dropzone = new Dropzone(".dropzone", {
    url: 'http://velzon-default.lc/store/my/file',
    method: "post",
    previewTemplate: previewTemplate,
    previewsContainer: "#dropzone-preview",
    headers: {
        'X-CSRF-TOKEN': _token,
        'Attachment-type': _type,
        'Attachment-Order-Id': _orderId,
        'Attachment-Order-Price': _orderPrice
    },
    success: function (file, response) {
        //Here you can get your response.
        if (files.value === "[]") {
            files.value = [response];
        } else {
            var items = [];
            items.push(files.value);
            items.push(response);
            files.value = items;
        }
    }
});// Form Event
