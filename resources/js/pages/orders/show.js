var conclusionViewModalLabel = document.getElementById('conclusionViewModal');
conclusionViewModalLabel.addEventListener('show.bs.modal', function (event) {
    // Button that triggered the modal
    var button = event.relatedTarget; // Extract info from data-bs-* attributes

    var file = button.getAttribute('data-bs-file'); // If necessary, you could initiate an AJAX request here
    var fileName = button.getAttribute('data-bs-file-name'); // If necessary, you could initiate an AJAX request here
    console.log("btn id", fileName);
    // and then do the updating in a callback.
    //
    // Update the modal's content.

    var modalTitle = conclusionViewModalLabel.querySelector('.modal-title');
    var modalBodyInput = conclusionViewModalLabel.querySelector('.modal-body object');
    modalTitle.textContent = 'Opening file ' + fileName;
    modalBodyInput.setAttribute("data", file);
});

var dropzonePreviewNode = document.querySelector("#dropzone-preview-list");
dropzonePreviewNode.itemid = "1234524";
var previewTemplate = dropzonePreviewNode.parentNode.innerHTML;
dropzonePreviewNode.parentNode.removeChild(dropzonePreviewNode);
var _token = document.querySelector('input[name="_token"]').value;
Dropzone.autoDiscover = false;

var file_up_names = [];
let dropzoneForm = new Dropzone("#createfile-form", {
    previewTemplate: previewTemplate,
    previewsContainer: "#dropzone-preview",
    createImageThumbnails: true,
    acceptedFiles: ".png,.jpg,.jpeg",
    clickable: true,
    addRemoveLinks: false,
    headers: {
        'X-CSRF-TOKEN': _token,
    },
    /*success: function(file, response) {
        file_up_names.push(response);
        console.table(file_up_names);
    },
    removedfile: function(file) {
        x = confirm('Do you want to delete this logo?');
        if (!x) return false;
        for (var i = 0; i < file_up_names.length; i++) {
            console.table(file_up_names);
            console.log(file);
            if (file_up_names[i] === file) {
                alert(file_up_names[i]);
                $.ajax({
                    type: 'POST',
                    url: 'delete-logo.php',
                    data: "file_name=" + file_up_names[i],
                    dataType: 'html'
                });
                myDropzone.options.maxFiles = myDropzone.options.maxFiles + 1;
                var _ref;
                return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file
                    .previewElement) : void 0;
            }
        }
    },
    dictRemoveFile: 'Remove Logo',*/
});




