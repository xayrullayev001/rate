let handleCustomerClick = function (element) {
    var customerBlock = document.getElementById("customer-block");
    var customerCompanyBlock = document.getElementById("customer-company-block");
    if (element === 'physical') {
        customerBlock.classList.remove('d-none');
        customerCompanyBlock.classList.add('d-none');
    } else {
        customerBlock.classList.add('d-none');
        customerCompanyBlock.classList.remove('d-none');
    }
}
let handleOwnerClick = function (element) {
    var ownerBlock = document.getElementById("owner-block");
    var ownerCompanyBlock = document.getElementById("owner-company-block");
    if (element === 'physical') {
        ownerBlock.classList.remove('d-none');
        ownerCompanyBlock.classList.add('d-none');
    } else {
        ownerBlock.classList.add('d-none');
        ownerCompanyBlock.classList.remove('d-none');
    }
}
