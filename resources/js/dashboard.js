// Vertical Swiper
// var swiper = new Swiper(".vertical-swiper", {
//     slidesPerView: 2,
//     spaceBetween: 10,
//     mousewheel: true,
//     loop: true,
//     direction: "vertical",
//     autoplay: {
//         delay: 2500,
//         disableOnInteraction: false,
//     },
// });

var layoutRightSideBtn = document.querySelector('.layout-rightside-btn');
if (layoutRightSideBtn) {
    Array.from(document.querySelectorAll(".layout-rightside-btn")).forEach(function (item) {
        var userProfileSidebar = document.querySelector(".layout-rightside-col");
        item.addEventListener("click", function () {
            if (userProfileSidebar.classList.contains("d-block")) {
                userProfileSidebar.classList.remove("d-block");
                userProfileSidebar.classList.add("d-none");
            } else {
                userProfileSidebar.classList.remove("d-none");
                userProfileSidebar.classList.add("d-block");
            }
        });
    });
    window.addEventListener("resize", function () {
        var userProfileSidebar = document.querySelector(".layout-rightside-col");
        if (userProfileSidebar) {
            Array.from(document.querySelectorAll(".layout-rightside-btn")).forEach(function () {
                if (window.outerWidth < 1699 || window.outerWidth > 3440) {
                    userProfileSidebar.classList.remove("d-block");
                } else if (window.outerWidth > 1699) {
                    userProfileSidebar.classList.add("d-block");
                }
            });
        }
    });
    var overlay = document.querySelector('.overlay');
    if (overlay) {
        document.querySelector(".overlay").addEventListener("click", function () {
            if (document.querySelector(".layout-rightside-col").classList.contains('d-block') == true) {
                document.querySelector(".layout-rightside-col").classList.remove("d-block");
            }
        });
    }
}
var chartNode = document.querySelector("#store-visits-source2");
if (chartNode) {
    var options2 = {
        series: JSON.parse(chartNode.getAttribute('data-values')),
        labels: JSON.parse(chartNode.getAttribute('data-labels')),
        chart: {
            height: 333,
            type: "donut",
        },
        legend: {
            position: "bottom",
        },
        stroke: {
            show: false
        },
        dataLabels: {
            dropShadow: {
                enabled: false,
            },
        },
        colors: JSON.parse(chartNode.getAttribute('data-colors')),
    };
    var chart2 = new ApexCharts(
        document.querySelector("#store-visits-source2"),
        options2
    );
    chart2.render();
}
