<?php

namespace Database\Seeders;

use App\Models\AutoOrder;
use Illuminate\Database\Seeder;

class AutoOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        AutoOrder::factory()
            ->count(30)
            ->create();
    }
}
