<?php

namespace Database\Seeders;

use App\Models\EstateOrder;
use Illuminate\Database\Seeder;

class EstateOrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EstateOrder::factory()
            ->count(30)
            ->create();
    }
}
