<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        DB::statement("create sequence if not exists order_number_seq start 1001 increment 1");
        Schema::create('auto_orders', function (Blueprint $table) {
            $table->id();
            $table->string("number");
//            $table->foreignId("diller_id")->references('id')->on('users');
            $table->bigInteger("diller_id")->nullable();

            $table->string("ordered_customer")->nullable();
            $table->string("ordered_customer_phone")->nullable();

            $table->string("purpose");
            $table->string("car_mark");
            $table->string("car_category");
            $table->string("car_subcategory");

            $table->bigInteger("cost", false,)->comment("cost of service");
            $table->string("note", 1024)->nullable();

            $table->bigInteger("price", false, true)->nullable()->comment("paid by customer summ");
            $table->string("price_note", 1024)->nullable()->comment("note on time evaluate order");
            $table->string("status");
            $table->string('customer_first_name')->nullable()->comment("null when customer is juridical");
            $table->string('customer_last_name')->nullable()->comment("null when customer is juridical");
            $table->string('customer_patronymic')->nullable()->comment("null when customer is juridical");
            $table->string('customer_company')->nullable()->comment("null when customer is physical");
            $table->string('customer_type');

            $table->string('owner_first_name')->nullable()->comment("null when owner is juridical");
            $table->string('owner_last_name')->nullable()->comment("null when owner is juridical");
            $table->string('owner_patronymic')->nullable()->comment("null when owner is juridical");
            $table->string('owner_company')->nullable()->comment("null when owner is physical");
            $table->string('owner_type');

            $table->string('car_number');
            $table->string('body');
            $table->string('engine');
            $table->string('color')->nullable();
            $table->string('tech_passport')->nullable();
            $table->string('tech_given_date')->nullable();
            $table->string('tech_given_whom')->nullable();
            $table->string('type');
            $table->string('shassi');
            $table->string('made_date');
            $table->bigInteger('object_price')->nullable()->comment("order evaluated price");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auto_orders');
    }
};
