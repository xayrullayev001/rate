<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->uuid('id')->unique();
            $table->string('path');
            $table->string('extension', 5);
            $table->integer('order_id');
            $table->string('type')->default('ALL');
            $table->string('name')->default('File uploaded');
            $table->string('size')->default(1);
            $table->bigInteger('size_in_bytes')->default(0);
            $table->string('order_type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('files');
    }
};
