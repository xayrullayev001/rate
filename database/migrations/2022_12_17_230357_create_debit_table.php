<?php

use App\Models\Debit;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('debits', function (Blueprint $table) {
            $table->id();
            $table->foreignId('appraiser_id')->references('id')->on('users');
            $table->bigInteger('order_id')->nullable();
            $table->bigInteger("cost", false, true);
            $table->string('customer');
            $table->string('expired');
            $table->string('order_type')->nullable();
            $table->string('status')->default(Debit::$NOPAID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('debits');
    }
};
