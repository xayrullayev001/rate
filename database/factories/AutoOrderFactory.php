<?php

namespace Database\Factories;

use App\Enums\OrderStatusEnum;
use App\Enums\RoleEnum;
use App\Models\AutoOrder;
use App\Models\Bonus;
use App\Models\OrderMembers;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AutoOrder>
 */
class AutoOrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'number' => "AUTO-" . $this->faker->numberBetween(10001, 2002),
            'customer_first_name' => $this->faker->firstName(),
            'customer_last_name' => $this->faker->lastName(),
            'customer_patronymic' => $this->faker->userName(),
            'customer_company' => $this->faker->company(),
            'customer_type' => $this->faker->randomElement([
                'juridical', 'physical'
            ]),
            'owner_first_name' => $this->faker->firstName(),
            'owner_last_name' => $this->faker->lastName(),
            'owner_patronymic' => $this->faker->userName(),
            'owner_company' => $this->faker->company(),
            'owner_type' => $this->faker->randomElement([
                'juridical', 'physical'
            ]),
            'purpose' => $this->faker->randomElement(["CREDIT", "REALIZE", "INVEST", "NOTARIAL"]),
            'car_mark' => $this->faker->randomElement([
                'Damas', 'Nexia', 'Nexia 2', 'Nexia 3', 'Spark', 'Cobalt', 'Malibu', 'Gentra'
            ]),
            'car_category' => $this->faker->randomElement([
                'local1', 'local2', 'local3', 'local4', 'local5', 'local6', 'import1',
                'import2', 'import3', 'import4', 'import5', 'import6', 'special1', 'special2',
                'special3', 'special4', 'special5', 'special6', 'special7', 'special8', 'special9'
            ]),
            'car_subcategory' => $this->faker->randomElement([
                'local1', 'local2', 'local3', 'local4', 'local5', 'local6', 'import1',
                'import2', 'import3', 'import4', 'import5', 'import6', 'special1', 'special2',
                'special3', 'special4', 'special5', 'special6', 'special7', 'special8', 'special9'
            ]),
            'color' => $this->faker->colorName(),
            'diller_id' => User::select('id')
                ->where('role', RoleEnum::APPRAISER->name)
                ->value('id'),
            'made_date' => $this->faker->date('Y-m-d'),
            'body' => Str::random(21),
            'engine' => Str::random(21),
            //AAF № 3004735 выдан CHILONZOR TRIB от 19.06.2021 года
            'tech_passport' => "AAF № " . $this->faker->numberBetween(3004735, 3104735) . " выдан " . $this->faker->address() . " от " . $this->faker->date('Y-m-d') . " года",
            'tech_given_date' => $this->faker->date('Y-m-d'),
            'tech_given_whom' => $this->faker->address(),
            'cost' => $this->faker->numberBetween(100000, 100000000),
            'note' => $this->faker->sentence(10),
            'type' => $this->faker->sentence(2),
            'shassi' => $this->faker->sentence(1),
            'car_number' => $this->faker->randomElement([01, 40, 50, 60, 70, 75, 95, 10, 25, 30, 90]) . " " . strtoupper(Str::random(1)) . " " . $this->faker->numberBetween(0, 999) . " " . strtoupper(Str::random(2)),
            'status' => $this->faker->randomElement([
                OrderStatusEnum::DRAFT->name,
                OrderStatusEnum::STARTED->name,
                OrderStatusEnum::FINISHED->name,
                OrderStatusEnum::APPROVED->name,
                OrderStatusEnum::REJECTED->name
            ])
        ];
    }

    public function configure(): AutoOrderFactory
    {
        return $this->afterCreating(function (AutoOrder $order) {
            Log::debug("After creating AutoOrderModel", [$order->id, $order->status]);
            if (in_array($order->status, [
                OrderStatusEnum::STARTED->name,
                OrderStatusEnum::FINISHED->name,
                OrderStatusEnum::APPROVED->name,
                OrderStatusEnum::REJECTED->name
            ])) {
                Log::debug('Orders status is', [$order->status]);

                $appraiser = User::select('id')
                    ->where('role', RoleEnum::APPRAISER->name)
                    ->first();
                Log::debug("User is role appraiser some one", [$appraiser->id]);

                OrderMembers::factory(1)
                    ->create([
                        'user_id' => $appraiser->id,
                        'order_id' => $order->id
                    ]);
                if ($order->status == OrderStatusEnum::APPROVED->name) {
                    Log::debug("Giving bonus to appraiser for order", [$appraiser->id, $order->id]);
                    $givingBonusToOrderAppraiser = User::find($appraiser->id);
                    $bonusAmount = $this->faker->randomElement([10000, 15000, 20000], 1);
                    Log::debug("Bonus amount", [$bonusAmount]);
                    Bonus::factory(1)
                        ->create([
                            'appraiser_id' => $appraiser->id,
                            'order_id' => $order->id,
                            'amount' => $bonusAmount
                        ]);
                    $balance = $bonusAmount + $givingBonusToOrderAppraiser->balance;
                    $givingBonusToOrderAppraiser->update([
                        'balance' => $balance
                    ]);
                }
            }
            return $order;
        });
    }
}
