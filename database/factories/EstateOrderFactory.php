<?php

namespace Database\Factories;

use App\Enums\OrderStatusEnum;
use App\Enums\PurposeEnum;
use App\Enums\RoleEnum;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Model>
 */
class EstateOrderFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'number' => "ESTATE-" . $this->faker->numberBetween(10001, 2002),
            'customer_first_name' => $this->faker->firstName(),
            'customer_last_name' => $this->faker->lastName(),
            'customer_patronymic' => $this->faker->userName(),
            'customer_company' => $this->faker->company(),
            'customer_type' => $this->faker->randomElement([
                'juridical', 'physical'
            ]),
            'owner_first_name' => $this->faker->firstName(),
            'owner_last_name' => $this->faker->lastName(),
            'owner_patronymic' => $this->faker->userName(),
            'owner_company' => $this->faker->company(),
            'owner_type' => $this->faker->randomElement([
                'juridical', 'physical'
            ]),
            'purpose' => $this->faker->randomElement([
                PurposeEnum::CREDIT->name,
                PurposeEnum::REALIZE->name,
                PurposeEnum::INVEST->name,
                PurposeEnum::NOTARIAL->name
            ]),
            'name_of_object' => $this->faker->company(),
            'contract_date' => $this->faker->date('d-m-Y'),
            'region' => $this->faker->address(),
            'district' => $this->faker->address(),
            'address' => $this->faker->address(),
            'home' => $this->faker->address(),
            'area' => $this->faker->randomElement([62, 79, 86, 105, 150, 300]),
            'overall_area' => $this->faker->randomElement([62, 79, 86, 105, 150, 300]),
            'usefull_area' => $this->faker->randomElement([62, 79, 86, 105, 150, 300]),
            'live_area' => $this->faker->randomElement([62, 79, 86, 105, 150, 300]),
            'diller_id' => User::select('id')
                ->where('role', RoleEnum::APPRAISER->name)
                ->value('id'),
            //AAF № 3004735 выдан CHILONZOR TRIB от 19.06.2021 года
            'tech_passport' => "AAF № " . $this->faker->numberBetween(3004735, 3104735) . " выдан " . $this->faker->address() . " от " . $this->faker->date('Y-m-d') . " года",
            'tech_given_date' => $this->faker->date('d-m-Y'),
            'tech_given_whom' => $this->faker->address(),
            'cost' => $this->faker->numberBetween(100000, 100000000),
            'note' => $this->faker->sentence(10),
            'status' => $this->faker->randomElement([
                OrderStatusEnum::DRAFT->name,
                OrderStatusEnum::STARTED->name,
                OrderStatusEnum::FINISHED->name,
                OrderStatusEnum::APPROVED->name,
                OrderStatusEnum::REJECTED->name
            ])
        ];
    }
}
