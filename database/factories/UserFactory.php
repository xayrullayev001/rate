<?php

namespace Database\Factories;

use App\Enums\RoleEnum;
use Exception;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class UserFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     * @throws Exception
     */
    public function definition(): array
    {
        return [
            'name' => $this->faker->userName(),
            'phone' => '+9989' .'-'. $this->faker->numberBetween(0, 1) . '-' . $this->faker->numberBetween(000, 999) . '-' . $this->faker->numberBetween(00, 99) . '-' . $this->faker->numberBetween(00, 99),
            'email_verified_at' => now(),
            'password' => '$2y$10$92IXUNpkjO0rOQ5byMi.Ye4oKoEa3Ro9llC/.og/at2.uheWG/igi', // password
            'remember_token' => Str::random(10),
            'avatar' => '/assets/images/users/avatar-' . $this->faker->numberBetween(1, 10) . '.jpg',
            'role' => $this->faker->randomElement([
                "APPRAISER","DILLER","USER"]),
            'status' => $this->faker->randomElement(['active', 'inactive'])
        ];
    }

    /**
     * Indicate that the model's email address should be unverified.
     *
     * @return Factory
     */
    public function unverified(): Factory
    {
        return $this->state(function (array $attributes) {
            return [
                'email_verified_at' => null,
            ];
        });
    }
}
