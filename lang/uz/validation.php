<?php

return [
    'validation' => [
        'required' => 'translation.field-is-required',
        'exists' => 'translation.field-is-not-exists'
    ],
    'custom' => array(
        'number' => array(
            'required' => 'translation.field-is-required',
        ),
        'ordered_customer_phone' => [
            'required' => 'translation.field-is-required',
        ],
        'diller_id' => [
            'required' => 'translation.field-is-required',
        ],
        'ordered_customer' => [
            'required' => 'translation.field-is-required',
        ],
        'customer_first_name' => [
            'required_if' => 'translation.field-is-required',
        ],
        'customer_last_name' => [
            'required_if' => 'translation.field-is-required',
        ],
        'customer_patronymic' => [
            'required_if' => 'translation.field-is-required',
        ],
        'customer_company' => [
            'required_if' => 'translation.field-is-required',
        ],
        'owner_first_name' => [
            'required_if' => 'translation.field-is-required',
        ],
        'owner_last_name' => [
            'required_if' => 'translation.field-is-required',
        ],
        'owner_patronymic' => [
            'required_if' => 'translation.field-is-required',
        ],
        'owner_company' => [
            'required_if' => 'translation.field-is-required',
        ],
        'purpose' => [
            'required' => 'translation.field-is-required',
        ],
        'car_mark' => [
            'required' => 'translation.field-is-required',
        ],
        'car_category' => [
            'required' => 'translation.field-is-required',
        ],
        'car_subcategory' => [
            'required' => 'translation.field-is-required',
        ],
        'color' => [
            'required' => 'translation.field-is-required',
        ],
        'made_date' => [
            'required' => 'translation.field-is-required',
        ],
        'body' => [
            'required' => 'translation.field-is-required',
        ],
        'engine' => [
            'required' => 'translation.field-is-required',
        ],
        'tech_passport' => [
            'required' => 'translation.field-is-required',
        ],
        'tech_given_date' => [
            'required' => 'translation.field-is-required',
        ],
        'tech_given_whom' => [
            'required' => 'translation.field-is-required',
        ],
        'type' => [
            'required' => 'translation.field-is-required',
        ],
        'shassi' => [
            'required' => 'translation.field-is-required',
        ],
        'cost' => [
            'required' => 'translation.field-is-required',
        ],
        'note' => [
            'required' => 'translation.field-is-required',
        ],
        'car_number' => [
            'required' => 'translation.field-is-required',
        ],
        'status' => [
            'required' => 'translation.field-is-required',
        ],

        'file' => [
            'required' => 'translation.field-is-required',
        ],
        'expired' => [
            'required' => 'translation.field-is-required',
        ],
        'price' => [
            'required_if' => 'translation.field-is-required',
        ],
        'is_diller' => [
            'required' => 'translation.field-is-required',
        ],
        'order_id' => [
            'required' => 'translation.field-is-required',
        ],
        'object_price' => [
            'required' => 'translation.field-is-required',
        ],
        'password' => [
            'required' => 'translation.field-is-required',
        ],
        'phone' => [
            'required' => 'translation.field-is-required',
            'exists' => 'translation.user-blocked',
        ],
    ),
];
