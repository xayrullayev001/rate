<?php

namespace App\Http\Requests\Estate;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @property $cost int
 */
class UpdateRequest extends FormRequest
{
    /**
     * The URI that users should be redirected to if validation fails.
     *
     * @var string
     */
    //    protected $redirect = '/auto/create';

    /**
     * The route that users should be redirected to if validation fails.
     *
     * @var string
     */
    // protected $redirectRoute = 'auto.index';

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'number' => 'required|unique:auto_orders',
            'ordered_customer' => 'string|nullable',
            'ordered_customer_phone' => 'string|nullable',
            'diller_id' => 'exists:users,id,role,DILLER|nullable',

            'customer_first_name' => ['required_if:customer_type,physical', 'required_with:customer_last_name,customer_patronymic', 'nullable', 'string', 'max:191'],
            'customer_last_name' => ['required_if:customer_type,physical', 'required_with:customer_first_name,customer_patronymic', 'nullable', 'string', 'max:191'],
            'customer_patronymic' => ['required_if:customer_type,physical', 'required_with:customer_first_name,customer_last_name', 'nullable', 'string', 'max:191'],
            'customer_company' => ['required_if:customer_type,juridical', 'nullable', 'string', 'max:191'],
            'customer_type' => 'string',
            'owner_first_name' => ['required_if:owner_type,physical', 'required_with:owner_patronymic,owner_last_name', 'nullable', 'string', 'max:191'],
            'owner_last_name' => ['required_if:owner_type,physical', 'required_with:owner_first_name,owner_patronymic', 'nullable', 'string', 'max:191'],
            'owner_patronymic' => ['required_if:owner_type,physical', 'required_with:owner_first_name,owner_last_name', 'nullable', 'string', 'max:191'],
            'owner_company' => ['required_if:owner_type,juridical', 'nullable', 'string', 'max:191'],
            'owner_type' => 'string',
            'purpose_id' => 'exists:purposes,id',

            'contract_date' => 'required|date|max:191',
            'name_of_object' => 'required|string|max:191',
            'region' => 'required|integer|exists:regions,id',
            'district' => 'required|integer|exists:districts,id',
            'address' => 'required|string|max:191',
            'home' => 'string|max:191',
            'area' => 'string|max:191',
            'overall_area' => 'string|max:191',
            'usefull_area' => 'string|max:191',
            'live_area' => 'string|max:191',

//            'tech_passport' => 'required|string|max:191',
//            'tech_given_date' => 'required|date|max:191',
//            'tech_given_whom' => 'required|string|max:256',

            'cost' => 'required',
            'note' => 'string|nullable|max:1024',
            'status' => 'required',
            'created_at'=>'date'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'number' => __('translation.order-number'),
            'ordered_customer' => __('translation.ordered-customer'),
            'ordered_customer_phone' => __('translation.ordered-customer-phone'),
            'diller_id' => __('translation.diller'),
            'customer_first_name' => __('translation.customer-first-name'),
            'customer_last_name' => __('translation.customer-last-name'),
            'customer_patronymic' => __('translation.customer-patronymic'),
            'customer_company' => __('translation.customer-company'),
            'owner_first_name' => __('translation.owner-first-name'),
            'owner_last_name' => __('translation.owner-last-name'),
            'owner_patronymic' => __('translation.owner-patronymic'),
            'owner_company' => __('translation.owner-company'),
            'purpose' => __('translation.purpose'),
            'car_mark' => __('translation.car-mark'),
            'car_category' => __('translation.car_category'),
            'car_subcategory' => __('translation.car-subcategory'),
            'color' => __('translation.color'),
            'made_date' => __('translation.made-date'),
            'body' => __('translation.body'),
            'engine' => __('translation.engine'),
//            'tech_passport' => __('translation.technical-passport'),
//            'tech_given_date' => __('translation.technical-passport-given-date'),
//            'tech_given_whom' => __('translation.technical-passport-given-by-whom'),
            'type' => __('translation.type'),
            'shassi' => __('translation.shassi'),
            'cost' => __('translation.cost'),
            'note' => __('translation.note'),
            'car_number' => __('translation.car-number'),
            'status' => __('translation.status'),
        ];
    }

    protected function prepareForValidation()
    {
        $cost = str_replace(',', '', $this->cost);
        $this->merge([
            'cost' => $cost,
            'number' => mb_strtoupper($this->number),
            'ordered_customer' => mb_strtoupper($this->ordered_customer),
            'customer_first_name' => mb_strtoupper($this->customer_first_name),
            'customer_last_name' => mb_strtoupper($this->customer_last_name),
            'customer_patronymic' => mb_strtoupper($this->customer_patronymic),
            'customer_company' => mb_strtoupper($this->customer_company),
            'owner_first_name' => mb_strtoupper($this->owner_first_name),
            'owner_last_name' => mb_strtoupper($this->owner_last_name),
            'owner_patronymic' => mb_strtoupper($this->owner_patronymic),
            'owner_company' => mb_strtoupper($this->owner_company),
            'tech_passport' => mb_strtoupper($this->tech_passport),
            'tech_given_whom' => mb_strtoupper($this->tech_given_whom),

            'name_of_object' => mb_strtoupper($this->name_of_object),
            'region' => mb_strtoupper($this->region),
            'district' => mb_strtoupper($this->district),
            'address' => mb_strtoupper($this->address),
            'home' => mb_strtoupper($this->home),
            'area' => mb_strtoupper($this->area),
            'overall_area' => mb_strtoupper($this->overall_area),
            'usefull_area' => mb_strtoupper($this->usefull_area),
            'live_area' => mb_strtoupper($this->live_area),

            'note' => mb_strtoupper(trim($this->note)),
            'car_number' => mb_strtoupper($this->car_number)
        ]);
    }
}
