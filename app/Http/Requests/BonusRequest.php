<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BonusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'appraiser_id' => ['required', 'exists:users,id'],
            'order_id' => ['required', 'exists:auto_orders,id'],
            'order_type' => ['required'],
            'amount' => ['required']
        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'amount' => str_ireplace(',', '', $this->amount)
        ]);
    }
}
