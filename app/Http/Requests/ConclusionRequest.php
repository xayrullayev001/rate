<?php

namespace App\Http\Requests;

use App\Enums\OrderTypeEnum;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ConclusionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'file' => 'required|file|mimes:pdf',
            'expired' => 'required_if:is_diller,yes|date',
            'price' => 'required_if:is_diller,no',
            'is_diller' => 'required',
            'order_id' => 'required',
            'object_price' => 'required',
            'note' => 'nullable',
            'order_type' => ['required', 'string', Rule::in(OrderTypeEnum::AUTO->name, OrderTypeEnum::ESTATE->name)],
        ];
    }

    public function attributes()
    {
        return [

            'file' => trans('translation.file'),
            'expired' => trans('translation.due-date'),
            'price' => trans('translation.price'),
            'is_diller' => trans('translation.diller'),
            'order_id' => trans('translation.order'),
            'object_price' => trans('translation.price'),
            'note' => trans('translation.note'),
            'order_type' => trans('translation.order-type'),


        ];
    }

    public function prepareForValidation()
    {
        $this->merge([
            'price' => str_ireplace(',', '', $this->price),
            'object_price' => str_ireplace(',', '', $this->object_price)
        ]);
    }
}
