<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserFilterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules(): array
    {
        return [
            'status' => 'string|nullable',
            'page' => 'integer|nullable',
            'size' => 'integer|nullable',
            'search' => 'string|nullable',
            'role' => 'string|nullable',
            'appraiser_id' => 'string|nullable',
            // 'period' => 'string|nullable',
            // 'from' => 'string|nullable',
            // 'to' => 'string|nullable'
        ];
    }

    /**
     * Prepare the data for validation.
     *
     * @return void
     */
    // protected function prepareForValidation()
    // {
    //     if ($this->get('period') != null) {
    //         $fromToPeriods = explode(" to ", $this->get('period'));
    //         Log::debug("From to period filter", [$this->get('period'), $fromToPeriods]);

    //         if (isset($fromToPeriods[0])) {
    //             $this->merge([
    //                 'from' => $fromToPeriods[0],
    //                 'to' => $fromToPeriods[0],
    //             ]);
    //         }
    //         if (isset($fromToPeriods[1])) {
    //             $this->merge([
    //                 'to' => $fromToPeriods[1],
    //             ]);
    //         }
    //     }

    //     $this->merge([
    //         'created_by' => $this->user()->getPhone(),
    //         'prefix' => 'AUTO'
    //     ]);
    // }
}
