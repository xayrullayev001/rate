<?php

namespace App\Http\Filters;


use Illuminate\Database\Eloquent\Builder;

class DebitFilter extends Filter
{
    protected function getCallbacks(): array
    {
        return array_merge(parent::getCallbacks(), [
            'appraiser' => [$this, 'appraiser'],
            'diller' => [$this, 'diller'],
            'order_type' => [$this, 'orderType'],
            'status' => [$this, 'status'],
            'search' => [$this, 'search'],
        ]);
    }

    protected function search(Builder $builder, $value): Builder
    {
        // if (strlen($value) > 0) {
        //     return $builder->where('', 'like', "%{$value}%")
        //         ->orWhere('customer', 'like', "%{$value}%");
        //         // ->orWhere('', 'like', "%{$value}%");
        //     // ->orWhere(AutoOrder::$owner, 'like', "%{$value}%");
        // }
        return $builder;
    }

    protected function status(Builder $builder, $value): Builder
    {
        return $builder;
    }

    protected function orderType(Builder $builder, $value): Builder
    {
        return $builder->where('order_type', $value);
    }

    protected function appraiser(Builder $builder, $value): Builder
    {
        return $builder->where('appraiser_id', $value);
    }

    protected function diller(Builder $builder, $value): Builder
    {
        return $builder->where('diller_id', $value);
    }

    protected function customerId(Builder $builder, $value): Builder
    {
        return $builder->where('customer_id', $value);
    }
}
