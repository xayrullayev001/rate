<?php

namespace App\Http\Filters;

use App\Models\AutoOrder;
use Illuminate\Database\Eloquent\Builder;

class AutoFilter extends Filter
{
    protected function getCallbacks(): array
    {

        return array_merge(parent::getCallbacks(), [
            AutoOrder::$purpose => [$this, 'purpose'],
            AutoOrder::$carCategory => [$this, 'car_category'],
            AutoOrder::$status => [$this, 'status'],
            'diller' => [$this, 'diller'],
            self::SEARCH => [$this, self::SEARCH],
        ]);
    }

    protected function search(Builder $builder, $value): Builder
    {
        if (strlen($value) > 0) {
            return $builder->where(AutoOrder::$orderedCustomer, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$orderedCustomerPhone, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$orderNumber, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$customer_first_name, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$customer_last_name, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$customer_patronymic, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$owner_first_name, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$owner_last_name, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$owner_patronymic, 'like', "%{$value}%")
                ->orWhere(AutoOrder::$carMark, 'like', "%{$value}%");
        }
        return $builder;
    }

    protected function purpose(Builder $builder, $value): Builder
    {
        return $builder->where(AutoOrder::$purpose, $value);
    }

    protected function diller(Builder $builder, $value): Builder
    {
        return $builder->where(AutoOrder::$dillerId, $value);
    }

    protected function car_category(Builder $builder, $value): Builder
    {
        return $builder->where(AutoOrder::$carCategory, $value);
    }
}
