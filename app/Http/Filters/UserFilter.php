<?php

namespace App\Http\Filters;

use Illuminate\Database\Eloquent\Builder;

class UserFilter extends Filter
{
    protected function getCallbacks(): array
    {
        return array_merge(parent::getCallbacks(), [
            'role' => [$this, 'role'],
            'roleNot' => [$this, 'roleNot'],
            'status' => [$this, 'status'],
            'search' => [$this, 'search'],
            'appraiser_id' => [$this, 'appraiserId'],
        ]);
    }

    protected function search(Builder $builder, $value): Builder
    {
        if (strlen($value) > 0) {
            return $builder->where('phone', 'like', "%{$value}%")
                ->orWhere('name', 'like', "%{$value}%");
        }
        return $builder;
    }

    protected function role(Builder $builder, $value): Builder
    {
        return $builder->where('role', $value);
    }

    protected function roleNot(Builder $builder, $value): Builder
    {
        return $builder->whereNot('role', $value);
    }

    protected function status(Builder $builder, $value): Builder
    {
        return $builder->where('status', $value);
    }


    protected function appraiserId(Builder $builder, $value): Builder
    {
        return $builder->where('appraiser_id', $value);
    }

    protected function beforeConstruct(array $queryParams)
    {
        $queryParams['roleNot'] = 'admin';
        // $this->roleNot = 'admin';
    }

    public function getAppraiserId()
    {
        return $this->getQueryParam('appraiser_id');
    }
}
