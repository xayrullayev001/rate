<?php

namespace App\Http\Filters;

use App\Models\EstateOrder;
use Illuminate\Database\Eloquent\Builder;

class EstateFilter extends Filter
{
    protected function getCallbacks(): array
    {
        return array_merge(parent::getCallbacks(), [
            EstateOrder::$purpose => [$this, 'purpose'],
            EstateOrder::$status => [$this, self::STATUS],
            self::SEARCH => [$this, self::SEARCH],
            'diller' => [$this, 'diller'],
        ]);
    }

    protected function search(Builder $builder, $value): Builder
    {
        if (strlen($value) > 0) {
            return $builder->where(EstateOrder::$orderedCustomer, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$orderedCustomerPhone, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$orderNumber, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$customer_first_name, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$customer_last_name, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$customer_patronymic, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$owner_first_name, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$owner_last_name, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$owner_patronymic, 'like', "%{$value}%")
                ->orWhere(EstateOrder::$nameOfObject, 'like', "%{$value}%");
        }
        return $builder;
    }

    protected function purpose(Builder $builder, $value): Builder
    {
        return $builder->where(EstateOrder::$purpose, $value);
    }

    protected function diller(Builder $builder, $value): Builder
    {
        return $builder->where('diller_id', $value);
    }
}
