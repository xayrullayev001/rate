<?php

namespace App\Http\Filters;


use Illuminate\Database\Eloquent\Builder;

class Filter extends AbstractFilter
{
    protected $page = 0;

    protected $size = 10;

    protected $status;

    protected $from;

    protected $to;

    protected $search = "";

    protected const SEARCH = "search";
    protected const FROM = "from";
    protected const TO = "to";
    protected const STATUS = "status";
    protected const PAGE = "page";
    protected const SIZE = "size";
    protected const GROUP_BY = "groupBy";

    public function __construct(array $queryParams)
    {
        parent::__construct($queryParams);

        $this->page = $this->getQueryParam(self::PAGE, $this->page);
        $this->size = $this->getQueryParam(self::SIZE, $this->size);
        $this->status = $this->getQueryParam(self::STATUS);
        $this->from = $this->getQueryParam(self::FROM);
        $this->to = $this->getQueryParam(self::TO);
        $this->search = $this->getQueryParam(self::SEARCH);
    }

    /**
     * @return array[]
     */
    protected function getCallbacks(): array
    {
        return [
            self::PAGE => [$this, self::PAGE],
            self::SIZE => [$this, self::SIZE],
            self::FROM => [$this, self::FROM],
            self::TO => [$this, self::TO],
            self::STATUS => [$this, self::STATUS],
            self::GROUP_BY => [$this, self::GROUP_BY]
        ];
    }

    protected function from(Builder $builder, $value): Builder
    {
        return $builder->whereDate('created_at', '>=', $value);
    }

    protected function to(Builder $builder, $value): Builder
    {
        return $builder->whereDate('created_at', '<=', $value);
    }

    protected function status(Builder $builder, $value): Builder
    {
        return $builder->where(self::STATUS, $value);
    }

    protected function page(Builder $builder, $value): Builder
    {
        return $builder->forPage($value, $this->size);
    }

    protected function size(Builder $builder, $value): Builder
    {
        return $builder->forPage($this->page, $value);
    }

    protected function search(Builder $builder, $value): Builder
    {
        return $builder;
    }

    protected function groupBy(Builder $builder, $value): Builder
    {
        return ($value != null) ? $builder->groupBy($value) : $builder;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }

    /**
     * @param int $size
     */
    public function setSize(int $size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @param mixed $from
     */
    public function setFrom($from): void
    {
        $this->from = $from;
    }

    /**
     * @return mixed
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @param mixed $to
     */
    public function setTo($to): void
    {
        $this->to = $to;
    }

    /**
     * @return mixed
     */
    public function getSearch()
    {
        return $this->search;
    }

    /**
     * @param mixed $search
     */
    public function setSearch($search): void
    {
        $this->search = $search;
    }

    /**
     * @return int
     */
    public function getPage(): int
    {
        return $this->page;
    }

    /**
     * @param int $page
     */
    public function setPage(int $page): void
    {
        $this->page = $page;
    }


    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getUserId()
    {
        return $this->userId;
    }

    /**
     * @param mixed $userId
     */
    public function setUserId($userId): void
    {
        $this->userId = $userId;
    }

    /**
     * @return mixed
     */
    public function getAppraiserId()
    {
        return $this->appraiser_id;
    }

    /**
     * @param mixed $appraiserId
     */
    public function setAppraiserId(int $appraiserId): void
    {
        $this->appraiser_id = $appraiserId;
    }

    protected function beforeConstruct(array $queryParams)
    {
        // TODO: Implement beforeConstruct() method.
    }

    protected function afterConstruct(array $queryParams)
    {
        // TODO: Implement afterConstruct() method.
    }
}
