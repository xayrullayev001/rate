<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreCertificateRequest;
use App\Models\Certificate;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view('certificate.index', [
            'certificates' => Certificate::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreCertificateRequest $request
     * @return RedirectResponse
     */
    public function store(StoreCertificateRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $fileUploaded = $request->file('file');
        $destinationPath = 'public/certificate';
        $fileName = Str::uuid() . '.' . $fileUploaded->clientExtension();
        $path = $fileUploaded->storeAs($destinationPath, $fileName);
        Log::debug("Certificate path ", [$path]);
        if (isset($data['id'])) {
            Certificate::where($data['id'])
                ->update([
                    'type' => $data['type'],
                    'path' => $path
                ]);
        } else {
            Certificate::create([
                'type' => $data['type'],
                'path' => $path
            ]);
        }

        return redirect()->to('/certificates');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Certificate $certificate
     * @return RedirectResponse
     */
    public function destroy(Certificate $certificate): RedirectResponse
    {
        Storage::delete($certificate->path);
        $certificate->delete();
        return redirect()->back();
    }
}
