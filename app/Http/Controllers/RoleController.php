<?php

namespace App\Http\Controllers;

use App\Http\Filters\UserFilter;
use App\Http\Requests\UserFilterRequest;
use App\Models\User;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Redirect;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(UserFilterRequest $request): Application|Factory|View
    {
        $data = $request->validated();
        $filter = app()->make(UserFilter::class, ['queryParams' => array_filter($data)]);
        $users = User::filter($filter);

        $statusStats = DB::table('users')
            ->selectRaw(DB::raw('count(*) as qty, status'))
            ->groupBy('status')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$item->status => $item->qty];
            });

        $roleStats = DB::table('users')
            ->selectRaw(DB::raw('count(*) as qty, role'))
            ->whereNot('role', 'admin')
            ->groupBy('role')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$item->role => $item->qty];
            });

        return view('role.index', [
            'users' => $users,
            'size' => $filter->getSize(),
            'roleStats' => $roleStats,
            'statusStats' => $statusStats,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function permit(Request $request): RedirectResponse
    {
        $data = $request->validate([
            'user_id' => 'required|integer',
            'role' => 'required|string'
        ]);

        (new User)->where('id', $data['user_id'])
            ->update(['role' => $data['role']]);

        return Redirect::back();
    }

}
