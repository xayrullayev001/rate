<?php

namespace App\Http\Controllers;


use App\Models\OrderMembers;
use App\Models\TrackingActions;
use App\Models\User;
use App\Models\UserInfo;
use Doctrine\DBAL\Schema\Table;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{

    /**
     * Display the specified resource.
     *
     * @param $collection
     * @return View
     */
    public function profile(): View
    {
        $userInfo = UserInfo::where('user_id', Auth::id())->first();
        $projects ="";/* \DB::select(['auto_orders.updated_at', 'status', 'number', "'AUTO' as type"])
            ->join('auto_orders', 'order_members.order_id', '=', 'auto_orders.id')
            ->where('user_id', Auth::id())
            ->unionAll(OrderMembers::select(['estate_orders.updated_at', 'status', 'number', "'ESTATE' as type"])
                ->join('estate_orders', 'order_members.order_id', '=', 'estate_orders.id')
                ->where('user_id', Auth::id())
            )
            ->get();*/
        return view('user.profile', [
            'user' => Auth::user(),
            'info' => $userInfo,
            'projects' => $projects
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return RedirectResponse
     */
    public function updateProfile(Request $request, $id): RedirectResponse
    {
        $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email'],
            'avatar' => ['nullable', 'image', 'mimes:jpg,jpeg,png', 'max:1024'],
        ]);

        $user = (new User)->findOrFail($id);
        $user->name = $request->get('name');
        $user->email = $request->get('email');

        if ($request->file('avatar')) {
            $avatar = $request->file('avatar');
            $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
            $avatarPath = public_path('/images/');
            $avatar->move($avatarPath, $avatarName);
            $user->avatar = $avatarName;
        }

        $user->update();
        if ($user) {
            Session::flash('message', 'User Details Updated successfully!');
            Session::flash('alert-class', 'alert-success');
            // return response()->json([
            //     'isSuccess' => true,
            //     'Message' => "User Details Updated successfully!"
            // ], 200); // Status code here
            return redirect()->back();
        } else {
            Session::flash('message', 'Something went wrong!');
            Session::flash('alert-class', 'alert-danger');
            // return response()->json([
            //     'isSuccess' => true,
            //     'Message' => "Something went wrong!"
            // ], 200); // Status code here
            return redirect()->back();

        }
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function updatePassword(Request $request, $id): JsonResponse
    {
        $request->validate([
            'current_password' => ['required', 'string'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);

        if (!(Hash::check($request->get('current_password'), Auth::user()->password))) {
            return response()->json([
                'isSuccess' => false,
                'Message' => "Your Current password does not matches with the password you provided. Please try again."
            ], 200); // Status code
        } else {
            $user = User::find($id);
            $user->password = Hash::make($request->get('password'));
            $user->update();
            if ($user) {
                Session::flash('message', 'Password updated successfully!');
                Session::flash('alert-class', 'alert-success');
                return response()->json([
                    'isSuccess' => true,
                    'Message' => "Password updated successfully!"
                ], 200); // Status code here
            } else {
                Session::flash('message', 'Something went wrong!');
                Session::flash('alert-class', 'alert-danger');
                return response()->json([
                    'isSuccess' => true,
                    'Message' => "Something went wrong!"
                ], 200); // Status code here
            }
        }
    }

    /**
     * @return Application|Factory|View
     */
    public function showActivities(): View|Factory|Application
    {
        $actions = TrackingActions::whereUserId(Auth::user()->id)->orderBy('created_at', 'desc')->get();

        return view('profile.activities', [
            'actions' => $actions
        ]);
    }

    /**
     * @return Factory|View|Application
     */
    public function showProjects(): Factory|View|Application
    {
        return view('profile.projects');
    }

    /**
     * @return Application|Factory|View
     */
    public function showDocuments(): View|Factory|Application
    {
        return \view('profile.documents');
    }
}
