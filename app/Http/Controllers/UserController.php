<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Filters\UserFilter;
use App\Http\Requests\UserFilterRequest;
use App\Models\User;
use App\Models\UserInfo;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\ValidationException;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserFilterRequest $request
     * @return Application|Factory|View
     * @throws BindingResolutionException
     */
    public function index(UserFilterRequest $request): Application|Factory|View
    {
        $data = $request->validated();
        $data['roleNot'] = 'admin';
        $filter = app()->make(UserFilter::class, ['queryParams' => array_filter($data)]);

        $users = User::filter($filter);
        $statusStats = DB::table('users')
            ->selectRaw(DB::raw('count(*) as qty, status'))
            ->groupBy('status')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$item->status => $item->qty];
            });

        $roleStats = DB::table('users')
            ->selectRaw(DB::raw('count(*) as qty, role'))
            ->whereNot('role', 'admin')
            ->groupBy('role')
            ->get()
            ->mapWithKeys(function ($item, $key) {
                return [$item->role => $item->qty];
            });
        return view('user.index', [
            'users' => $users,
            'roleStats' => $roleStats,
            'statusStats' => $statusStats,
            'size' => $filter->getSize()
        ]);
    }

    /**
     * @param UserFilterRequest $request
     * @return Application|Factory|View
     * @throws BindingResolutionException
     */
    public function appraisers(UserFilterRequest $request): Application|Factory|View
    {
        $data = $request->validated();
        $data['role'] = RoleEnum::APPRAISER->name;
        $filter = app()->make(UserFilter::class, ['queryParams' => array_filter($data)]);

        $users = User::filter($filter);
        $appraisers = User::whereRole(RoleEnum::APPRAISER->name)->get();
        return view('user.appraisers', [
            'users' => $users,
            'size' => $filter->getSize(),
            'appraisers' => $appraisers
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     * @throws BindingResolutionException
     */
    public function appraisersBonus(UserFilterRequest $request): Application|Factory|View
    {
        $data = $request->validated();
        $filter = app()->make(UserFilter::class, ['queryParams' => array_filter($data)]);
        $bonuses = DB::table('bonuses')
            ->selectRaw("number, order_id, amount, cast(auto_orders.customer_first_name || ' ' || auto_orders.customer_last_name || ' ' || auto_orders.customer_patronymic as text) as customer")
            ->join('auto_orders', 'bonuses.order_id', '=', 'auto_orders.id')
            ->where('appraiser_id', $filter->getAppraiserId())
            ->paginate($filter->getSize());

        $appraiser = (new User)
            ->select(['id', 'name'])
            ->where('id', $filter->getAppraiserId())
            ->limit(1)
            ->first();

        return view('user.appraisers-bonus', [
            'bonuses' => $bonuses,
            'size' => $filter->getSize(),
            'appraiser' => $appraiser
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $validated = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'avatar' => ['required', 'image', 'mimes:jpg,jpeg,png', 'max:1024'],
        ]);

        if (request()->has('avatar')) {
            $avatar = request()->file('avatar');
            $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
            $avatarPath = storage_path('images/');
            $avatar->move($avatarPath, $avatarName);
        }

        $user = (new User)->create([
            'name' => $validated['name'],
            'phone' => $validated['phone'],
            'password' => Hash::make($validated['password']),
            'avatar' => $avatarName
        ]);

        UserInfo::createDefault($user);

        return Redirect::back();
    }

    /**
     * Display the specified resource.
     *
     * @param User $id
     * @return Application|Factory|View
     */
    public function show(User $id): Application|Factory|View
    {
//        $user = User::find($id);
        return view('user.show', ['user' => $id]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|Factory|View
     */
    public function edit(): View
    {
        return view('user.profile-edit', ['user' => Auth::user()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     * @throws ValidationException
     */
    public function update(Request $request): RedirectResponse
    {
        $validated = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'string', 'max:255'],
            'avatar' => ['image', 'mimes:jpg,jpeg,png', 'max:1024'],
        ])->validate();

        Log::debug("validated data", $validated);
        $update = [
            'name' => $validated['name'],
            'phone' => $validated['phone'],
        ];
        $user = User::find($request->input('user_id'));
        request()->whenHas('avatar', function ($avatar) use ($user) {
            $avatarName = time() . '.';
            $avatarName .= $avatar->getClientOriginalExtension();
            $avatar->storeAs('public/images/users', $avatarName);
            $user->avatar = 'storage/images/users/' . $avatarName;
            Log::debug("avatar name ", [$avatarName, $user->avatar]);
        });

        request()->whenHas('password', function ($password) use ($user) {
            if ($password != null) {
                $user->password = Hash::make($password);
            }
        });

        $user->update($update);
        return Redirect::back();
    }

    /**
     * Activate user by given id
     *
     * @param integer $id
     * @return RedirectResponse
     */
    public function activate(int $id): RedirectResponse
    {
        (new User)->where('id', $id)
            ->update(['status' => 'active']);

        return Redirect::back();
    }

    /**
     * Set blocked user by given id
     *
     * @param integer $id
     * @return RedirectResponse
     */
    public function block(int $id): RedirectResponse
    {
        (new User)->where('id', $id)
            ->update(['status' => 'inactive']);

        return Redirect::back();
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     */
    public function updatePassword(Request $request): RedirectResponse
    {
//        $data = $request->validated();
//        (new User)->where('id', auth()->user()->id)
//            ->update(['password' => Hash::make($data['password'])]);
        # Validation
        $request->validate([
            'old_password' => 'required',
            'password' => 'required|confirmed',
        ]);

        #Match The Old Password
        if(!Hash::check($request->old_password, auth()->user()->password)){
            return back()->with("error", "Old Password Doesn't match!");
        }

        #Update the new Password
        User::whereId(auth()->user()->id)->update([
            'password' => Hash::make($request->password)
        ]);

        return Redirect::back();
    }

    public function destroy(int $id): RedirectResponse
    {
        User::destroy($id);
        return Redirect::back();
    }
}
