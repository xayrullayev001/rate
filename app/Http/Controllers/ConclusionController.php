<?php

namespace App\Http\Controllers;

use App\Enums\MediaTypeEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\OrderTypeEnum;
use App\Http\Requests\ConclusionRejectRequest;
use App\Http\Requests\ConclusionRequest;
use App\Models\AutoOrder;
use App\Models\Certificate;
use App\Models\Debit;
use App\Models\EstateOrder;
use App\Models\File;
use App\Models\TrackingActions;
use App\Payload\ActionPayload;
use App\Pdf\Pdf;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class ConclusionController extends Controller
{

    /**
     * @param int $orderId
     * @param string $type
     * @return Factory|View|Application
     */
    public function create(int $orderId, string $type): Factory|View|Application
    {
        $order = null;
        if (OrderTypeEnum::AUTO->name == $type) {
            $order = AutoOrder::findOrFail($orderId);
        } else {
            $order = EstateOrder::findOrFail($orderId);
        }

        return view('conclusion.add', ['order' => $order, 'type' => $type]);
    }

    /**
     * Undocumented function
     *
     * @param ConclusionRequest $request
     * @return RedirectResponse
     */
    public function store(ConclusionRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $fileUploaded = $request->file('file');

        $file = new File();
        $file->id = Str::uuid();
        $file->extension = $fileUploaded->clientExtension();
        $file->order_id = $data['order_id'];
        $file->type = MediaTypeEnum::CONCLUSION->name;
        $file->order_type = $data['order_type'];
        $file->name = $fileUploaded->getClientOriginalName();
        $file->size = File::getFileSizeInMB($fileUploaded->getSize());

        $destinationPath = 'attachments/' . strtolower($data['order_type']).'_' . $data['order_id'];
        $fileNewName = $file->id . '.' . $file->extension;
        $file->path = $fileUploaded->storeAs($destinationPath, $fileNewName, 'public');
        $file->save();

        $order = ($data['order_type'] == OrderTypeEnum::AUTO->name) ? AutoOrder::findOrFail($data['order_id']) : EstateOrder::findOrFail($data['order_id']);
        // Retrieve the currently authenticated user's ID..
        $authenticatedUser = Auth::user();
        $trackingActionPayload = new ActionPayload();
        $trackingActionPayload->setUserId($authenticatedUser->id);
        $trackingActionPayload->setName($authenticatedUser->getName());
        $trackingActionPayload->setAvatar($authenticatedUser->getAvatar());
        $trackingActionPayload->setOrderId($order->id);
        $trackingActionPayload->setOrderType($data['order_type']);
        $trackingActionPayload->addAttachment("{$order->number}");
        $trackingActionPayload->addAttachment("{$file->object_price}");
        $trackingActionPayload->addAttachment("{$file->id}");
        $trackingActionPayload->addAttachment($file->name);
        $trackingActionPayload->addAttachment($file->path);
        $trackingActionPayload->addAttachment("{$file->size}");
        TrackingActions::actionConclusionAdd($trackingActionPayload);

        if ($data['is_diller'] == 'yes') {
            Debit::create([
                'appraiser_id' => $authenticatedUser->id,
                'order_id' => $order->id,
                'order_type' => $data['order_type'],
                'cost' => $order->cost,
                'customer' => $order->customer,
                'expired' => $data['expired']
            ]);
        } else if ($data['is_diller'] == 'no') {
            $order->object_price = $data['object_price'];
            $order->price = $data['price'];
            $order->price_note = $data['note'];
        }
        $order->status = OrderStatusEnum::FINISHED->name;
        $order->update();

        $qrCodePath = 'public/' . $destinationPath . '/qr.png';

        //save conclusion file
        $conclusionFile = Storage::path('public/' . $destinationPath . '/' . $fileNewName);
        Log::debug("Conclusion saved path ", [$conclusionFile]);

        // concatenate pdf files
        $pdf = new Pdf();
        $pdf->setQrPath(Storage::path($qrCodePath));
        $certificates = Certificate::whereIn('type', [
            MediaTypeEnum::APPRAISER_CERTIFICATE->name,
            MediaTypeEnum::INSURANCE_POLICY->name,
            MediaTypeEnum::PARTICIPATE_CERTIFICATE->name,
            MediaTypeEnum::CERTIFICATE->name
        ])->latest('sort')
            ->get();
        Log::debug("standard certificates", [$certificates]);
        $files = [$conclusionFile];
        if (count($certificates) > 0) {
            $files = array_merge($files, [
                Storage::path($certificates[0]->path),
                Storage::path($certificates[1]->path),
                Storage::path($certificates[2]->path),
                Storage::path($certificates[3]->path)
            ]);
        }
        $pdf->setFiles($files);
        $pdf->concat();
        $pdf->Output('F', $conclusionFile);

        /*if (file_exists($conclusionFile)) {
            $pdf = new Pdf();
            $pdf->setQrPath(Storage::path($qrCodePath));
            $pageCount = $pdf->setSourceFile(StreamReader::createByFile($conclusionFile));
            for ($i = 1; $i <= $pageCount; $i++) {
                $tpl = $pdf->importPage($i);
                $size = $pdf->getTemplateSize($tpl);
                $pdf->addPage();
                $pdf->useTemplate($tpl, 1, 1, $size['width'], $size['height']);
            }

            $pdf->Output('F', $conclusionFile);
        }*/
        if (OrderTypeEnum::AUTO->name == $data['order_type'])
            return redirect()->route('auto.index');
        else
            return redirect()->route('estate.index');
    }


    /**
     * @param ConclusionRejectRequest $request
     * @return RedirectResponse
     */
    public function reject(ConclusionRejectRequest $request): RedirectResponse
    {
        $data = $request->validated();
        Log::debug("Reject request payload", [$request]);
        $order = ($data['order_type'] == OrderTypeEnum::AUTO->name) ? AutoOrder::findOrFail($data['order_id']) : EstateOrder::findOrFail($data['order_id']);
        if (OrderStatusEnum::FINISHED->name === $order->status) {
            $order->update([
                'status' => OrderStatusEnum::REJECTED->name
            ]);
            $authenticatedUser = Auth::user();
            $payload = new ActionPayload();
            $payload->setUserId($authenticatedUser->id);
            $payload->setName($authenticatedUser->getName());
            $payload->setAvatar($authenticatedUser->getAvatar());
            $payload->setOrderId($order->id);
            $payload->setOrderType($data['order_type']);
            $payload->addAttachment("{$order->number}");
            $payload->addAttachment("{$data['note']}");
            TrackingActions::actionConclusionReject($payload);
            return redirect()->back();
        } else
            throw new BadRequestException("Order not finished");
    }
}
