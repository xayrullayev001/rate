<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected string $redirectTo = 'user';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'phone' => ['required', 'unique:users', 'string', 'max:255'],
            'password' => ['required', 'string', 'min:4', 'confirmed'],
            'password_confirmation' => ['required', 'string', 'min:4'],
            'avatar' => ['required', 'image', 'mimes:jpg,jpeg,png', 'max:1024'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return User
     */
    protected function create(array $data): User
    {
        // return request()->file('avatar');
//        if (request()->has('avatar')) {
//            $avatar = request()->file('avatar');
//            $avatarName = time() . '.' . $avatar->getClientOriginalExtension();
//            $avatarPath = storage_path('images/');
//            $avatar->move($avatarPath, $avatarName);
//        }
//        request()->whenHas('avatar', function ($avatar) use ($data) {
        $avatarName = time() . '.';
        $avatarName .= $data['avatar']->getClientOriginalExtension();
        $data['avatar']->storeAs('public/images/users', $avatarName);
        $data['avatar'] = 'storage/images/users/' . $avatarName;
        Log::debug("avatar name ", [$avatarName, $data['avatar']]);
//        });
//        dd($data);
        return User::create([
            'name' => $data['name'],
            'phone' => $data['phone'],
            'password' => Hash::make($data['password']),
            'avatar' => $data['avatar']
        ]);
    }
}
