<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Filters\UserFilter;
use App\Http\Requests\UserFilterRequest;
use App\Models\User;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\View\View;

class DillerController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param UserFilterRequest $request
     * @return View
     * @throws BindingResolutionException
     */
    public function __invoke(UserFilterRequest $request): View
    {
        $data = $request->validated();
        $data['role'] = RoleEnum::DILLER->name;
        $filter = app()->make(UserFilter::class, ['queryParams' => array_filter($data)]);
        $dillers = User::filter($filter);

        return view('diller.index', [
            'dillers' => $dillers,
            'size' => $filter->getSize()
        ]);
    }
}
