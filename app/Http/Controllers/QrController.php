<?php

namespace App\Http\Controllers;

use App\Enums\MediaTypeEnum;
use App\Enums\OrderTypeEnum;
use App\Models\AutoOrder;
use App\Models\EstateOrder;
use App\Models\File;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Storage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QrController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param string $content
     * @return Application|Factory|View
     */
    public function show(string $content): Application|Factory|View
    {
        $content = explode(":", base64_decode($content));

            if (OrderTypeEnum::AUTO->name == $content[1]) {
                $order = AutoOrder::findOrFail($content[0]);
                $conclusions = File::where(['order_id' => $order->id, 'type' => MediaTypeEnum::CONCLUSION->name, 'order_type' => OrderTypeEnum::AUTO->name])->orderBy('created_at','DESC')->first();
                return view('qr.auto', ['order' => $order, 'conclusion' => $conclusions]);
            } else {
                $order = EstateOrder::findOrFail($content[0]);
                $conclusions = File::where(['order_id' => $order->id, 'type' => MediaTypeEnum::CONCLUSION->name, 'order_type' => OrderTypeEnum::ESTATE->name])->orderBy('created_at','DESC')->first();
                return view('qr.estate', ['order' => $order, 'conclusion' => $conclusions]);
            }

    }

    /**
     * @param int $id
     * @param string $type
     * @return RedirectResponse
     */
    public function reGenerate(int $id, string $type): RedirectResponse
    {
        $order = (OrderTypeEnum::AUTO->name == $type) ? AutoOrder::findOrFail($id) : EstateOrder::findOrFail($id);
        $destinationPath = 'attachments/' . strtolower($type) . '_' . $order->id;
        $qrCodePath = 'public/' . $destinationPath . '/qr.png';
        $qrCodeContent = route('qr.show', base64_encode("{$id}:{$type}"));
        $qrCode = QrCode::format('png')
            ->style('round')
            ->color(0, 0, 0)
            ->size(80)
            ->generate($qrCodeContent);
        Storage::put($qrCodePath, $qrCode);
        return \Redirect::back();
    }
}
