<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Filters\DebitFilter;
use App\Http\Requests\DebitFilterRequest;
use App\Models\Debit;
use App\Models\User;
use Auth;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\View\View;

class DebitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param DebitFilterRequest $request
     * @return View
     * @throws BindingResolutionException
     */
    public function index(DebitFilterRequest $request): View
    {
        $data = $request->validated();
        if (Auth::user()->role == RoleEnum::DILLER->name) {
            $data['diller'] = Auth::id();
        }
        if (Auth::user()->role == RoleEnum::APPRAISER->name) {
            $data['appraiser'] = Auth::id();
        }
        $appraisers = User::where('role', RoleEnum::APPRAISER->name)->get();
        $filter = app()->make(DebitFilter::class, ['queryParams' => array_filter($data)]);
        Log::debug("Validated data {0}, Filter {1}", [$data, $filter]);

        $debits = Debit::filter($filter);
        return view('debit.index', [
            'debits' => $debits,
            'filters' => $request->all(),
            'appraisers' => $appraisers,
            'size' => $filter->getSize()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function update(int $id): RedirectResponse
    {
        Debit::whereKey($id)
            ->update([
                'status' => Debit::$PAID
            ]);
        return Redirect::back();
    }
}
