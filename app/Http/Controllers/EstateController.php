<?php

namespace App\Http\Controllers;

use App\Enums\MediaTypeEnum;
use App\Enums\OrderStatusEnum;
use App\Enums\OrderTypeEnum;
use App\Enums\RoleEnum;
use App\Http\Filters\EstateFilter;
use App\Http\Filters\EstateStatsFilter;
use App\Http\Requests\Estate\CreateRequest;
use App\Http\Requests\Estate\UpdateRequest;
use App\Http\Requests\EstateFilterRequest;
use App\Models\AutoOrder;
use App\Models\Certificate;
use App\Models\District;
use App\Models\EstateOrder;
use App\Models\File;
use App\Models\OrderMembers;
use App\Models\Purpose;
use App\Models\Region;
use App\Models\TrackingActions;
use App\Models\User;
use App\Payload\ActionPayload;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\View\View;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class EstateController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @param EstateFilterRequest $request
     * @return View
     * @throws BindingResolutionException
     */
    public function index(EstateFilterRequest $request): View
    {
        Log::debug("Request {0}", [$request]);
        $data = $request->validated();
        if (Auth::user()->role == RoleEnum::DILLER->name) {
            $data['diller'] = Auth::id();
        }
        $filter = app()->make(EstateFilter::class, ['queryParams' => array_filter($data)]);
        Log::debug("Validated data {0}, Filter {1}", [$data, $filter]);
        $orders = EstateOrder::filter($filter);

        $data['groupBy'] = 'status';
        $filterWithoutPage = app()->make(EstateStatsFilter::class, ['queryParams' => array_filter($data)]);
        $stats = EstateOrder::selectRaw("count(*) as qty, status")
            ->stats($filterWithoutPage)
            ->mapWithKeys(function ($item, $key) {
                return [$item->status => $item->qty];
            });

        $purposes = Purpose::all();
        return view('estate.index', [
            'orders' => $orders,
            'stats' => $stats,
            'filters' => $request->all(),
            'size' => $filter->getSize(),
            'purposeCases' => $purposes
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $dillers = User::whereRole(RoleEnum::DILLER->name)
            ->get();
        $purposes = Purpose::all();
        $districts = District::all();
        $regions = Region::all();
        return view('estate.create', [
            'dillers' => $dillers,
            'purposeCases' => $purposes,
            'regions' => $regions,
            'districts' => $districts
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateRequest $request
     * @return Application|Redirector|RedirectResponse
     */
    public function store(CreateRequest $request): Redirector|RedirectResponse|Application
    {

        $data = $request->validated();

        $order = EstateOrder::create($data);
        $destinationPath = 'attachments/estate_' . $order->id;
        $qrCodePath = 'public/' . $destinationPath . '/qr.png';
        $qrCodeContent = route('qr.show', base64_encode("{$order->id}:" . OrderTypeEnum::AUTO->name));
        $qrCode = QrCode::format('png')
            ->style('round')
            ->color(0, 0, 0)
            ->size(80)
            ->generate($qrCodeContent);
        Log::debug($qrCodePath);
        Storage::put($qrCodePath, $qrCode);

        $authenticatedUser = Auth::user(); // Retrieve the currently authenticated user's ID..
        $trackingActionPayload = new  ActionPayload();
        $trackingActionPayload->setAvatar($authenticatedUser->getAvatar());
        $trackingActionPayload->setName($authenticatedUser->getName());
        $trackingActionPayload->setOrderId($order->id);
        $trackingActionPayload->setOrderType(OrderTypeEnum::ESTATE->name);
        $trackingActionPayload->setUserId($authenticatedUser->id);
        $trackingActionPayload->addAttachment("{$order->number}");
        TrackingActions::actionCreated($trackingActionPayload);

        return redirect('estate');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function show(EstateOrder $order): View
    {
        $appraisers = (new User)->where(['role' => RoleEnum::APPRAISER->name, 'status' => 'active'])->get();
        $orderMembers = OrderMembers::select(['user_id'])->where(['order_id' => $order->id, 'order_type' => OrderTypeEnum::ESTATE->name])->pluck('user_id');
        $members = (new User)->whereIn('id', $orderMembers)->get();
        $files = File::where(['order_id' => $order->id, 'type' => MediaTypeEnum::ADDITIONAL->name, 'order_type' => OrderTypeEnum::ESTATE->name])->limit(5)->get();
        $conclusions = File::where(['order_id' => $order->id, 'type' => MediaTypeEnum::CONCLUSION->name, 'order_type' => OrderTypeEnum::ESTATE->name])->get();
        $isAppraisers = $orderMembers->contains(Auth::user()->id);
        return view('estate.show', [
            'order' => $order,
            'appraisers' => $appraisers,
            'members' => $members,
            'conclusions' => $conclusions,
            'files' => $files,
            'isAppraisers' => $isAppraisers,
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param EstateOrder $order
     * @param Request $request
     * @return View
     */
    public function showDocuments(EstateOrder $order, Request $request): View
    {
        $type = $request->query('type', 'ALL');

        $files = File::where(['order_id' => $order->id, 'order_type' => OrderTypeEnum::ESTATE->name]);
        $certificate = null;
        switch ($type) {
            case $type == MediaTypeEnum::APPRAISER_CERTIFICATE->name:
            case $type == MediaTypeEnum::INSURANCE_POLICY->name:
            case $type == MediaTypeEnum::PARTICIPATE_CERTIFICATE->name:
            case $type == MediaTypeEnum::CERTIFICATE->name:
                $certificate = Certificate::where(['type' => $type])->get();
                break;
            case $type != MediaTypeEnum::ALL->name:
                $files->where('type', $type);
                break;
        }

        $countDocs = clone $files;
        $countMediaFiles = clone $files;
        $sizeInStorageFiles = clone $files;
        return view('estate.show-documents', [
            'order' => $order,
            'files' => $files->latest()->paginate(12),
            'type' => $type,
            'countDocs' => $countDocs->count(),
            'countMediaFiles' => $countMediaFiles->whereIn('extension', ['jpeg', 'jpg', 'png'])->count(),
            'otherFiles' => $countMediaFiles->whereNotIn('extension', ['jpeg', 'jpg', 'png'])->count(),
            'sizeInStorage' => $sizeInStorageFiles->whereIn('extension', ['jpeg', 'jpg', 'png'])->sum('size_in_bytes'),
            'certificates' => $certificate
        ]);
    }

    /**
     * @param AutoOrder $order
     * @return View
     */
    public function showAppCertificate(AutoOrder $order): View
    {
        $certificate = Certificate::where([
            'type' => MediaTypeEnum::APPRAISER_CERTIFICATE
        ])->get();
        return view('estate.show-documents-app-certificate', [
            'certificate' => $certificate,
            'order' => $order
        ]);
    }

    /**
     * @param AutoOrder $order
     * @return View
     */
    public function showInsurance(AutoOrder $order): View
    {
        $certificate = Certificate::where([
            'type' => MediaTypeEnum::INSURANCE_POLICY
        ])->get();
        return view('estate.show-documents-insurance', [
            'certificate' => $certificate,
            'order' => $order
        ]);
    }

    /**
     * @param AutoOrder $order
     * @return View
     */
    public function showParticipate(AutoOrder $order): View
    {
        $certificate = Certificate::where([
            'type' => MediaTypeEnum::PARTICIPATE_CERTIFICATE
        ])->get();
        return view('estate.show-documents-part-certificate', [
            'certificate' => $certificate,
            'order' => $order
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function showActivities(int $id): View
    {
        $order = EstateOrder::findOrFail($id);
        $actions = TrackingActions::where(["order_id" => $id, 'order_type' => OrderTypeEnum::ESTATE->name])->orderBy('created_at', 'desc')->get();

        return view('estate.show-activities', [
            'order' => $order,
            'actions' => $actions
        ]);
    }

    /**
     * @param int $id
     * @return View
     */
    public function showTeam(int $id): View
    {
        $order = EstateOrder::findOrFail($id);
        $appraisers = User::whereRole(RoleEnum::APPRAISER->name)->get();
        $members = DB::select(DB::raw("
        with tmp as (select coalesce(count(m.user_id) filter (where auto_orders.status = 'FINISHED'),0) as finished,
                    coalesce(count(m.user_id) filter (where auto_orders.status = 'STARTED'),0)  as started,
                    coalesce(count(m.user_id) filter (where auto_orders.status = 'APPROVED'),0)  as approved,
                    coalesce(count(m.user_id) filter (where auto_orders.status = 'REJECTED'),0)  as rejected,
                    user_id
             from auto_orders
                      inner join order_members m on auto_orders.id = m.order_id and m.order_type='" . OrderTypeEnum::ESTATE->name . "'
               and auto_orders.id = " . $id . " group by m.user_id)
                select users.name,
                        users.id,
                        users.avatar,
                        users.role,
                       tmp.finished,
                       tmp.started,
                       tmp.approved,
                       tmp.rejected
                from users inner join tmp on tmp.user_id = users.id
                where users.role='" . RoleEnum::APPRAISER->name . "'"));

        return view('estate.show-team', [
            'order' => $order,
            'appraisers' => $appraisers,
            'members' => $members
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return View
     */
    public function edit(int $id): View
    {
        $order = EstateOrder::findOrFail($id);
        $dillers = User::whereRole(RoleEnum::DILLER->name)
            ->get();
        $purposes = Purpose::all();
        $districts = District::all();
        $regions = Region::all();
        return view('estate.edit', [
            'order' => $order,
            'dillers' => $dillers,
            'purposeCases' => $purposes,
            'regions' => $regions,
            'districts' => $districts
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRequest $request
     * @param int $id
     * @return Application|RedirectResponse|Redirector
     */
    public function update(int $id, UpdateRequest $request): Redirector|RedirectResponse|Application
    {

        $data = $request->validated();
        $order = EstateOrder::findOrFail($id);

        $authenticatedUser = Auth::user();
        $updateActionPayload = new ActionPayload();
        $updateActionPayload->setUserId($authenticatedUser->id);
        $updateActionPayload->setAvatar($authenticatedUser->getAvatar());
        $updateActionPayload->setName($authenticatedUser->getName());
        $updateActionPayload->setOrderId($order->id);
        $updateActionPayload->setOrderType(OrderTypeEnum::ESTATE->name);
        $updateActionPayload->addAttachment("{$order->number}");

        foreach ($data as $key => $value) {
            if ($order->{$key} != $data[$key]) {
                $updateActionPayload->addAttachment($order->{$key} . " => " . $value);
                $order->{$key} = $value;
            }
        }
        if (count($updateActionPayload->getAttachments()) > 1) {
            if ($order->status == OrderStatusEnum::REJECTED) {
                $order->status = OrderStatusEnum::FINISHED;
            }
            $order->update();
        }
        TrackingActions::actionUpdate($updateActionPayload);

        return redirect('/estate/show/' . $order->id);
    }

    /**
     * @param Request $request
     * @return Factory|\Illuminate\Contracts\View\View|Application
     */
    public function orderClone(Request $request): Application|Factory|\Illuminate\Contracts\View\View
    {
        $order = EstateOrder::findOrFail($request->id);
        $dillers = User::whereRole(RoleEnum::DILLER->name)
            ->get();
        $purposes = Purpose::all();
        $districts = District::all();
        $regions = Region::all();
        return view('estate.clone', [
            'dillers' => $dillers,
            'purposeCases' => $purposes,
            'regions' => $regions,
            'districts' => $districts,
            'order' => $order
        ]);

    }
}
