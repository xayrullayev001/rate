<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserInfoRequest;
use App\Models\UserInfo;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;

class UserInfoController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return Application|Factory|View
     */
    public function edit(): View|Factory|Application
    {
        $userInfo = UserInfo::where('user_id', Auth::id())->first();
        return view('user-info.edit', [
            'info' => $userInfo
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserInfoRequest $request
     * @return RedirectResponse
     */
    public function update(UserInfoRequest $request): RedirectResponse
    {
        $data = $request->validated();
        $data['completed'] = count($data) * (100/8);
        UserInfo::where('user_id', Auth::id())->update($data);
        Session::flash('message', 'translation.user.info.updated');
        Session::flash('alert-class', 'alert-success');
        return Redirect::back();
    }
}
