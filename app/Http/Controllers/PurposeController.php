<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePurposeRequest;
use App\Http\Requests\UpdatePurposeRequest;
use App\Models\Purpose;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Redirect;

class PurposeController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return Factory|View|Application
     */
    public function index(): Factory|View|Application
    {
        return view("purposes.index", [
            'purposes' => Purpose::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view("purposes.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StorePurposeRequest $request
     * @return RedirectResponse
     */
    public function store(StorePurposeRequest $request): RedirectResponse
    {
        Purpose::create($request->validated());
        return Redirect::to('purposes');
    }

    /**
     * Display the specified resource.
     *
     * @param Purpose $purpose
     * @return Application|Factory|View
     */
    public function show(Purpose $purpose): Application|Factory|View
    {
        return view('purposes.show', ['purpose' => $purpose]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Purpose $purpose
     * @return Application|Factory|View
     */
    public function edit(Purpose $purpose): Application|Factory|View
    {
        return view('purposes.edit', ['purpose' => $purpose]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdatePurposeRequest $request
     * @param Purpose $purpose
     * @return RedirectResponse
     */
    public function update(UpdatePurposeRequest $request, Purpose $purpose): RedirectResponse
    {
        $purpose->update($request->validated());
        return Redirect::route('purposes.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Purpose $purpose
     * @return RedirectResponse
     */
    public function destroy(Purpose $purpose): RedirectResponse
    {
        $purpose->delete();
        return Redirect::route('purposes.index');
    }
}
