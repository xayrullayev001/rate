<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatusEnum;
use App\Enums\OrderTypeEnum;
use App\Enums\TrackingActionTypeEnum;
use App\Http\Requests\MemberRemoveRequest;
use App\Http\Requests\MembersInviteRequest;
use App\Models\AutoOrder;
use App\Models\EstateOrder;
use App\Models\OrderMembers;
use App\Models\TrackingActions;
use App\Models\User;
use App\Payload\ActionPayload;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class OrderMembersController extends Controller
{
    /**
     * Remove the specified resource from storage.
     *
     * @param MembersInviteRequest $request
     * @return RedirectResponse
     */
    public function invite(MembersInviteRequest $request): RedirectResponse
    {
        $content = $request->post();
        \Log::debug("content", [$content]);
        $orderId = $content['order_id'];
        $orderType = $content['order_type'];
        unset($content['order_id'], $content['_token']);

        $authenticatedUser = Auth::user();
        $actionPayload = new ActionPayload();
        $actionPayload->setAvatar($authenticatedUser->getAvatar());
        $actionPayload->setName($authenticatedUser->getName());
        $actionPayload->setUserId($authenticatedUser->id);
        $actionPayload->setOrderId($orderId);
        $actionPayload->setOrderType($orderType);
        $track = false;
        foreach ($content as $key => $value) {
            if (is_int($key)) {
                $ifDoesntExist = OrderMembers::where('order_id', $orderId)
                    ->where(['user_id' => $key, 'order_type' => $orderType])
                    ->doesntExist();
                if ($ifDoesntExist) {
                    OrderMembers::create([
                        'order_id' => $orderId,
                        'user_id' => $key,
                        'order_type' => $orderType
                    ]);
                    $member = User::findOrFail($key);
                    $actionPayload->addAttachment([$member->id, $member->name, $member->avatar]);
                    $track = true;
                }
            }
        }
        if ($track) {
            TrackingActions::actionMemberInvite($actionPayload);
        }
        if (OrderTypeEnum::AUTO->name === $orderType) {
            AutoOrder::where('id', $orderId)
                ->update([
                    'status' => OrderStatusEnum::STARTED->name
                ]);
        } else {
            EstateOrder::where('id', $orderId)
                ->update([
                    'status' => OrderStatusEnum::STARTED->name
                ]);
        }
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MemberRemoveRequest $request
     * @return RedirectResponse
     */
    public function remove(MemberRemoveRequest $request): RedirectResponse
    {
        $content = $request->post();
        $id = $content['id'];
        $orderId = $content['order_id'];
        $orderType = $content['order_type'];
        if (OrderTypeEnum::AUTO->name === $orderType) {
            $isApproved = AutoOrder::select('status')
                ->where('id', $orderId)
                ->where('status', OrderStatusEnum::APPROVED->name)
                ->doesntExist();
        } else {
            $isApproved = AutoOrder::select('status')
                ->where('id', $orderId)
                ->where('status', OrderStatusEnum::APPROVED->name)
                ->doesntExist();
        }

        if ($isApproved) {
            OrderMembers::where('order_id', $orderId)
                ->where(['user_id' => $id, 'order_type' => $orderType])
                ->delete();
        }

        $id = Auth::id(); // Retrieve the currently authenticated user's ID..
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::MEMBERS_REMOVED->name,
            'order_id' => $orderId,
            'order_type' => $orderType,
            'user_id' => $id,
            'msg' => TrackingActionTypeEnum::MEMBERS_REMOVED->value,
            'params' => json_encode([$id])
        ]);
        return back();
    }

}
