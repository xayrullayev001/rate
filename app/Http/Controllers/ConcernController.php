<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreConcernRequest;
use App\Http\Requests\UpdateConcernRequest;
use App\Models\Concern;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class ConcernController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): View|Factory|Application
    {
        return view("concerns.index", [
            'concerns' => Concern::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): View|Factory|Application
    {
        return view("concerns.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreConcernRequest $request
     * @return RedirectResponse
     */
    public function store(StoreConcernRequest $request): RedirectResponse
    {
        Concern::create($request->validated());
        return Redirect::to('concerns');
    }

    /**
     * Display the specified resource.
     *
     * @param Concern $concern
     * @return Application|Factory|View
     */
    public function show(Concern $concern): View|Factory|Application
    {
        return view('concerns.show', ['concern' => $concern]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Concern $concern
     * @return Application|Factory|View
     */
    public function edit(Concern $concern): Application|Factory|View
    {
        return view('concerns.edit', [
            'concern' => $concern
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateConcernRequest $request
     * @param Concern $concern
     * @return RedirectResponse
     */
    public function update(UpdateConcernRequest $request, Concern $concern): RedirectResponse
    {
        $concern->update($request->validated());
        return Redirect::route('concerns.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Concern $concern
     * @return RedirectResponse
     */
    public function destroy(Concern $concern): RedirectResponse
    {
        $concern->delete();
        return Redirect::route('concerns.index');
    }
}
