<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreDistrictRequest;
use App\Http\Requests\UpdateDistrictRequest;
use App\Models\District;
use App\Models\Region;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class DistrictController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        return view("districts.index", [
            'districts' => District::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): Application|Factory|View
    {
        return view("districts.create", [
            'regions' => Region::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreDistrictRequest $request
     * @return RedirectResponse
     */
    public function store(StoreDistrictRequest $request): RedirectResponse
    {
        District::create($request->validated());
        return Redirect::to('districts');
    }

    /**
     * Display the specified resource.
     *
     * @param District $district
     * @return Application|Factory|View
     */
    public function show(District $district): View|Factory|Application
    {
        return view('districts.show', ['district' => $district]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param District $district
     * @return Application|Factory|View
     */
    public function edit(District $district): View|Factory|Application
    {
        return view('districts.edit', [
            'district' => $district,
            'regions' => Region::all()
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateDistrictRequest $request
     * @param District $district
     * @return RedirectResponse
     */
    public function update(UpdateDistrictRequest $request, District $district): RedirectResponse
    {
        $district->update($request->validated());
        return Redirect::route('districts.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param District $district
     * @return RedirectResponse
     */
    public function destroy(District $district): RedirectResponse
    {
        $district->delete();
        return Redirect::route('districts.index');
    }
}
