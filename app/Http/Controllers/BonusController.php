<?php

namespace App\Http\Controllers;

use App\Enums\RoleEnum;
use App\Http\Requests\BonusRequest;
use App\Models\Bonus;
use App\Models\TrackingActions;
use App\Models\User;
use App\Payload\ActionPayload;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Psr\Container\ContainerExceptionInterface;
use Psr\Container\NotFoundExceptionInterface;
use function request;

class BonusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     * @throws ContainerExceptionInterface
     * @throws NotFoundExceptionInterface
     */
    public function index(): View
    {
        $size = request()->get('size') ? request()->get('size') : 10;
        $bonuses = Bonus::query()
//            ->select(['number', 'order_id', 'order_type', 'amount', 'appraiser_id', 'customer_first_name', 'customer_last_name', 'customer_patronymic', 'users.name as appraiser'])
//            ->join('auto_orders', 'bonuses.order_id', '=', 'auto_orders.id')
//            ->join('estate_orders', 'bonuses.order_id', '=', 'estate_orders.id')
            ->join('users', 'bonuses.appraiser_id', '=', 'users.id')->where('role', '=', RoleEnum::APPRAISER->name)
            ->paginate($size);

        return view('bonus.index', [
            'bonuses' => $bonuses,
            'size' => $size
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BonusRequest $request
     * @return RedirectResponse
     */
    public function store(BonusRequest $request): RedirectResponse
    {
        Bonus::create($request->validated());
        $user = (new User)->firstWhere('id', '=', $request->input('appraiser_id'));
        $user->update([
            'balance' => $user->balance += $request->input('amount')
        ]);
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function storeCredit(Request $request): RedirectResponse
    {
        $user = (new User)->firstWhere('id', '=', $request->input('appraiser_id'));
        $user->update([
            'balance' => $user->balance -= str_replace(',', '', $request->input('amount'))
        ]);
        $actionPayload = new ActionPayload();
        $actionPayload->setUserId(auth()->id());
        $actionPayload->setOrderId(1);
        $actionPayload->setOrderType($request->input('order_type'));
        $actionPayload->addAttachment(time());
        $actionPayload->addAttachment($request->input('amount'));
        $actionPayload->addAttachment($request->input('note'));
        TrackingActions::actionAppraiserTakenBonus($actionPayload);
        return redirect()->back();
    }
}
