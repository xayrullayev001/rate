<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRegionRequest;
use App\Http\Requests\UpdateRegionRequest;
use App\Models\District;
use App\Models\Region;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Redirect;

class RegionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View
     */
    public function index(): Application|Factory|View
    {
        return view("regions.index", [
            'regions' => Region::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View
     */
    public function create(): Application|Factory|View
    {
        return view("regions.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreRegionRequest $request
     * @return RedirectResponse
     */
    public function store(StoreRegionRequest $request): RedirectResponse
    {
        Region::create($request->validated());
        return Redirect::to('regions');
    }

    /**
     * Display the specified resource.
     *
     * @param Region $region
     * @return Application|Factory|View
     */
    public function show(Region $region): Application|Factory|View
    {
        return view('regions.show', ['region' => $region]);
    }

    /**
     * @param int $id
     * @return Application|Factory|View
     */
    public function districts(int $id): Application|Factory|View
    {
        return view("districts.index", [
            'districts' => District::where(['region_id' => $id])->get()
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Region $region
     * @return Application|Factory|View
     */
    public function edit(Region $region): Application|Factory|View
    {
        return view('regions.edit', ['region' => $region]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateRegionRequest $request
     * @param Region $region
     * @return RedirectResponse
     */
    public function update(UpdateRegionRequest $request, Region $region): RedirectResponse
    {
        $region->update($request->validated());
        return Redirect::route('regions.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Region $region
     * @return RedirectResponse
     */
    public function destroy(Region $region): RedirectResponse
    {
        $region->delete();
        return Redirect::route('regions.index');
    }
}
