<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatusEnum;
use App\Models\AutoOrder;
use App\Models\EstateOrder;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     */
    public function index()
    {
        $topAutoCategories = DB::table('auto_orders')
            ->selectRaw('purposes.' . \app()->getLocale() . ' as purpose, count(auto_orders.id) as qty')
            ->fromRaw('auto_orders')
            ->join('purposes', 'purposes.id', '=', 'auto_orders.purpose_id')
            ->groupBy('purposes.id')
            ->orderBy('qty', 'desc')
            ->get();
        $topEstateCategories = DB::table('estate_orders')
            ->selectRaw('purposes.' . \app()->getLocale() . ' as purpose, count(estate_orders.id) as qty')
            ->fromRaw('estate_orders')
            ->join('purposes', 'purposes.id', '=', 'estate_orders.purpose_id')
            ->groupBy('purposes.id')
            ->orderBy('qty', 'desc')
            ->get();

        $recentOrders = AutoOrder::select()
            ->orderBy('created_at', 'desc')
            ->limit(7)
            ->get();
        $forModerate = AutoOrder::select(["id", "number", "updated_at", "price", "object_price"])
            ->selectRaw("'auto_' as type")
            ->where("status", OrderStatusEnum::FINISHED->name)
            ->orderByDesc('updated_at')
            ->limit(3)
            ->unionAll(
                EstateOrder::select(["id", "number", "updated_at", "price", "object_price"])
                    ->selectRaw("'estate_' as type")
                    ->where("status", OrderStatusEnum::FINISHED->name)
                    ->orderByDesc('updated_at')
                    ->limit(3)
                    ->getQuery()
            )
            ->get();
        $appraiserChart = DB::select(DB::raw("with tmp as (select count(user_id) as qty,user_id, order_id
            from auto_orders
                     inner join order_members om on auto_orders.id = om.order_id
            where status='FINISHED'
            group by om.user_id, om.order_id)
            select
                users.name,
                tmp.qty
                from users
            inner
                join tmp on tmp.user_id=users.id
            where users.status='active' and role='APPRAISER' limit 5"));
//        dd($appraiserChart);
        $labels = [];
        $values = [];
        foreach ($appraiserChart as $chart) {
            array_push($labels, $chart->name);
            array_push($values, $chart->qty);
        }

        $actions = DB::table('tracking_actions')
            ->latest()
            ->limit(8)
            ->get();
        $estateOrders = EstateOrder::count();
        $autoOrders = AutoOrder::count();
        $summaryPrice = EstateOrder::sum('price');
        $summaryPrice += AutoOrder::sum('price');
        return view("index", [
            'topEstateCategories' => $topEstateCategories,
            'topAutoCategories' => $topAutoCategories,
            'appraiserChart' => [$labels, $values],
            'recentOrders' => $recentOrders,
            'forModerate' => $forModerate,
            'actions' => $actions,
            'estateOrders' => $estateOrders,
            'autoOrders' => $autoOrders,
            'summaryPrice' => $summaryPrice,
        ]);
    }

    /**
     * @param Request $request
     * @return View|Factory|Application
     */
    public function pages(Request $request): View|Factory|Application
    {
        if (view()->exists($request->path())) {
            return view($request->path());
        }
        if (view()->exists("pages." . $request->path())) {
            return view("pages." . $request->path());
        }
        return abort(404);
    }

    /*Language Translation*/
    /**
     * @param $locale
     * @return RedirectResponse
     */
    public function lang($locale): RedirectResponse
    {
        if ($locale) {
            App::setLocale($locale);
            Session::put('lang', $locale);
            Session::save();
            return redirect()->back()->with('locale', $locale);
        } else {
            return redirect()->back();
        }
    }


}
