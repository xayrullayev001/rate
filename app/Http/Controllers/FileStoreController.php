<?php

namespace App\Http\Controllers;

use App\Enums\OrderStatusEnum;
use App\Http\Requests\FileUploadRequest;
use App\Models\AutoOrder;
use App\Models\File;
use App\Models\TrackingActions;
use App\Payload\ActionPayload;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Ramsey\Uuid\UuidInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;

class FileStoreController extends Controller
{


    public function storeConclusionFile(Request $request): string
    {
        $fileUploaded = $request->file('file');
        $type = $request->header('Attachment-type', null);
        $orderId = $request->header('Attachment-Order-Id', null);
        $orderPrice = $request->header('Attachment-Order-Price', null);

        $file = new File();
        $file->id = Str::uuid();
        $file->extension = $fileUploaded->clientExtension();
        $file->order_id = $orderId;
        $file->type = $type;
        $file->name = $fileUploaded->getClientOriginalName();
        $file->size = $this->getFileSizeInMB($fileUploaded->getSize());

        $destinationPath = 'public/attachments/' . strtolower($type) . '_' . $orderId;
        $file->path = $fileUploaded->storeAs($destinationPath, $file->id . '.' . $file->extension);
        $file->save();

        Log::debug("Uploaded file path", $file->path);
        if ($type === 'conclusion') {
            $order = AutoOrder::findOrFail($orderId);
            // Retrieve the currently authenticated user's ID..
            $authenticatedUser = Auth::user();
            $trackingActionPayload = new ActionPayload();
            $trackingActionPayload->setUserId($authenticatedUser->id);
            $trackingActionPayload->setName($authenticatedUser->getName());
            $trackingActionPayload->setAvatar($authenticatedUser->getAvatar());
            $trackingActionPayload->setOrderId($order->id);
            $trackingActionPayload->addAttachment("{$order->number}");
            $trackingActionPayload->addAttachment("{$orderPrice}");
            $trackingActionPayload->addAttachment("{$file->id}");
            $trackingActionPayload->addAttachment("{$file->name}");
            $trackingActionPayload->addAttachment("{$file->path}");
            $trackingActionPayload->addAttachment("{$file->size}");
            TrackingActions::actionConclusionAdd($trackingActionPayload);
            $order->update([
                'status' => OrderStatusEnum::FINISHED->name,
                'price' => $orderPrice
            ]);
            return route('auto.edit', ['id' => $orderId]);
        }
        return $file->id;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function attachFiles(FileUploadRequest $request): string
    {
        // Log::debug($request->all());
        $data = $request->validated();
        $fileUploaded = $data['file'];
        $type = $data['file_type'];
        $orderId = $data['order_id'];
        $orderType = $data['order_type'];

        // Log::debug('Files ', [$fileUploaded->getSize(), $fileUploaded->getClientOriginalName()]);

        $file = new File();
        $file->id = Str::uuid();
        $file->extension = $fileUploaded->clientExtension();
        $file->order_id = $orderId;
        $file->type = $type;
        $file->order_type = $orderType;
        $file->name = $fileUploaded->getClientOriginalName();
        $file->size = File::getFileSizeInMB($fileUploaded->getSize());
        $file->size_in_bytes = $fileUploaded->getSize();

        $destinationPath = 'attachments/' . strtolower($orderType) . '_' . $orderId;
        $file->path = $fileUploaded->storeAs($destinationPath, $file->id . '.' . $file->extension, ['disk' => 'public']);
        Log::debug($file->path);
        $file->save();
        // $order = AutoOrder::findOrFail($orderId);
        // // Retrieve the currently authenticated user's ID..
        // $authenticatedUser = User::findOrFail(Auth::id());
        // $trackingActionPayload = new ActionPayload();
        // $trackingActionPayload->setUserId($authenticatedUser->id);
        // $trackingActionPayload->setName($authenticatedUser->getName());
        // $trackingActionPayload->setAvatar($authenticatedUser->getAvatar());
        // $trackingActionPayload->setOrderId($order->id);
        // $trackingActionPayload->addAttachment("{$order->prefix}-{$order->order_number}");
        // $trackingActionPayload->addAttachment("{$file->id}");
        // $trackingActionPayload->addAttachment("{$file->name}");
        // $trackingActionPayload->addAttachment("{$file->path}");
        // $trackingActionPayload->addAttachment("{$file->size}");
        // TrackingActions::actionAttach($trackingActionPayload);

        return $file->id;
    }

    /**
     * @param Request $request
     * @return UuidInterface|string
     */
    public function attachProfileFiles(Request $request): UuidInterface|string
    {
        Log::debug($request);
        $data = $request->all();

        $image = $request->image;  // your base64 encoded
        $image = str_replace('data:image/jpeg;base64,', '', $request->file('file'));
        $image = str_replace(' ', '+', $image);
        $imageName = Str::uuid() . '.' . 'jpg';
        Storage::put($imageName, base64_decode($image));

        $fileUploaded = $request->file('file');
        $type = $request->input('file_type');
        $orderId = $request->input('user_id');

        $file = new File();
        $file->id = Str::uuid();
        $file->extension = $fileUploaded->clientExtension();
        $file->order_id = $orderId;
        $file->type = $type;
        $file->name = $fileUploaded->getClientOriginalName();
        $file->size = File::getFileSizeInMB($fileUploaded->getSize());
        $file->size_in_bytes = $fileUploaded->getSize();

        $destinationPath = 'profile/' . $orderId;
        $file->path = $fileUploaded->storeAs($destinationPath, $file->id . '.' . $file->extension, ['disk' => 'public']);
        Log::debug($file->path);
        $file->save();

        return $file->id;
    }

    /**
     * Undocumented function
     *
     * @param int $file
     * @return BinaryFileResponse
     */
    public function downloadFile(File $file): BinaryFileResponse
    {
        Log::debug("storage/" . $file->path);
        /**this will force download your file**/
        return Response::download("storage/" . $file->path, $file->name);
    }

    /**
     * @param File $file
     * @return RedirectResponse
     */
    public function delete(File $file): RedirectResponse
    {
        $file->delete();
        return Redirect::back();
    }
}
