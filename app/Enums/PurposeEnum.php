<?php

namespace App\Enums;

enum PurposeEnum: string
{
    case CREDIT = 'translation.credit';//'Для обеспечения кредита залогом';
    case REALIZE = 'translation.realize';//'Для реализации';
    case INVEST = 'translation.invest';//'Для вклада в уставный капитал';
    case NOTARIAL = 'translation.notarial';//'Для нотариального оформления';
    case CONSULT = 'translation.consult';//'Для консультации';

    public static function getLabel(string $value): string
    {
        return match ($value) {
            'CREDIT' => 'translation.credit',//'Для обеспечения кредита залогом',
            'REALIZE' => 'translation.realize',//'Для реализации',
            'INVEST' => 'translation.invest',//'Для вклада в уставный капитал',
            'NOTARIAL' => 'translation.notarial',//'Для нотариального оформления'
            'CONSULT' => 'translation.consult',//'Для консультации'
        };
    }
}
