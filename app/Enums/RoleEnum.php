<?php

namespace App\Enums;

enum RoleEnum: string
{
    case USER = 'translation.role-user';
    case APPRAISER = 'translation.role-appraiser';
    case DILLER = 'translation.role-diller';
    case MANAGER = 'translation.role-manager';
}
