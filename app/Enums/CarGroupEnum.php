<?php

namespace App\Enums;

enum CarGroupEnum: string
{
    case LOCAL = 'АВТОМОБИЛИ ОТЕЧЕСТВЕННЫЕ';
    case IMPORT = 'АВТОМОБИЛИ ИНОСТРАННЫЕ';
    case SPECIAL = 'СПЕЦИАЛЬНАЯ ТЕХНИКА';


    public static function getLabel(string $value): string
    {
        return match ($value) {
            'LOCAL' => 'АВТОМОБИЛИ ОТЕЧЕСТВЕННЫЕ',
            'IMPORT' => 'АВТОМОБИЛИ ИНОСТРАННЫЕ',
            'SPECIAL' => 'СПЕЦИАЛЬНАЯ ТЕХНИКА',
        };
    }

    public static function getCategoryLabel(string $labelKey): string
    {

        $labels = [
            'local1' => 'Легковые автомобили',
            'local2' => 'Грузовые автомобили',
            'local3' => 'Автобусы',
            'local4' => 'Прицепы',
            'local5' => 'Мотоциклы',
            'local6' => 'Прочая техника',
            'import1' => 'Легковые автомобили',
            'import2' => 'Грузовые автомобили',
            'import3' => 'Автобусы',
            'import4' => 'Прицепы',
            'import5' => 'Мотоциклы',
            'import6' => 'Прочая техника',
            'special1' => 'Автогрейдеры',
            'special2' => 'Погрузчики фронтальные одноковшовые',
            'special3' => 'екскаваторы одноковшовые',
            'special4' => 'Промышленный трактор Caterpillar D6',
            'special5' => 'Промышленный трактор Komatsu D65',
            'special6' => 'Отечественные сельскохозяйственные трактора',
            'special7' => 'Импортные сельскохозяйственные трактора (80-100 л.с.)',
            'special8' => 'Прочие импортные трактора',
            'special9' => 'Прочая дорожно-строительная техника',
        ];

        return $labels[$labelKey];
    }

    public static function getCategories(): array
    {
        return [
            self::LOCAL->name => [
                'local1' => 'Легковые автомобили',
                'local2' => 'Грузовые автомобили',
                'local3' => 'Автобусы',
                'local4' => 'Прицепы',
                'local5' => 'Мотоциклы',
                'local6' => 'Прочая техника',

            ],
            self::IMPORT->name => [
                'import1' => 'Легковые автомобили',
                'import2' => 'Грузовые автомобили',
                'import3' => 'Автобусы',
                'import4' => 'Прицепы',
                'import5' => 'Мотоциклы',
                'import6' => 'Прочая техника',
            ],
            self::SPECIAL->name => [
                'special1' => 'Автогрейдеры',
                'special2' => 'Погрузчики фронтальные одноковшовые',
                'special3' => 'екскаваторы одноковшовые',
                'special4' => 'Промышленный трактор Caterpillar D6',
                'special5' => 'Промышленный трактор Komatsu D65',
                'special6' => 'Отечественные сельскохозяйственные трактора',
                'special7' => 'Импортные сельскохозяйственные трактора (80-100 л.с.)',
                'special8' => 'Прочие импортные трактора',
                'special9' => 'Прочая дорожно-строительная техника',
            ],
        ];
    }
}
