<?php

namespace App\Enums;

enum LanguagesEnum : string
{
    case UZ = "uz";
    case RU = "ru";
    case CR = "cr";
}
