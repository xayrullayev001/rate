<?php

namespace App\Enums;

enum TrackingActionTypeEnum: string
{
    case ORDER_CREATED = "translation.order-created";
    case ORDER_UPDATED = "translation.order-updated";
    case MEMBERS_ADDED = "translation.members-added";
    case ATTACHMENTS = "translation.adding-new-attachments";
    case MEMBERS_REMOVED = "translation.members-removed";
    case IN_PROGRESS = "translation.in-progress";
    case COMPLETED = "translation.conclusion-completed";
    case REJECTED = "translation.conclusion-rejected";
    case APPROVED = "translation.conclusion-approved";
    case TAKEN_BONUS = "translation.appraiser-taken-bonus";
}
