<?php

namespace App\Enums;

enum OrderStatusEnum: string
{
    case DRAFT = 'translation.created';//'Yaratildi';
    case STARTED = 'translation.started';//'Baxolovchi biriktirildi';
    case FINISHED = 'translation.finished';//'Baxolandi';
    case APPROVED = 'translation.approved';//'Tasdiqlandi';
    case REJECTED = 'translation.rejected';//'Rad etildi';

    public static function getLabel(string $value): string
    {
        return match ($value) {
            'DRAFT' => 'translation.created',
            'STARTED' => 'translation.started',
            'FINISHED' => 'translation.finished',
            'APPROVED' => 'translation.approved',
            'REJECTED' => 'translation.rejected'
        };
    }
}
