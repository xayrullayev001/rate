<?php

namespace App\Enums;

enum OrderTypeEnum: string
{
    case AUTO = 'translation.auto';
    case ESTATE = 'translation.estate';
}
