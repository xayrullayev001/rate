<?php

namespace App\Enums;


enum MediaTypeEnum: string
{
    case ALL = 'translation.all';
    case PASSPORT_CUSTOMER = 'translation.passport-customer';
    case OBJECT_FILES = 'translation.object-files';
    case COMPARES = 'translation.compares';
    case OBJECT_PHOTO = 'translation.object-photo';
    case PARTICIPATE_CERTIFICATE = 'translation.participate-certificate';
    case APPRAISER_CERTIFICATE = 'translation.appraiser-certificate';
    case INSURANCE_POLICY = 'translation.insurance-policy';
    case CONCLUSION = 'translation.conclusion';
    case ADDITIONAL = 'translation.additional';
    case CERTIFICATE = 'translation.certificate';

    public static function getLabel(string $value): string
    {
        return match ($value) {
            'ALL' => 'translation.all',
            'PASSPORT_CUSTOMER' => 'translation.passport-customer',
            'OBJECT_FILES' => 'translation.object-files',
            'COMPARES' => 'translation.compares',
            'OBJECT_PHOTO' => 'translation.object-photo',
            'PARTICIPATE_CERTIFICATE' => 'translation.participate-certificate',
            'APPRAISER_CERTIFICATE' => 'translation.appraiser-certificate',
            'INSURANCE_POLICY' => 'translation.insurance-policy',
            'CONCLUSION' => 'translation.conclusion',
            'ADDITIONAL' => 'translation.additional',
            'CERTIFICATE' => 'translation.certificate'
        };
    }
}
