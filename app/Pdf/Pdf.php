<?php

namespace App\Pdf;

use Illuminate\Support\Facades\Log;
use setasign\Fpdi\Fpdi;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\Filter\FilterException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use setasign\Fpdi\PdfReader\PdfReaderException;

class Pdf extends Fpdi
{
    private string $qrPath;
    public array $files = [];

    /**
     * @var string
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @throws CrossReferenceException
     * @throws PdfReaderException
     * @throws PdfParserException
     * @throws FilterException
     * @throws PdfTypeException
     */
    public function concat()
    {
        foreach ($this->files as $file) {
            $pageCount = $this->setSourceFile($file);
            for ($pageNo = 1; $pageNo <= $pageCount; $pageNo++) {
                $pageId = $this->ImportPage($pageNo);
                $s = $this->getTemplatesize($pageId);
                $this->AddPage($s['orientation'], $s);
                $this->useImportedPage($pageId);
            }
        }
    }

    function Header()
    {
        if ($this->PageNo() == 1) {
            $xxx_final = ($this->CurPageSize[0] - 70);
            $yyy_final = ($this->CurPageSize[1] - 15);
            $this->Image($this->getQrPath(), $xxx_final, 27, 0, 0, 'png');
        }
    }

    function Footer()
    {
        Log::debug($this->PageNo());
        //Put the watermark
        $xxx_final = ($this->CurPageSize[0] - 50);
        $yyy_final = ($this->CurPageSize[1] - 30);

//        $this->setY(-16.5);
//        $this->SetFont('Arial', 'B', 8);
//        $string = "Отчет №448 от 28.06.2022г.Об определении рыночной стоимости АМТС_____________Тураев Т.Р";
//        $this->Cell(0, 8, $this->convert_utf8($string), 0, 8, 'C');
//
//        $this->setY(-12);
//        $this->SetFont('Arial', 'B', 8);
//        $this->Cell(0, 8, mb_convert_encoding("Страница " . $this->page . " из ", "UTF-8", mb_detect_encoding($string, "auto", true)), 0, 8, 'C');
        if ($this->PageNo() != 1) {
            $this->Image($this->getQrPath(), $xxx_final, $yyy_final, 0, 0, 'png');
        }
    }

    function setQrPath(string $qrPath)
    {
        $this->qrPath = $qrPath;
        Log::debug("Qr code path ", [$this->getQrPath()]);
    }

    function getQrPath(): string
    {
        return $this->qrPath;
    }
}
