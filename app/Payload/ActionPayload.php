<?php

namespace App\Payload;


/**
 *
 * TrackingActionTypeEnum:ATTACHMENTS
 * {
 *   "orderId": 1,
 *   "orderType": 'auto',
 *   "user": {
 *      "id": 1,
 *      "name": "Natasha Carey",
 *      "avatar": "assets/images/users/avatar-2.jpg"
 *   },
 *   "attachments": [
 *      "assets/images/small/img-2.jpg",
 *      "assets/images/small/img-3.jpg",
 *      "assets/images/small/img-4.jpg"
 *   ]
 * }
 *
 */
class ActionPayload
{
    private int $orderId;
    private string $orderType;
    private array $user;
    private array $attachments = [];

    /**
     * @param int $orderId
     */
    public function setOrderId(int $orderId): void
    {
        $this->orderId = $orderId;
    }

    /**
     * @return string
     */
    public function getOrderType(): string
    {
        return $this->orderType;
    }

    /**
     * @param string $orderType
     */
    public function setOrderType(string $orderType): void
    {
        $this->orderType = $orderType;
    }

    /**
     * @param array $userId
     */
    public function setUserId(int $userId): void
    {
        $this->user['id'] = $userId;
    }

    /**
     * @param array $avatar
     */
    public function setAvatar(string $avatar): void
    {
        $this->user['avatar'] = $avatar;
    }

    /**
     * @param array $name
     */
    public function setName(string $name): void
    {
        $this->user['name'] = $name;
    }

    /**
     * @return array
     */
    public function getUser(): array
    {
        return $this->user;
    }

    /**
     * @param string|array $attachmentItem
     * @return void
     */
    public function addAttachment(string|array $attachmentItem): void
    {
        if (count($this->attachments) > 0) {
            array_push($this->attachments, $attachmentItem);
        } else {
            $this->attachments[] = $attachmentItem;
        }
    }

    /**
     * @return array
     */
    public function getAttachments(): array
    {
        return $this->attachments;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function convertToJson(): string
    {
        return json_encode(['orderId' => $this->getOrderId(), 'orderType' => $this->getOrderType(), 'user' => $this->getUser(), 'attachments' => $this->getAttachments()]);
    }
}
