<?php

namespace App\Console\Commands;

use App\Models\User;
use Illuminate\Console\Command;

class LastLoginUser extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'last:login';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checking last logined users';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(): int
    {
        $users = User::whereRaw("last_login < date_trunc('day', NOW() - interval '1 month')")
            ->get();

        foreach ($users as $user) {
            $user->update(['status' => 'inactive']);
        }
        return count($users);
    }
}
