<?php

namespace App\Models;

use App\Models\Traits\Filterable;
use App\Models\Traits\StatsFilterable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\AutoOrder
 *
 * @property int $id
 * @property string $number
 * @property int|null $diller_id
 * @property string|null $ordered_customer
 * @property string|null $ordered_customer_phone
 * @property string $purpose
 * @property string $car_mark
 * @property string $car_category
 * @property string $car_subcategory
 * @property int $cost cost of service
 * @property string|null $note
 * @property int|null $price paid by customer summ
 * @property string|null $price_note note on time evaluate order
 * @property string $status
 * @property string|null $customer_first_name null when customer is juridical
 * @property string|null $customer_last_name null when customer is juridical
 * @property string|null $customer_patronymic null when customer is juridical
 * @property string|null $customer_company null when customer is physical
 * @property string|null $owner_first_name null when owner is juridical
 * @property string|null $owner_last_name null when owner is juridical
 * @property string|null $owner_patronymic null when owner is juridical
 * @property string|null $owner_company null when owner is physical
 * @property string $car_number
 * @property string $body
 * @property string $engine
 * @property string|null $color
 * @property string $tech_passport
 * @property string $tech_given_date
 * @property string $tech_given_whom
 * @property string $type
 * @property string $shassi
 * @property string $made_date
 * @property int|null $object_price order evaluated price
 * @property string $created_at
 * @property string $updated_at
 * @property-read string $customer
 * @property-read mixed $diller
 * @property-read mixed $owner
 * @method static \Database\Factories\AutoOrderFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder filter(\App\Http\FIlters\FilterInterface $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder query()
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereBody($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCarCategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCarMark($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCarNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCarSubcategory($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereColor($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCustomerCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCustomerFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCustomerLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereCustomerPatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereDillerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereEngine($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereMadeDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereObjectPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOrderedCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOrderedCustomerPhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOwnerCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOwnerFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOwnerLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereOwnerPatronymic($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder wherePriceNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder wherePurpose($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereShassi($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereTechGivenDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereTechGivenWhom($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereTechPassport($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|AutoOrder whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string $customer_type
 * @property string $owner_type
 * @property string $contract_date
 * @property string $name_of_object
 * @property string $address
 * @property string $region
 * @property string $district
 * @property string $home
 * @property string|null $area
 * @property string|null $overall_area
 * @property string|null $usefull_area
 * @property string|null $live_area
 * @property-read \App\Models\User|null $dillerUser
 * @method static Builder|EstateOrder whereAddress($value)
 * @method static Builder|EstateOrder whereArea($value)
 * @method static Builder|EstateOrder whereContractDate($value)
 * @method static Builder|EstateOrder whereCustomerType($value)
 * @method static Builder|EstateOrder whereDistrict($value)
 * @method static Builder|EstateOrder whereHome($value)
 * @method static Builder|EstateOrder whereLiveArea($value)
 * @method static Builder|EstateOrder whereNameOfObject($value)
 * @method static Builder|EstateOrder whereOverallArea($value)
 * @method static Builder|EstateOrder whereOwnerType($value)
 * @method static Builder|EstateOrder whereRegion($value)
 * @method static Builder|EstateOrder whereUsefullArea($value)
 */
class EstateOrder extends Model
{
    use HasFactory, Filterable, StatsFilterable;

    public const JURIDICAL = 'juridical';
    public const PHYSICAL = 'physical';

    public static string $customer_first_name = 'customer_first_name';
    public static string $customer_last_name = 'customer_last_name';
    public static string $customer_patronymic = 'customer_patronymic';
    public static string $owner_first_name = 'owner_first_name';
    public static string $owner_last_name = 'owner_last_name';
    public static string $owner_patronymic = 'owner_patronymic';
    public static string $owner = 'owner';
    public static string $purpose = 'purpose_id';
    public static string $carCategory = 'car_category';
    public static string $status = 'status';
    public static string $diller = 'diller';
    public static string $orderedCustomer = 'ordered_customer';
    public static string $orderedCustomerPhone = 'ordered_customer_phone';
    public static string $orderNumber = 'number';
    protected $fillable = [
        'number',
        'ordered_customer',
        'ordered_customer_phone',
        'diller_id',
        'customer_first_name',
        'customer_last_name',
        'customer_patronymic',
        'customer_company',
        'customer_type',
        'owner_first_name',
        'owner_last_name',
        'owner_patronymic',
        'owner_company',
        'owner_type',
        'purpose_id',

        'name_of_object',
        'contract_date',
        'region',
        'district',
        'address',
        'home',
        'area',
        'overall_area',
        'usefull_area',
        'live_area',
//        'tech_passport',
//        'tech_given_date',
//        'tech_given_whom',

        'cost',
        'note',
        'status'
    ];

    public static string $nameOfObject = 'name_of_object';

    public function dillerUser()
    {
        return $this->hasOne(User::class, 'id', 'diller_id');
    }

    public function purposeOne()
    {
        return $this->hasOne(Purpose::class, 'id', 'purpose_id');
    }

    public function regions()
    {
        return $this->hasOne(Region::class, 'id', 'region');
    }

    public function districts()
    {
        return $this->hasOne(District::class, 'id', 'district');
    }

    public function getCustomerAttribute(): string
    {
        if (self::PHYSICAL == $this->customer_type) {
            return sprintf("%s %s. %s.",
                $this->customer_last_name,
                mb_substr($this->customer_first_name, 0, 1, "utf-8"),
                mb_substr($this->customer_patronymic, 0, 1, "utf-8")
            );
        }
        return $this->customer_company;
    }

    public function getOwnerAttribute()
    {
        if (self::PHYSICAL == $this->owner_type) {
            return sprintf("%s %s. %s.",
                $this->owner_last_name,
                mb_substr($this->owner_first_name, 0, 1, "utf-8"),
                mb_substr($this->owner_patronymic, 0, 1, "utf-8")
            );
        }
        return $this->owner_company;
    }

    public function getDillerAttribute()
    {
        if ($this->diller_id != null) {
            return sprintf("%s %s. %s.",
                $this->dillerUser->last_name,
                mb_substr($this->dillerUser->name, 0, 1, "utf-8"),
                mb_substr($this->dillerUser->patronymic, 0, 1, "utf-8")
            );
        }
        return "";
    }

    public function getCreatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    public function getUpdatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    public function getMadeDateAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }
}
