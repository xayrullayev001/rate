<?php

namespace App\Models\Traits;

use App\Http\FIlters\FilterInterface;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

trait Filterable
{
    /**
     * @param Builder $builder
     * @param FilterInterface $filter
     * @return LengthAwarePaginator
     */
    public function scopeFilter(Builder $builder, FilterInterface $filter):LengthAwarePaginator
    {
        $builder->orderBy('updated_at','desc');
        Log::debug("Builder {0}",[$builder->getQuery()]);
        $filter->apply($builder);
        return $builder->paginate($filter->getSize());
    }
}
