<?php

namespace App\Models\Traits;

use App\Http\FIlters\FilterInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

trait StatsFilterable
{
    /**
     * @param Builder $builder
     * @param FilterInterface $filter
     * @return Collection|Builder[]
     */
    public function scopeStats(Builder $builder, FilterInterface $filter): array|Collection
    {
        Log::debug("Builder {0}", [$builder->getQuery()]);
        $filter->apply($builder);
        return $builder->get();
    }
}
