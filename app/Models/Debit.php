<?php

namespace App\Models;

use App\Enums\OrderTypeEnum;
use App\Models\Traits\Filterable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Debit
 *
 * @property int $id
 * @property int $appraiser_id
 * @property int $order_id
 * @property int $cost
 * @property string $customer
 * @property string $expired
 * @property string $created_at
 * @property string $updated_at
 * @property string $status
 * @property-read \App\Models\User|null $appraiser
 * @property-read \App\Models\AutoOrder|null $order
 * @method static \Database\Factories\DebitFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit filter(\App\Http\FIlters\FilterInterface $filter)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Debit newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Debit query()
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereAppraiserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereCost($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereCustomer($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereExpired($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $order_type
 * @method static \Illuminate\Database\Eloquent\Builder|Debit whereOrderType($value)
 */
class Debit extends Model
{
    use HasFactory, Filterable;

    public static string $NOPAID = "nopaid";
    public static string $PAID = "paid";

    /**
     * @var string[]
     */
    protected $fillable = [
        'appraiser_id',
        'order_id',
        'diller_id',
        'cost',
        'customer',
        'expired',
        'status',
        'order_type',
    ];

    public function appraiser(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'appraiser_id');
    }

    public function diller(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'appraiser_id');
    }

    public function order(): HasOne
    {
        if ($this->order_type == OrderTypeEnum::AUTO->name) {
            return $this->hasOne(AutoOrder::class, 'id', 'order_id');
        } else {
            return $this->hasOne(EstateOrder::class, 'id', 'order_id');
        }
    }

    public function getCreatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    public function getUpdatedAtAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }

    public function getExpiredAttribute($value): string
    {
        return Carbon::parse($value)->format('d.m.Y');
    }
}
