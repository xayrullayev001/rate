<?php

namespace App\Models;

use App\Enums\RoleEnum;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\UserInfo
 *
 * @property int $id
 * @property int $user_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $description
 * @property string $address
 * @property string $cover
 * @property string $skills
 * @property string $company
 * @property float $completed
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo query()
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCompany($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCompleted($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCover($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereSkills($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|UserInfo whereUserId($value)
 * @mixin \Eloquent
 */
class UserInfo extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'description',
        'address',
        'cover',
        'skills',
        'company',
        'completed'
    ];

    public function getUser(): HasOne
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    /**
     * @param User $user
     * @return void
     */
    public static function createDefault(User $user)
    {
        UserInfo::create([
            'user_id' => $user->id,
            'first_name' => $user->name,
            'last_name' => $user->name,
            'email' => $user->name . '@gmail.com',
            'description' => $user->name . ' description',
            'address' => "O'zbekiston Toshkent",
            'cover' => 'assets/images/profile-bg.jpg',
            'skills' => RoleEnum::tryFrom($user->role)->value,
            'company' => 'Sifat Baholash',
            'completed' => 0
        ]);
    }

    /**
     * Return skills like array for printing
     * @return array
     */
    public function getSkillsAsArray(): array
    {
        return explode(',', $this->skills);
    }
}
