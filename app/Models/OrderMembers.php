<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\OrderMembers
 *
 * @property int $id
 * @property int $order_id
 * @property int $user_id
 * @property string|null $note
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Database\Factories\OrderMembersFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereNote($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $order_type
 * @method static \Illuminate\Database\Eloquent\Builder|OrderMembers whereOrderType($value)
 */
class OrderMembers extends Model
{
    use HasFactory;

    protected $fillable = [
        'order_id',
        'user_id',
        'order_type'
    ];
}
