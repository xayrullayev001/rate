<?php

namespace App\Models;

use App\Enums\OrderTypeEnum;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * App\Models\Bonus
 *
 * @property int $id
 * @property int $order_id
 * @property int $appraiser_id
 * @property int $amount
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User|null $appraiser
 * @property-read \App\Models\AutoOrder|null $order
 * @method static \Database\Factories\BonusFactory factory(...$parameters)
 * @method static Builder|Bonus newModelQuery()
 * @method static Builder|Bonus newQuery()
 * @method static Builder|Bonus query()
 * @method static Builder|Bonus whereAmount($value)
 * @method static Builder|Bonus whereAppraiserId($value)
 * @method static Builder|Bonus whereCreatedAt($value)
 * @method static Builder|Bonus whereId($value)
 * @method static Builder|Bonus whereOrderId($value)
 * @method static Builder|Bonus whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property string|null $order_type
 * @method static Builder|Bonus whereOrderType($value)
 */
class Bonus extends Model
{
    use HasFactory;

    protected $fillable = [
        'appraiser_id',
        'amount',
        'order_id',
        'order_type'
    ];

    public function appraiser()
    {
        return $this->hasOne(User::class, 'id', 'appraiser_id');
    }

    public function order(): HasOne
    {
        if ($this->order_type == OrderTypeEnum::AUTO->name) {
            return $this->hasOne(AutoOrder::class, 'id', 'order_id');
        } else {
            return $this->hasOne(EstateOrder::class, 'id', 'order_id');
        }
    }
}
