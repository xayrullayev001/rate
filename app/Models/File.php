<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\File
 *
 * @property string $id
 * @property string $path
 * @property string $extension
 * @property int $order_id
 * @property string $type
 * @property string $order_type
 * @property string $name
 * @property string $size
 * @property int $size_in_bytes
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|File newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|File query()
 * @method static \Illuminate\Database\Eloquent\Builder|File whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereExtension($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereSize($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereSizeInBytes($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|File whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|File whereOrderType($value)
 */
class File extends Model
{
    use HasFactory;

    public $incrementing = false;

    public static function getFileSizeInMB(int $fileSize): string
    {
        if ($fileSize >= 1073741824) {
            return number_format($fileSize / 1073741824, 2) . ' GB';
        } elseif ($fileSize >= 1048576) {
            return number_format($fileSize / 1048576, 2) . ' MB';
        } elseif ($fileSize >= 1024) {
            return number_format($fileSize / 1024, 2) . ' KB';
        } elseif ($fileSize > 1) {
            return $fileSize . ' bytes';
        } elseif ($fileSize == 1) {
            return '1 byte';
        } else {
            return '0 bytes';
        }
    }
}
