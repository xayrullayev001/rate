<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Certificate
 *
 * @property int $id
 * @property string $uz
 * @property string $ru
 * @property string $cr
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose query()
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose wherePath($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereSort($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereUpdatedAt($value)
 * @mixin \Eloquent
 * @method static \Database\Factories\PurposeFactory factory(...$parameters)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereCr($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereRu($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Purpose whereUz($value)
 */
class Purpose extends Model
{
    use HasFactory;

    protected $fillable = [
        'uz',
        'ru',
        'cr'
    ];
}
