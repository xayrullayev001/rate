<?php

namespace App\Models;

use App\Enums\TrackingActionTypeEnum;
use App\Payload\ActionPayload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\TrackingActions
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_id
 * @property string|null $images
 * @property string $msg
 * @property string $params
 * @property string $type
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions query()
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereImages($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereMsg($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereParams($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereUserId($value)
 * @mixin \Eloquent
 * @property string|null $order_type
 * @method static \Illuminate\Database\Eloquent\Builder|TrackingActions whereOrderType($value)
 */
class TrackingActions extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'order_id',
        'user_id',
        'msg',
        'params',
        'order_type'
    ];

    /**
     *
     * Where user added additional attachment files to order
     *
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionAttach(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::ATTACHMENTS->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::ATTACHMENTS,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    public static function actionUpdate(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::ORDER_UPDATED->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::ORDER_UPDATED,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    /**
     *
     * Where user created order
     *
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionCreated(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::ORDER_CREATED->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::ORDER_CREATED,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    /**
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionMemberInvite(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::MEMBERS_ADDED->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::MEMBERS_ADDED,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    /**
     * When user|appraiser uploaded conclusion file to order
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionConclusionAdd(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::COMPLETED->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::COMPLETED,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    /**
     * When ADMIN reject order will be created reject history
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionConclusionReject(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::REJECTED->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::REJECTED,
            'params' => $actionPayload->convertToJson()
        ]);
    }

    /**
     * When appraiser taken bonus action history created automatically
     * @param ActionPayload $actionPayload
     * @return void
     */
    public static function actionAppraiserTakenBonus(ActionPayload $actionPayload): void
    {
        TrackingActions::create([
            'type' => TrackingActionTypeEnum::TAKEN_BONUS->name,
            'order_id' => $actionPayload->getOrderId(),
            'order_type' => $actionPayload->getOrderType(),
            'user_id' => $actionPayload->getUser()['id'],
            'msg' => TrackingActionTypeEnum::TAKEN_BONUS,
            'params' => $actionPayload->convertToJson()
        ]);
    }
}
